import React, { Component } from 'react'
import { Button, Card, Dialog, Classes, Slider, Intent, FileInput, TextArea } from "@blueprintjs/core"
import { DateInput, TimePrecision } from "@blueprintjs/datetime"
import BigCalendar from 'react-big-calendar'
import Select from 'react-select'

import { connect } from 'react-redux'
import { updatePostCategory, fetchCats, fetchPosts, fetchBrands } from '../../../actions'

import _ from 'lodash'
import moment from 'moment'

import 'react-big-calendar/lib/css/react-big-calendar.css'
import './styles.css'



class Calendar extends Component {
  constructor(props){
    super(props)

    this.state = {
      newWeekDialogOpen: false,
      rank: 1,
      newEventOpen: false,
      events: [{
        start: new Date(),
        end: new Date(moment().add(0, "days")),
        title: "Algun posteo"
      }],
      date:null,
      selectedOption: null,
      updated: false,
      copyValue: '',
      commentValue: ''
    }
  }

  componentWillMount() {
      this.props.fetchPosts( this.props.id );
      this.props.fetchCats( this.props.id );
  }

  handleChange = (selectedOption) => {
    this.setState({
      selectedOption
    });

    // this.props.updatePostCategory( selectedOption.value, this.props.post.id, this.props.post.provider );
  }

  setCats() {
    let options = [];
    this.props.categories.map(
      ( cat ) => {
        options.push( { 'label': cat.name, 'value': cat.id } )
        return '';
      }
    )

    return options;
  }

  renderSelect() {
    //console.log( "Actualizando post - marca ", this.props.addingBrand );

    if( !this.props.loading_cats ) {
      let that = this;
      const options = this.setCats();
      const selectPlaceholder = 'Categories';

      let sel_label = '';
      let sel_value = 0;

      //console.log( "======================================================== categorias " );
      //console.log( this.props.categories );
      //console.log( "======================================================== cateogoria id:" );
      // console.log( "that.props.post.categoria_id", that.props.post.categoria_id );
      //console.log( "========================================================" );

      let getCategoriaID = this.props.posts.posts.map((post) => {
        return post.categoria_id
      })

      console.log(getCategoriaID)

      // se busca la categoría del post
      let selectedOption = _.findLast(this.props.categories, function(cat) {
                     console.log( "cat ", cat );
                    return cat.id == 1
                  });

      console.log(selectedOption)

      // if( this.state.selectedOption!==null )
      // {
      //   sel_label = this.state.selectedOption.label;
      //   sel_value = this.state.selectedOption.value;
      // }
      // else
      // {
      //   sel_label = selectedOption.name;
      //   sel_value = selectedOption.id;
      // }

      return(
        <Select
            value={ { 'label': sel_label, 'value': sel_value } }
            onChange={this.handleChange}
            options={options}
            placeholder={selectPlaceholder}
            className='react-select-container'
            classNamePrefix='react-select'
          />
      );
    }
    else {
      let that = this;
      const options = null;
      const selectPlaceholder = 'Categories';
      const selectStyle = {
        option: (base, state) => ({
          ...base,
          borderBottom: '1px dotted pink',
          padding: 5,
          backgroundColor: 'green'
        }),
        control: () => ({
          // none of react-selects styles are passed to <View />
          width: 100,
          backgroundColor: 'red'
        }),
        singleValue: (base, state) => {
          const opacity = state.isDisabled ? 0.5 : 1;
          const transition = 'opacity 300ms';

          return { ...base, opacity, transition };
        }
      }

      let sel_label = '';
      let sel_value = 0;
      sel_label = '';
      sel_value = 1;

      return(
        <Select
            onChange={this.handleChange}
            placeholder={selectPlaceholder}
            className='react-select-container'
            classNamePrefix='react-select'
          />
      );
    }
  }



  render () {

    console.log("calendar props ---->", this.props)

    const localizer = BigCalendar.momentLocalizer(moment)

    return(
      <div className="calendarShell" >

        <div className="calendarControls">
          <Button onClick={this.openNewWeekDialog} minimal="true" className="button button-primary" >New Week</Button>
          <Button onClick={this.openNewEventDialog} minimal="true" className="button button-primary" style={{ marginLeft: '12px' }} >New Event</Button>
        </div>

        <BigCalendar
            defaultDate={new Date()}
            defaultView="month"
            events={this.state.events}
            style={{ height: "calc(100vh - 80px)" }}
            localizer={localizer}
          />

          <Dialog
                    onClose={this.closeNewWeekDialog}
                    title="Select the number of weeks to add:"
                    isOpen={this.state.newWeekDialogOpen}
                    className="bp3-dark">
            <div className={Classes.DIALOG_BODY}>

            <Slider
                  min={1}
                  max={4}
                  stepSize={1}
                  labelStepSize={10}
                  value={ this.state.rank }
                  onChange={this.getChangeRankHandler}
              />

            </div>
            <div className={Classes.DIALOG_FOOTER}>
                <div className={Classes.DIALOG_FOOTER_ACTIONS}>
                    <Button onClick={this.closeNewWeekDialog}>Select</Button>
                </div>
            </div>


          </Dialog>

          <Dialog
                    isOpen={this.state.newEventOpen}
                    className="bp3-dark calendarPosts">
            <div className={Classes.DIALOG_BODY}>

              <div className="grid">

                <div className="col-7">
                  <Card className="bp3-dark">

                    <div className="calendarPostsConfig">
                      <div className="grid">
                        <div className="col-4">
                          <div className="calendarPostsConfigDate">

                            <DateInput
                                formatDate={date => moment(date).format("YYYY/MM/DD", "h:mm")}
                                onChange={this.handleDateChange}
                                parseDate={str => new Date(str)}
                                placeholder={"YYYY/M/D"}
                                defaultValue={new Date()}
                            />
                          </div>

                        </div>
                        <div className="col-1" style={{ paddingRight: '0' }}>
                          <span className="searchCalendarIcon"><i className="far fa-calendar-alt"></i></span>
                        </div>
                        <div className="col-7" style={{ paddingLeft:'0' }}>
                          <div className="postExtrasCategory">
                            { this.renderSelect() }
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="calendarPostsImages">

                      <div className="providerInfo">
                        <div className="providerInfoLogo">
                          <i className="fab fa-facebook-f"></i>
                        </div>

                        <div className="providerInfoTxt">
                            <p>Image 1,200 x 1,200</p>
                            <p>Video 1,200 x 675 4MB</p>
                        </div>

                      </div>

                      <div className="fileInput">
                        <FileInput text="Choose file..." />
                      </div>


                    </div>

                    <div className="calendarInput">

                      <TextArea
                          large={true}
                          onChange={this.handleCopyChange}
                          value={this.state.copyValue}
                          placeholder="COPY"
                          fill={true}
                          className="calendarCopyTextArea"
                      />
                      <div className="calendarCopywords">
                        { this.state.copyValue.length }
                      </div>

                    </div>

                    <div className="calendarComments">
                      <div className="grid">
                        <div className="col-8 no-pad-r">

                          <div className="calendarInput">

                            <TextArea
                                large={true}
                                onChange={this.handleCommentChange}
                                value={this.state.commentValue}
                                placeholder="COMMENT"
                                fill={true}
                                className="calendarCopyTextArea"
                            />

                          </div>

                        </div>
                        <div className="col-4 no-pad-l">
                        </div>

                      </div>

                    </div>
                    <div className="calendarPostsAddFooter">

                      <div className="grid">
                        <div className="col-8">
                          <div className="calendarCuenta">
                            <img src={this.state.brandImageUrl} />
                          </div>
                        </div>
                        <div className="col-4">
                          <Button intent="danger" text="Delete" onClick="" className="marcaActionsErase" />
                          <Button onClick={this.closeNewEventDialog}>Create</Button>
                        </div>
                      </div>

                    </div>

                  </Card>
                </div>

                <div className="col-5">
                  <Card className="bp3-dark">
                    <div className="calendarPostsComments">
                      Kate Courtney ciclista profesional de montaña te enseña como mantenerte sobre tu bici. Specialized Bicycles
                    </div>
                  </Card>
                </div>

                <div className="col-12">
                  <Card className="bp3-dark">
                    image
                  </Card>
                </div>
              </div>



            </div>


          </Dialog>

      </div>
    )
  }


  openNewWeekDialog = () => this.setState({ newWeekDialogOpen: true })
  closeNewWeekDialog = () => this.setState({ newWeekDialogOpen: false })

  openNewEventDialog = () => this.setState({ newEventOpen: true })
  closeNewEventDialog = () => this.setState({ newEventOpen: false })

  getChangeRankHandler = (value) => this.setState({ rank: value })

  handleRangeChange = (date) => {
    this.setState({ date });
    console.log(date);
  }

  handleCopyChange = (e) => {
    let getComment = e.target.value
    console.log(getComment)
    this.setState({ copyValue: getComment })
  }

  handleCommentChange = (e) => {
    let getComment = e.target.value
    console.log(getComment)
    this.setState({ commentValue: getComment })
  }


}

function mapStateToProps( state ){
  return {
    loading_cats: state.categories.loading,
    categories: state.categories.categories,
    posts: state.posts,
    loading_brands: state.brands.loading,
    brands: state.brands.brands,
    addingBrand: state.adding
  }
}
export default connect(mapStateToProps, { fetchCats, fetchPosts, fetchBrands })( Calendar );
