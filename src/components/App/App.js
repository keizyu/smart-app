import React, { Component } from 'react'
import { Route, Redirect } from 'react-router-dom' // BrowserRouter, Route, Redirect
import Home from '../Home/Home'
import Login from '../Login/Login'
import Dashboard from '../Dashboard/Dashboard'
import SocialIntelligence from '../SocialIntelligence/Dashboard/Dashboard'
import AdsIntelligence from '../AdsIntelligence/Dashboard/Dashboard'
import WebIntelligence from '../WebIntelligence/Dashboard/Dashboard'
import Calendar from '../Calendar/Dashboard'
import { connect } from 'react-redux'
import * as actions from '../../actions'

import './styles.css'

class App extends Component {
  render() {
    let token = localStorage.getItem( "api_access_token" );
    let validSession = ( token !== null );

    return (
      <div className="App">

        <Route path="/" exact component={Login} />

        <Route path="/dashboard/:id" component={Dashboard} render={(props) => (
              validSession ? <Home /> : <Redirect to='/' />
            )}/>

        <Route path="/social/:id" component={SocialIntelligence} render={(props) => (
              validSession ? <Home /> : <Redirect to='/' />
            )}/>

        <Route path="/ads/:id" component={AdsIntelligence} render={(props) => (
          validSession ? <Home /> : <Redirect to='/' />
        )}/>

        <Route path="/web/:id" component={WebIntelligence} render={(props) => (
            validSession ? <Home /> : <Redirect to='/' />
          )}/>

        <Route path="/calendar/:id" component={Calendar} render={(props) => (
              validSession ? <Home /> : <Redirect to='/' />
            )}/>

        <Route path="/home" render={(props) => (
              validSession ? <Home /> : <Redirect to='/' />
            )} />
      </div>
    );
  }
}

function mapStateToProps( state )
{
  return { auth: state.auth };
}

export default connect( mapStateToProps, actions )( App );
