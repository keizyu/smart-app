import React, { Component } from 'react';
//import ListadoMarcas from '../ListadoMarcas/ListadoMarcas';
import logoLogin from '../../assets/img/loginsmart.png';

//import BtFacebook from '../BtFacebook';
//import Home from '../Home/Home';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import { API_URL } from '../../config.js';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props'

import './styles.css'

class Login extends Component {

  responseFacebook = (resultObject) => {

    this.props.changeAuth( true );
      // se guarda el usuario en el navegador
      localStorage.setItem('resultObject', JSON.stringify({
          user:{
              id: resultObject.id,
              userID: resultObject.id,
              accessToken: resultObject.accessToken,
              name: resultObject.name
          },
          authResponse: {
              accessToken: resultObject.accessToken
          }
      }));

      const fbResponse = resultObject;

      let user_id = fbResponse.userID;
      let token = fbResponse.accessToken;

      let data = {
          facebook_id: user_id,
          username: fbResponse.name,
          user_token: token,
          name: fbResponse.name
        };

      fetch( API_URL + 'users', {
          method: 'POST',
          body: JSON.stringify(data),
          headers:{
            'Content-Type': 'application/json'
          }
        }).then(res=>res.json())
          .then(
            res => {
              let data = res.data;

              console.log( res );

              localStorage.setItem('api_access_token', data.token );

              // se envía al usuario al home
              this.props.history.push('/home')
            });

  }

  render() {
    return (
      <div className="login">
        <div className="mainLogin grid-center-middle">
          <div className="col-6_sm-12">
            <div className="mainLoginLogo">
              <img src={logoLogin} className="logoLogin" alt="logo" />
            </div>
          </div>

          <div className="col-6_sm-12">
            <div className="loginForm">

              <FacebookLogin
                  appId="1214221415366486"
                  callback={this.responseFacebook}
                  scope="public_profile,read_insights,manage_pages,instagram_basic,email,instagram_manage_comments,instagram_manage_insights,ads_read"
                  version="3.0"
                  render={renderProps => (
                    <button onClick={renderProps.onClick} className="loginButton">
                      <span className="loginButtonTxt">Login with facebook</span>
                      <i className="fab fa-facebook fa-3x"></i>
                    </button>
                  )}
                />

            </div>
            <p className="powered">Powered by Quark Technologies</p>
          </div>

        </div>
      </div>
    );
  }
}


function mapStateToProps( state )
{
  return { auth: state.auth };
}

export default connect( mapStateToProps, actions )( Login );
