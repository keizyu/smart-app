// Web Intelligence Dashboard
import React, { Component } from 'react';
import Sidebar from 'react-sidebar';

// COMPONENTES
import MainContainer from '../MainContainer/MainContainer';
import Header from '../../Shared/Header/Header';
import Navbar from '../Navbar/Navbar';
import SidebarContent from '../../Shared/SidebarContent/SidebarContent';
import { connect } from 'react-redux';
import { fetchCats } from '../../../actions';

class WebIntelligence extends Component {

  constructor(props) {
    super(props);
    this.state = {
      open: false,
      sidebarOpen: false,
      pullRight: true,
      shouldHide: false,
      selectedBrand: 0,
      cats: [],
      formatDate: null,
      range: [null, null],
    }
  }

  onSetSidebarOpen = (open) => {
    this.props.fetchCats();
    this.setState({ sidebarOpen: open });
  }

  render () {
    let sidebarContent = <SidebarContent  id={ this.props.match.params.id } />;

    return(

      <Sidebar sidebar={sidebarContent}
             open={this.state.sidebarOpen}
             onSetOpen={this.onSetSidebarOpen}
             pullRight={this.state.pullRight}
             rootClassName="sidebar__root"
             sidebarClassName="sidebar__controller"
             overlayClassName="sidebar__overlay" >

            <div className="dashboard-shell">

              <Header sidebar={this.onSetSidebarOpen} hide={this.state.shouldHide} />

              <Navbar id={ this.props.match.params.id } />

              <MainContainer id={ this.props.match.params.id } />

            </div>

    </Sidebar>

    );
  }
}

function mapStateToProps( state )
{
  return {
    selectedBrand: state.selectedBrand,
    cats: state.categories.categories
  }
}

export default connect(mapStateToProps, { fetchCats })( WebIntelligence );
