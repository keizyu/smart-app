import React, { Component } from 'react'
import { Button, H1 } from "@blueprintjs/core"
import { DateRangeInput } from "@blueprintjs/datetime"
import Cookies from 'universal-cookie'
import moment from 'moment'
import { connect } from 'react-redux'


// COMPONENTES
import PageActivity from '../PageActivity/PageActivity'

import Visitors from '../Visitors/Visitors'
import VisitorsSessions from '../VisitorsSessions/VisitorsSessions'
import VisitorsDuration from '../VisitorsDuration/VisitorsDuration'

import Gender from '../Gender/Gender'
import GenderSessions from '../GenderSessions/GenderSessions'
import GenderDuration from '../GenderDuration/GenderDuration'

import Age from '../Age/Age'
import AgeSession from '../AgeSession/'
import AgeDuration from '../AgeDuration/'

import Language from '../Language/'
import LanguageSession from '../LanguageSession/'
import LanguageDuration from '../LanguageDuration/'

import LocationComponent from '../LocationComponent/'
import Browser from '../Browser/'
import Devices from '../Devices/'
import Brand from '../Brand/'

import SiteSpeed from '../SiteSpeed/'
import Frequency from '../Frequency/'
import PagePath from '../PagePath/'

import BroadAudience from '../BroadAudience/'
import SpecificAudience from '../SpecificAudience/'
import GranularAudience from '../GranularAudience/'

import Conversion from '../Conversion/'

import SalesScore from '../SalesScore/'

import SourceMedium from '../SourceMedium'
import SearchTerm from '../SearchTerm'
import Campaign from '../Campaign'

import GenderSS from '../GenderSS'
import AgeSS from '../AgeSS'
import Region from '../Region'

import Goal from '../Goal'

import './styles.css'

class MainContainer extends Component {

  constructor(props) {
    super(props)

    // se obtiene la fecha de la cookie
    const cookies = new Cookies();
    let range = cookies.get('range')

    if( !range )
    {
      range = [ new Date(moment().subtract(15, "days").format('YYYY-MM-DD')), new Date(moment().format('YYYY-MM-DD'))]
    }
    else
    {
      range = [ new Date(range[0]), new Date(range[1])];
    }

    cookies.set('range', range, { path: '/' });

    this.state = {
        range: range,
    };

  }

  render () {
    if( this.props.loading )
    {
    }

    return(


      <div className="main-container__dashboard">

        <div className="top-container">
          <div className="brands-added">

            <div className="grid">

              <div className="col-5_sm-12">
                <H1 className="brands-added__title">Web Intelligence</H1>
              </div>

              <div className="col-7_sm-12">

                <div className="searchContainer">
                  <div className="dateSmart bp3-dark">
                    <span className="searchCalendarIcon"><i className="far fa-calendar-alt"></i></span>
                    <DateRangeInput
                        formatDate={date => moment(date).format("YYYY-MM-DD")}
                        onChange={this.handleRangeChange}
                        parseDate={str => new Date(str)}
                        value={this.state.range}
                        placeholder={"YYYY-mm-dd"}
                    />
                    <Button onClick={this.openSearchCalendar} minimal="true" className="button btn-outline" >Search</Button>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>

        <div className="social-intelligence__container">


          <div className="grid">
            <div className="col-12">
                <PageActivity />
            </div>

            <div className="col-4_sm-12">
                <Visitors />
            </div>

            <div className="col-4_sm-12">
                <VisitorsSessions />
            </div>

            <div className="col-4_sm-12">
                <VisitorsDuration />
            </div>

            <div className="col-4_sm-12">
                <Gender />
            </div>

            <div className="col-4_sm-12">
                <GenderSessions />
            </div>

            <div className="col-4_sm-12">
                <GenderDuration />
            </div>

            <div className="col-4_sm-12">
                <Age />
            </div>

            <div className="col-4_sm-12">
              <AgeSession />
            </div>

            <div className="col-4_sm-12">
              <AgeDuration />
            </div>

            <div className="col-4_sm-12">
              <Language />
            </div>

            <div className="col-4_sm-12">
              <LanguageSession />
            </div>

            <div className="col-4_sm-12">
              <LanguageDuration />
            </div>

            <div className="col-4_sm-12">
              <LocationComponent />
            </div>

            <div className="col-4_sm-12 no-pad-b">
                <div className="col-12">
                  <div className="best-days-shell">
                    <div className="grid">

                        <div className="col-12 no-pad-l-f">
                          <Devices />
                        </div>

                        <div className="col-12 no-pad-l-f no-pad-b">
                          <Brand />
                        </div>

                    </div>
                  </div>
                </div>
            </div>


            <div className="col-4_sm-12">
              <Browser />
            </div>

            <div className="col-4_sm-12">
              <SiteSpeed />
            </div>

            <div className="col-4_sm-12">
              <Frequency />
            </div>

            <div className="col-4_sm-12">
              <PagePath />
            </div>

            <div className="col-4_sm-12">
              <BroadAudience />
            </div>

            <div className="col-4_sm-12">
              <SpecificAudience />
            </div>

            <div className="col-4_sm-12">
              <GranularAudience />
            </div>

            <div className="col-12">
                <Conversion />
            </div>

            <div className="col-12">
                <SalesScore />
            </div>

            <div className="col-4_sm-12">
              <SourceMedium />
            </div>

            <div className="col-4_sm-12">
              <SearchTerm />
            </div>

            <div className="col-4_sm-12">
              <Campaign />
            </div>

            <div className="col-4_sm-12">
              <GenderSS />
            </div>

            <div className="col-4_sm-12">
              <AgeSS />
            </div>

            <div className="col-4_sm-12">
              <Region />
            </div>

            <div className="col-4_sm-12">
              <Goal />
            </div>

            <div className="col-4_sm-12">
              <Goal />
            </div>

            <div className="col-4_sm-12">
              <Goal />
            </div>



          </div>
        </div>
      </div>
    );
  }


  openSearchCalendar = () =>  {

  }

  handleRangeChange = (range) => {
    const cookies = new Cookies();
    cookies.set('range', range, { path: '/' });
    this.setState({ range });
  }

}

function mapStateToProps( state )
{
  return {
    loading: state.cats.loading,
  }
}

export default connect(mapStateToProps)( MainContainer );
