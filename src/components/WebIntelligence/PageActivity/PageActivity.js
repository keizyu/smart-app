import React, { Component } from 'react'
import { Card, Elevation, Tooltip, Position } from "@blueprintjs/core"
import numeral from 'numeral'

class PageActivity extends Component {
  render () {

    return (
      <Card interactive={true} elevation={Elevation.TWO} className="no-pad-t">
        <div className="subtitle">
          Page Activity

          <div className="question-tooltip question-tooltip__right">
            <Tooltip content="Reacciones Generales en página" position={Position.BOTTOM}>
              <div><i className="fas fa-question"></i></div>
            </Tooltip>
          </div>

        </div>

        <div className="page-activity" style={{textAlign: 'center'}}>

          <div className="flex-holder">

            <div className="grid">
              <div className="col-2_sm-6 no-pad-b">
                <div className="total-activity__item--impressions webIntelligenceIconItem">
                  <div className="total-activity__icon webIntelligenceRowIcon" >
                    <i className="fas fa-flag-checkered"></i>
                  </div>
                  <div className="total-activity__text">
                    <h3 className="total-activity__text--">
                      Sales Score <br />
                    { numeral(238).format('0,0') }
                    </h3>
                  </div>
                </div>
              </div>

              <div className="col-2_sm-6 no-pad-b">
                <div className="total-activity__item--video webIntelligenceIconItem">
                  <div className="total-activity__icon webIntelligenceRowIcon">
                    <i className="fas fa-user"></i>
                  </div>
                  <div className="total-activity__text">
                    <h3 className="total-activity__text--">
                    Revenue <br />
                  { numeral(238).format('0,0') }
                    </h3>
                  </div>
                </div>
              </div>

              <div className="col-2_sm-6 no-pad-b">
                <div className="total-activity__item--engagement webIntelligenceIconItem">
                  <div className="total-activity__icon webIntelligenceRowIcon">
                    <i className="far fa-clone"></i>
                  </div>
                  <div className="total-activity__text">
                    <h3 className="total-activity__text--">
                    Transaction <br />
                  { numeral(1.3).format('0.0') }
                    </h3>
                  </div>
                </div>
              </div>

              <div className="col-2_sm-6 no-pad-b">
                <div className="total-activity__item--posts webIntelligenceIconItem">
                  <div className="total-activity__icon webIntelligenceRowIcon" >
                    <i className="far fa-clock"></i>
                  </div>
                  <div className="total-activity__text">
                    <h3 className="total-activity__text--">
                    Avg Order Value <br />
                      { numeral(238).format('00:00:00') }
                    </h3>
                  </div>
                </div>
              </div>

              <div className="col-2_sm-6 no-pad-b">
                <div className="total-activity__item--posts webIntelligenceIconItem">
                  <div className="total-activity__icon webIntelligenceRowIcon" style={{backgroundColor: '#981ae6'}} >
                    <i className="fas fa-ban"></i>
                  </div>
                  <div className="total-activity__text">
                    <h3 className="total-activity__text--">
                    Conversion Rate <br />
                  { numeral(0.9748).format('0.00%') }
                    </h3>
                  </div>
                </div>
              </div>

              <div className="col-2_sm-6 no-pad-b">
                <div className="total-activity__item--posts webIntelligenceIconItem">
                  <div className="total-activity__icon webIntelligenceRowIcon" style={{backgroundColor: '#4fabe6'}} >
                    <i className="fas fa-eye"></i>
                  </div>
                  <div className="total-activity__text">
                    <h3 className="total-activity__text--">
                    Promotion Views <br />
                  { numeral(238).format('0,0') }
                    </h3>
                  </div>
                </div>
              </div>

            </div>



          </div>
        </div>

      </Card>
    )

  }
}

export default PageActivity;
