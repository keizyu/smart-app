import React, { Component } from 'react';
import { Card, Elevation, Tooltip, Position } from "@blueprintjs/core";
import { Column, Table, AutoSizer } from 'react-virtualized';
import { Bar } from 'react-chartjs-2';
import { connect } from 'react-redux';
import { fetchSocialBestCats, fetchSocialBestCatsGraph } from '../../../actions';
import { Spinner } from '@blueprintjs/core';

class Campaign extends Component {

	constructor(props){
		super(props);
		this.state = {
			engagementData : {
				labels: [],
				datasets: [{
					data: [],
					backgroundColor: [],
				}],
			}
		}
	}

	renderTable()
	{
		if( this.props.loading_graph )
		{
			return( <Spinner /> )
		}
		else
		{
      let table_data = [
				["(not set)", '$0', '0', '73%',],

			];

			return (
				<AutoSizer disableHeight>
					{
						({ width }) => {

							return (
								<Table
							    width={width}
							    height={140}
							    headerHeight={30}
							    rowHeight={26}
							    rowCount={table_data.length}
							    rowGetter={({ index }) => table_data[index]}
							  >
								<Column
									label='Campaign'
									dataKey='0'
									width={400}
								/>
								<Column
									width={140}
									label='Revenue'
									dataKey='1'
								/>
								<Column
									width={200}
									label='Transactions'
									dataKey='2'
								/>
								<Column
									width={100}
									label='$S'
									dataKey='3'
								/>
							  </Table>
							)
						}
					}
				</AutoSizer>
			)
		}
	}

	renderGraph(){
				let engagementData={
					labels: [
						"1"
					],
					datasets: [{
						data: [
							23
						],
						backgroundColor: [
							'#CF3'
						],
					}],
				}


				// console.log("data: ------>", this.props.graph.graph_data)

				return(
				<Bar
					data={ engagementData }
					width={230}
					options={{
						layout: {
							padding: {
									left: 0,
									right: 20,
									top: 0,
									bottom: 0
							}
						},
						legend: {
							display: false,
						},
						scales: {
							xAxes: [{
								barPercentage: 1,
								display: true,
								gridLines: {
									display: true,
									color: "#636363",
									drawTicks: false,
								},
								ticks: {
									fontColor: "#fff",
									padding: 14,
            		}
							}],
							yAxes: [{
								display: true,
								gridLines: {
									display: true,
									color: "#636363",
									drawTicks: false,
								},
								ticks: {
									fontColor: "#fff",
									padding: 10,
								}
							}]
						}
					}}
				/>
			)
	}

	renderComponent()
	{
		if( this.props.loading_graph )
		{
			return( <Card interactive={true} elevation={Elevation.TWO} className="no-pad-t"><Spinner /></Card> )
		}
		else
		{
		return(

			<Card interactive={true} elevation={Elevation.TWO} className="no-pad-t">
				<div className="subtitle">
					Campaign

					<div className="question-tooltip question-tooltip__right">
            <Tooltip content="Browser Tooltip" position={Position.BOTTOM}>
              <div><i className="fas fa-question"></i></div>
            </Tooltip>
          </div>

				</div>

				<div className="chart-container">
					{ this.renderGraph() }
				</div>
				{ this.renderTable() }
			</Card>

			);
		}
	}

  render () {

    return(
        <div>{ this.renderComponent() }</div>
    );
  }


}

function mapStateToProps( state )
{
  return {
		graph: state.graph.graph,
  }
}

export default connect(mapStateToProps, { fetchSocialBestCats, fetchSocialBestCatsGraph })( Campaign );
