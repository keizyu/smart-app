import React, { Component } from 'react'
import { Card, Elevation, Position, Tooltip } from "@blueprintjs/core"
import { Column, Table, AutoSizer } from 'react-virtualized'
import { connect } from 'react-redux'
import numeral from 'numeral'

class LocationComponent extends Component {

  constructor(props)
  {
      super(props);
      this.state = {
        page_fans_country: [
          {country: "MX", fans: 8765},
          {country: "US", fans: 33},
          {country: "CA", fans: 12},
          {country: "ES", fans: 7},
          {country: "IN", fans: 6},
          {country: "CO", fans: 5},
          {country: "HN", fans: 3},
          {country: "BR", fans: 3},
          {country: "PE", fans: 3}
        ]
      }

  }

  renderPaises(){
      return (
        <div className="paises">
         { this.state.page_fans_country.map(function(pais){

             return (
               <div key={pais.country} className="paisesMap">
                 <img src={require('../../../assets/img/flags/'+ pais.country.toLowerCase() +'.png')} alt={pais.country}/>
               </div>

             );
         })}
       </div>
      )
  }

  render () {

    let ciudadesLista = [
      {city: "Puebla", sessions: 114},
      {city: "Heroica Veracruz", sessions: 41},
      {city: "Tlaxcalancingo", sessions: 41},
      {city: "Xalapa", sessions: 41},
      {city: "Mexico City", sessions: 41},
      {city: "Guadalajara", sessions: 41},
      {city: "Villahermosa", sessions: 41},
      {city: "Coffeyville", sessions: 41},
      {city: "Cuernavaca", sessions: 41}
    ]

    let titleStyle = {
      textAlign: 'center',
      margin: 0
    }

    return(

          <Card interactive={true} elevation={Elevation.TWO} className="no-pad-t">
            <div className="subtitle">
              Location

              <div className="question-tooltip question-tooltip__right">
                <Tooltip content="Location Tooltip" position={Position.BOTTOM}>
                  <div><i className="fas fa-question"></i></div>
                </Tooltip>
              </div>
            </div>

            <div className="grid">
              <div className="col-12">

                <h3 style={titleStyle}>Page Sessions</h3>
                <div className="subtitle subtitle__no-border followers">
                  <div className="followers-data">
                    { numeral(99).format('0,0') }
                  </div>
                </div>

              </div>

            </div>

            { this.renderPaises() }

            <AutoSizer disableHeight>
            	{
              	({ width }) => {

                	return (

                    <Table
                      width={width}
                      height={164}
                      headerHeight={26}
                      rowHeight={26}
                      rowCount={ciudadesLista.length}
                      rowGetter={({ index }) => ciudadesLista[index]}
                    >
                      <Column
                        dataKey='city'
                        width={300}
                      />
                      <Column
                        width={150}
                        dataKey='sessions'
                      />
                    </Table>

									)
              	}
            	}
            </AutoSizer>
          </Card>
    );
  }

}

function mapStateToProps( state )
{
  return {

  }
}

export default connect(mapStateToProps)( LocationComponent );
