import React, { Component } from 'react'
import { Card, Elevation, Tooltip, Position } from "@blueprintjs/core"
import { Bar } from 'react-chartjs-2'

class Age extends Component {
  render () {

    let AgeData = {
      labels: [
        '25-34',
        '35-44',
        '18-24',
        '45-54'
      ],
      datasets: [{
        data: [55, 30, 18, 16],
        backgroundColor: [
          '#CF3',
          '#39afe2',
					'#e93a52',
					'#1fc560'
        ]
      }],
    }

    return (
      <Card interactive={true} elevation={Elevation.TWO} className="no-pad-t">
        <div className="subtitle">
          Age

          <div className="question-tooltip question-tooltip__right">
            <Tooltip content="Reporte de mayor rango de edades" position={Position.BOTTOM}>
              <div><i className="fas fa-question"></i></div>
            </Tooltip>
          </div>

        </div>

        {/*<h3 style={titleStyle}>Page / Sessions</h3>*/}

        <Bar
          data={ AgeData }
          width={330}
          options={{
            legend: {
              display: false,
            },
            scales: {
              xAxes: [{
                barPercentage: 1,
                display: true,
                gridLines: {
                  display: true,
                  color: "#636363",
                  drawTicks: false,
                },
                ticks: {
                  fontColor: "#fff",
                  padding: 14,
                }
              }],
              yAxes: [{
                display: true,
                gridLines: {
                  display: true,
                  color: "#636363",
                  drawTicks: false,
                },
                ticks: {
                  fontColor: "#fff",
                  padding: 10,
                }
              }]
            }
          }}
        />

      </Card>
    )

  }
}

export default Age;
