import React, { Component } from 'react'
import { Card, Elevation } from "@blueprintjs/core"
import { Pie } from 'react-chartjs-2'
import { connect } from 'react-redux'

class Visitors extends Component {

	renderDonut(){
			let Data = {
				labels: [
					'New',
			    '',
				],
				datasets: [{
			    label: 'Population',
					data: [9,1],
					backgroundColor: [
            '#b3ff26',
						'#2aa9e9',
			    ],
			    borderColor: '#363737',
					borderWidth: 0
				}],
		};

			return(
					<div>
						<div className="subtitle">
							Visitors
						</div>

						<Pie
							data={ Data }
							width={230}
							options={{
								legend: {
									display: false,
								}
							}}
						/>
					</div>
				);
	}

  render () {

    return(

        <Card interactive={true} elevation={Elevation.TWO} className="no-pad-t">
          { this.renderDonut() }
        </Card>

    );
  }
}

function mapStateToProps( state )
{
  return {

  }
}

export default connect(mapStateToProps)( Visitors );
