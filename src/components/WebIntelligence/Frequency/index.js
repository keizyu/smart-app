import React, { Component } from 'react'
import { Card, Elevation } from "@blueprintjs/core"
import { Pie } from 'react-chartjs-2'
import { connect } from 'react-redux'
import numeral from 'numeral'

class Frequency extends Component {

	renderDonut(){
			let Data = {
				labels: [
					'Brand 1',
			    'Brand 2',
          'Brand 3',
				],
				datasets: [{
			    label: 'Population',
					data: [9,1,1],
					backgroundColor: [
            '#b3ff26',
						'#2aa9e9',
						'#ff9615',
			    ],
			    borderColor: '#363737',
					borderWidth: 0
				}],
		};

    const titleStyle = {
      textAlign: 'center',
      margin: 0
    }

			return(
					<div>
						<div className="subtitle">
							Frequency
						</div>

            <div className="grid">
              <div className="col-12">

                <h3 style={titleStyle}>Page Views</h3>
                <div className="subtitle subtitle__no-border followers">
                  <div className="followers-data">
                    { numeral(99).format('0,0') }
                  </div>
                </div>

              </div>

            </div>

						<Pie
							data={ Data }
							width={230}
							options={{
								legend: {
									display: false,
								}
							}}
						/>
					</div>
				);
	}

  render () {

    return(

        <Card interactive={true} elevation={Elevation.TWO} className="no-pad-t">
          { this.renderDonut() }
        </Card>

    );
  }
}

function mapStateToProps( state )
{
  return {

  }
}

export default connect(mapStateToProps)( Frequency );
