import React, { Component } from 'react'
import { Card, Elevation, Tooltip, Position } from "@blueprintjs/core"
// import { Column, Table, AutoSizer } from 'react-virtualized'
import { Bar } from 'react-chartjs-2'
import { connect } from 'react-redux'
import { Spinner } from '@blueprintjs/core'

class Brand extends Component {

	constructor(props){
		super(props);
		this.state = {
			engagementData : {
				labels: [],
				datasets: [{
					data: [],
					backgroundColor: [],
				}],
			}
		}
	}

	renderGraph()
	{

				// let renderTableData = this.props.graph.graph_data;

				let engagementData={
					labels: [
						"Apple",
						"Samsung",
            "Huawei",
            "(not set)",
            "Motorola",
            "Sony"
					],
					datasets: [{
						data: [
							60,
							30,
              10,
              10,
              10,
              10
						],
						backgroundColor: [
              '#CF3',
              '#39afe2',
    					'#e93a52',
    					'#1fc560',
              "#ff984c",
              "ab2bf7"
						],
					}],
				}


				// console.log("data: ------>", this.props.graph.graph_data)

				return(
				<Bar
					data={ engagementData }
					width={280}
					options={{
						layout: {
							padding: {
									left: 0,
									right: 20,
									top: 0,
									bottom: 0
							}
						},
						legend: {
							display: false,
						},
						scales: {
							xAxes: [{
								barPercentage: 1,
								display: true,
								gridLines: {
									display: true,
									color: "#636363",
									drawTicks: false,
								},
								ticks: {
									fontColor: "#fff",
									padding: 14,
									callback: function(value) {
	                    return value.substr(0,6); //truncate
	               	}
            		}
							}],
							yAxes: [{
								display: true,
								gridLines: {
									display: true,
									color: "#636363",
									drawTicks: false,
								},
								ticks: {
									fontColor: "#fff",
									padding: 10,
									fontSize: 9,
									beginAtZero:true
								}
							}]
						}
					}}
				/>
			)
	}

	renderComponent()
	{
		if( this.props.loading_graph )
		{
			return( <Card interactive={true} elevation={Elevation.TWO} className="no-pad-t"><Spinner /></Card> )
		}
		else
		{
		return(

			<Card interactive={true} elevation={Elevation.TWO} className="no-pad-t">
				<div className="subtitle">
					Brand
					<div className="question-tooltip question-tooltip__right">
            <Tooltip content="Brand Tooltip" position={Position.BOTTOM}>
              <div><i className="fas fa-question"></i></div>
            </Tooltip>
          </div>
				</div>

				<div className="chart-container">
					{ this.renderGraph() }
				</div>
			</Card>

			);
		}
	}

  render () {

    return(
        <div>{ this.renderComponent() }</div>
    );
  }


}

function mapStateToProps( state )
{
  return {

  }
}

export default connect(mapStateToProps)( Brand );
