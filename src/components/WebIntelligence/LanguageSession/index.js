import React, { Component } from 'react'
import { Card, Elevation, Tooltip, Position } from "@blueprintjs/core"
import { Bar } from 'react-chartjs-2'

class LanguageSession extends Component {
  render () {

    let LanguageSessionData = {
      labels: [
        'es',
        'en'
      ],
      datasets: [{
        data: [4, 2],
        backgroundColor: [
          '#CF3',
          '#39afe2'
        ]
      }],
    }

    let titleStyle = {
      textAlign: 'center'
    }

    return (
      <Card interactive={true} elevation={Elevation.TWO} className="no-pad-t">
        <div className="subtitle">
          Language

          <div className="question-tooltip question-tooltip__right">
            <Tooltip content="Language Tooltip" position={Position.BOTTOM}>
              <div><i className="fas fa-question"></i></div>
            </Tooltip>
          </div>

        </div>

        <h3 style={titleStyle}>Page / Sessions</h3>

        <Bar
          data={ LanguageSessionData }
          width={330}
          options={{
            legend: {
              display: false,
            },
            scales: {
              xAxes: [{
                barPercentage: 1,
                display: true,
                gridLines: {
                  display: true,
                  color: "#636363",
                  drawTicks: false,
                },
                ticks: {
                  fontColor: "#fff",
                  padding: 14,
                }
              }],
              yAxes: [{
                display: true,
                gridLines: {
                  display: true,
                  color: "#636363",
                  drawTicks: false,
                },
                ticks: {
                  fontColor: "#fff",
                  padding: 10,
                  beginAtZero:true
                }
              }]
            }
          }}
        />

      </Card>
    )

  }
}

export default LanguageSession;
