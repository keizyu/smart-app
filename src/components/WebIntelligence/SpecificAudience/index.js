import React, { Component } from 'react'
import { Card, Elevation, Tooltip, Position } from "@blueprintjs/core"
import { Column, Table, AutoSizer } from 'react-virtualized'
import { Bar } from 'react-chartjs-2'
import { connect } from 'react-redux'
import { Spinner } from '@blueprintjs/core'

class SpecificAudience extends Component {

	constructor(props){
		super(props);
		this.state = {
			engagementData : {
				labels: [],
				datasets: [{
					data: [],
					backgroundColor: [],
				}],
			}
		}
	}

	componentWillMount()
  {

  }

	renderTable()
	{
		if( this.props.loading_graph )
		{
			return( <Spinner /> )
		}
		else
		{
      let table_data = [
				["Travel/Hotels & Accommodations	", 100],
				["Autos & Vehicles/Motor Vehicles/Motor Vehicles (Used)", 77],
        ["Autos & Vehicles/Vehicles (Other)/Bicycles & Accessories	", 77],
        ["Travel/Trips by Destination/Trips to North America", 77],
        ["Business Services/Advertising & Marketing Services	", 77],
        ["Travel/Trips by Destination/Trips to Latin America/Trips to Mexico	", 77],
				["Education/Post-Secondary Education	", 77],
				["Employment	", 77],
				["Software/Business & Productivity Software		", 77],
				["Autos & Vehicles/Motor Vehicles/Motor Vehicles by Brand/Volkswagen	", 77]
			];

			return (
				<AutoSizer disableHeight>
					{
						({ width }) => {

							return (
								<Table
							    width={width}
							    height={200}
							    headerHeight={30}
							    rowHeight={26}
							    rowCount={table_data.length}
							    rowGetter={({ index }) => table_data[index]}
							  >
							    <Column
							      label='In-Market Segment'
							      dataKey='0'
							      width={width / 2}
							    />
							    <Column
							      width={width / 2}
							      label='Sessions'
							      dataKey='1'
							    />
							  </Table>
							)
						}
					}
				</AutoSizer>
			)
		}
	}

	renderGraph()
	{

				// let renderTableData = this.props.graph.graph_data;

				let engagementData={
					labels: [
						"1",
						"2",
            "3",
            "4",
            "5",
            "6"
					],
					datasets: [{
						data: [
							27,
							23,
							22,
              22,
              21,
              21
						],
						backgroundColor: [
              '#CF3',
              '#39afe2',
    					'#e93a52',
    					'#1fc560',
              "#ff984c",
              "ab2bf7"
						],
					}],
				}


				// console.log("data: ------>", this.props.graph.graph_data)

				return(
				<Bar
					data={ engagementData }
					width={280}
					options={{
						layout: {
							padding: {
									left: 0,
									right: 20,
									top: 0,
									bottom: 0
							}
						},
						legend: {
							display: false,
						},
						scales: {
							xAxes: [{
								barPercentage: 1,
								display: true,
								gridLines: {
									display: true,
									color: "#636363",
									drawTicks: false,
								},
								ticks: {
									fontColor: "#fff",
									padding: 14
            		}
							}],
							yAxes: [{
								display: true,
								gridLines: {
									display: true,
									color: "#636363",
									drawTicks: false,
								},
								ticks: {
									fontColor: "#fff",
									padding: 10,
								}
							}]
						}
					}}
				/>
			)
	}

	renderComponent()
	{
		if( this.props.loading_graph )
		{
			return( <Card interactive={true} elevation={Elevation.TWO} className="no-pad-t"><Spinner /></Card> )
		}
		else
		{
		return(

			<Card interactive={true} elevation={Elevation.TWO} className="no-pad-t">
				<div className="subtitle">
					Specific Audience
					<div className="question-tooltip question-tooltip__right">
            <Tooltip content="SpecificAudience Tooltip" position={Position.BOTTOM}>
              <div><i className="fas fa-question"></i></div>
            </Tooltip>
          </div>
				</div>

				<div className="chart-container">
					{ this.renderGraph() }
				</div>
				{ this.renderTable() }
			</Card>

			);
		}
	}

  render () {

    return(
        <div>{ this.renderComponent() }</div>
    );
  }


}

function mapStateToProps( state )
{
  return {

  }
}

export default connect(mapStateToProps)( SpecificAudience );
