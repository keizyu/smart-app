import React, { Component } from 'react'
import { Card, Elevation, Tooltip, Position } from "@blueprintjs/core"
import { Column, Table, AutoSizer } from 'react-virtualized'
import { Bar } from 'react-chartjs-2'
import { connect } from 'react-redux'
import { Spinner } from '@blueprintjs/core'

class Region extends Component {

	constructor(props){
		super(props);
		this.state = {
			engagementData : {
				labels: [],
				datasets: [{
					data: [],
					backgroundColor: [],
				}],
			}
		}
	}

	componentWillMount()
  {

  }

	renderTable()
	{
		if( this.props.loading_graph )
		{
			return( <Spinner /> )
		}
		else
		{
      let table_data = [
				["Sinaloa", '$0', '0', '73%',],
				["Berlin", '$0', '0', '73%',],
				["Puebla", '$0', '0', '73%',],
				["Nuevo Leon", '$0', '0', '73%',],
				["Pennsylvania", '$0', '0', '73%',],
				["Queretaro", '$0', '0', '73%',],
				["Santiago Metropolitan Region	", '$0', '0', '73%',],
				["Split-Dalmatia County	", '$0', '0', '73%',],
				["Tamaulipas	", '$0', '0', '73%',],
				["Texas	", '$0', '0', '73%',]

			];

			return (
				<AutoSizer disableHeight>
					{
						({ width }) => {

							return (
								<Table
							    width={width}
							    height={200}
							    headerHeight={30}
							    rowHeight={26}
							    rowCount={table_data.length}
							    rowGetter={({ index }) => table_data[index]}
							  >
							    <Column
							      label='Region'
							      dataKey='0'
							      width={400}
							    />
							    <Column
							      width={140}
							      label='Revenue'
							      dataKey='1'
							    />
									<Column
										width={200}
										label='Transactions'
										dataKey='2'
									/>
									<Column
										width={100}
										label='$S'
										dataKey='3'
									/>
							  </Table>
							)
						}
					}
				</AutoSizer>
			)
		}
	}

	renderGraph()
	{

				// let renderTableData = this.props.graph.graph_data;

				let engagementData={
					labels: [
						"Sinaloa",
						"Berlin",
            "Puebla",
            "Nuevo Leon",
            "Pennsylvania",
            "Queretaro",
					],
					datasets: [{
						data: [
							73,
							23,
              22,
              17,
              17,
              17
						],
						backgroundColor: [
              '#CF3',
              '#39afe2',
    					'#e93a52',
    					'#1fc560',
              "#ff984c",
              "ab2bf7"
						],
					}],
				}


				// console.log("data: ------>", this.props.graph.graph_data)

				return(
				<Bar
					data={ engagementData }
					width={280}
					options={{
						layout: {
							padding: {
									left: 0,
									right: 20,
									top: 0,
									bottom: 0
							}
						},
						legend: {
							display: false,
						},
						scales: {
							xAxes: [{
								barPercentage: 1,
								display: true,
								gridLines: {
									display: true,
									color: "#636363",
									drawTicks: false,
								},
								ticks: {
									fontColor: "#fff",
									padding: 14,
									callback: function(value) {
	                    return value.substr(0,6); //truncate
	               	}
            		}
							}],
							yAxes: [{
								display: true,
								gridLines: {
									display: true,
									color: "#636363",
									drawTicks: false,
								},
								ticks: {
									fontColor: "#fff",
									padding: 10,
									beginAtZero:true
								}
							}]
						}
					}}
				/>
			)
	}

	renderComponent()
	{
		if( this.props.loading_graph )
		{
			return( <Card interactive={true} elevation={Elevation.TWO} className="no-pad-t"><Spinner /></Card> )
		}
		else
		{
		return(

			<Card interactive={true} elevation={Elevation.TWO} className="no-pad-t">
				<div className="subtitle">
					Region
					<div className="question-tooltip question-tooltip__right">
            <Tooltip content="Region Tooltip" position={Position.BOTTOM}>
              <div><i className="fas fa-question"></i></div>
            </Tooltip>
          </div>
				</div>

				<div className="chart-container">
					{ this.renderGraph() }
				</div>
				{ this.renderTable() }
			</Card>

			);
		}
	}

  render () {

    return(
        <div>{ this.renderComponent() }</div>
    );
  }


}

function mapStateToProps( state )
{
  return {

  }
}

export default connect(mapStateToProps)( Region );
