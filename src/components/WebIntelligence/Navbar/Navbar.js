import React, { Component } from 'react';
import { connect } from 'react-redux';

// import Cookies from 'universal-cookie';

class Navbar extends Component {

  constructor(props) {
    super(props)
    this.state = { }
  }

  render () {

    return(
      <div className="navbarContainer">

        <ul className="menu bp3-list-unstyled">
          <li className="menuItem">
            <i className="far fa-user"></i>
            <div className="menuItemOnHover">Audience</div>
          </li>

          <li className="menuItem">
            <i className="far fa-money-bill-alt"></i>
            <div className="menuItemOnHover">Conversion</div>
          </li>

          <li className="menuItem">
            <i className="far fa-check-square"></i>
            <div className="menuItemOnHover">Goals</div>
          </li>

        </ul>

      </div>
    )
  }
}

function mapStateToProps( state )
{
  return {
  }
}

export default connect(mapStateToProps)( Navbar );
