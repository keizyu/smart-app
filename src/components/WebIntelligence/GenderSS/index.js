import React, { Component } from 'react'
import { Card, Elevation, Tooltip, Position } from "@blueprintjs/core"
import { Column, Table, AutoSizer } from 'react-virtualized'
// import { Bar } from 'react-chartjs-2'
import { connect } from 'react-redux'
import { Spinner } from '@blueprintjs/core'

class GenderSS extends Component {

	renderTable()
	{
		if( this.props.loading_graph )
		{
			return( <Spinner /> )
		}
		else
		{
      let table_data = [
				["female", '$0', '0', '73%',],
				["male", '$0', '0', '73%',],
			];

			return (
				<AutoSizer disableHeight>
					{
						({ width }) => {

							return (
								<Table
							    width={width}
							    height={200}
							    headerHeight={30}
							    rowHeight={26}
							    rowCount={table_data.length}
							    rowGetter={({ index }) => table_data[index]}
							  >
							    <Column
							      label='Gender'
							      dataKey='0'
							      width={400}
							    />
							    <Column
							      width={140}
							      label='Revenue'
							      dataKey='1'
							    />
									<Column
										width={200}
										label='Transactions'
										dataKey='2'
									/>
									<Column
										width={100}
										label='$S'
										dataKey='3'
									/>
							  </Table>
							)
						}
					}
				</AutoSizer>
			)
		}
	}

	renderComponent()
	{
		if( this.props.loading_graph )
		{
			return( <Card interactive={true} elevation={Elevation.TWO} className="no-pad-t"><Spinner /></Card> )
		}
		else
		{
		let genderWidth = {
			maleWidth:{
				width: '50%'
			},
			femaleWidth:{
				width: '50%',
			}
		}
		return(

			<Card interactive={true} elevation={Elevation.TWO} className="no-pad-t">
				<div className="subtitle">
					Gender
					<div className="question-tooltip question-tooltip__right">
            <Tooltip content="GenderSS Tooltip" position={Position.BOTTOM}>
              <div><i className="fas fa-question"></i></div>
            </Tooltip>
          </div>
				</div>

				<div className="chart-container gender">
						<div className="gender__item gender__item-female" style={ genderWidth.femaleWidth }>
							<div className="flex-holder">
								<div className="gender__item--icon">
									<i className="fas fa-female fa-4x"></i>
								</div>

								<span className="gender__item--data">
									{ genderWidth.femaleWidth.width }
								</span>
						</div>
						</div>


						<div className="gender__item gender__item-male"  style={ genderWidth.maleWidth }>
							<div className="flex-holder">
								<div className="gender__item--icon">
									<i className="fas fa-male fa-4x"></i>
								</div>
								<span className="gender__item--data">
									{ genderWidth.maleWidth.width }
								</span>
							</div>
						</div>

				</div>

				{ this.renderTable() }
			</Card>

			);
		}
	}

  render () {

    return(
        <div>{ this.renderComponent() }</div>
    );
  }


}

function mapStateToProps( state )
{
  return {

  }
}

export default connect(mapStateToProps)( GenderSS );
