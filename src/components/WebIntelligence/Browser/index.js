import React, { Component } from 'react'
import { Card, Elevation, Tooltip, Position } from "@blueprintjs/core"
import { Column, Table, AutoSizer } from 'react-virtualized'
import { Bar } from 'react-chartjs-2'
import { connect } from 'react-redux'
import { Spinner } from '@blueprintjs/core'

class Browser extends Component {

	constructor(props){
		super(props);
		this.state = {
			engagementData : {
				labels: [],
				datasets: [{
					data: [],
					backgroundColor: [],
				}],
			}
		}
	}

	componentWillMount()
  {

  }

	renderTable()
	{
		if( this.props.loading_graph )
		{
			return( <Spinner /> )
		}
		else
		{
      let table_data = [
				["Chrome", 331],
				["Safari", 15],
        ["Internet Explorer", 10],
        ["Firefox", 9],
        ["Edge", 8],
        ["Safari (in-app)", 8]
			];

			return (
				<AutoSizer disableHeight>
					{
						({ width }) => {

							return (
								<Table
							    width={width}
							    height={200}
							    headerHeight={30}
							    rowHeight={26}
							    rowCount={table_data.length}
							    rowGetter={({ index }) => table_data[index]}
							  >
							    <Column
							      label='Browser'
							      dataKey='0'
							      width={width / 2}
							    />
							    <Column
							      width={width / 2}
							      label='Pageviews'
							      dataKey='1'
							    />
							  </Table>
							)
						}
					}
				</AutoSizer>
			)
		}
	}

	renderGraph()
	{

				// let renderTableData = this.props.graph.graph_data;

				let engagementData={
					labels: [
						"Chrome",
						"Safari",
            "Internet",
            "Firefox",
            "Safari",
            "Edge"
					],
					datasets: [{
						data: [
							230,
							40,
              10,
              10,
              10,
              10
						],
						backgroundColor: [
              '#CF3',
              '#39afe2',
    					'#e93a52',
    					'#1fc560',
              "#ff984c",
              "ab2bf7"
						],
					}],
				}


				// console.log("data: ------>", this.props.graph.graph_data)

				return(
				<Bar
					data={ engagementData }
					width={280}
					options={{
						layout: {
							padding: {
									left: 0,
									right: 20,
									top: 0,
									bottom: 0
							}
						},
						legend: {
							display: false,
						},
						scales: {
							xAxes: [{
								barPercentage: 1,
								display: true,
								gridLines: {
									display: true,
									color: "#636363",
									drawTicks: false,
								},
								ticks: {
									fontColor: "#fff",
									padding: 14
            		}
							}],
							yAxes: [{
								display: true,
								gridLines: {
									display: true,
									color: "#636363",
									drawTicks: false,
								},
								ticks: {
									fontColor: "#fff",
									padding: 10,
								}
							}]
						}
					}}
				/>
			)
	}

	renderComponent()
	{
		if( this.props.loading_graph )
		{
			return( <Card interactive={true} elevation={Elevation.TWO} className="no-pad-t"><Spinner /></Card> )
		}
		else
		{
		return(

			<Card interactive={true} elevation={Elevation.TWO} className="no-pad-t">
				<div className="subtitle">
					Browser
					<div className="question-tooltip question-tooltip__right">
            <Tooltip content="Browser Tooltip" position={Position.BOTTOM}>
              <div><i className="fas fa-question"></i></div>
            </Tooltip>
          </div>
				</div>

				<div className="chart-container">
					{ this.renderGraph() }
				</div>
				{ this.renderTable() }
			</Card>

			);
		}
	}

  render () {

    return(
        <div>{ this.renderComponent() }</div>
    );
  }


}

function mapStateToProps( state )
{
  return {

  }
}

export default connect(mapStateToProps)( Browser );
