import React, { Component } from 'react';
import { Card, Elevation, Tooltip, Position } from "@blueprintjs/core";
import { connect } from 'react-redux';
import numeral from 'numeral'


class SiteSpeed extends Component {

  render () {

      return(
          <Card interactive={true} elevation={Elevation.TWO} className="no-pad-t">
            <div className="subtitle">
              Site Speed

              <div className="question-tooltip question-tooltip__right">
                <Tooltip content="Browser Tooltip" position={Position.BOTTOM}>
                  <div><i className="fas fa-question"></i></div>
                </Tooltip>
              </div>

            </div>
            <div className="total-activity">

              <div className="flex-holder">

                <div className="total-activity__item total-activity__item--impressions webIntelligenceIconItem">
                  <div className="total-activity__icon webIntelligenceIcon" >
                    <i className="far fa-dot-circle"></i>
                  </div>
                  <div className="total-activity__text">
                    <h3 className="total-activity__text--">
                      Page Load <br />
                      { numeral(238).format('00:00:00') }
                    </h3>
                  </div>
                </div>

                <div className="total-activity__item total-activity__item--video webIntelligenceIconItem">
                  <div className="total-activity__icon webIntelligenceIcon">
                    <i className="fas fa-search"></i>
                  </div>
                  <div className="total-activity__text">
                    <h3 className="total-activity__text--">
                    Domain Lookup <br />
                      { numeral(238).format('00:00:00') }
                    </h3>
                  </div>
                </div>

                <div className="total-activity__item total-activity__item--engagement webIntelligenceIconItem">
                  <div className="total-activity__icon webIntelligenceIcon">
                    <i className="fas fa-cloud-download-alt"></i>
                  </div>
                  <div className="total-activity__text">
                    <h3 className="total-activity__text--">
                    Page Download <br />
                      { numeral(238).format('00:00:00') }
                    </h3>
                  </div>
                </div>

                <div className="total-activity__item total-activity__item--posts webIntelligenceIconItem">
                  <div className="total-activity__icon webIntelligenceIcon" style={{backgroundColor: '#3cc453'}} >
                    <i className="fas fa-server"></i>
                  </div>
                  <div className="total-activity__text">
                    <h3 className="total-activity__text--">
                    Server Connection <br />
                      { numeral(238).format('00:00:00') }
                    </h3>
                  </div>
                </div>



              </div>
            </div>
          </Card>
      );
  }
}


function mapStateToProps( state )
{
  return {
  }
}

export default connect(mapStateToProps)( SiteSpeed );
