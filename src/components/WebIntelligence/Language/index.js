import React, { Component } from 'react'
import { Card, Elevation, Tooltip, Position } from "@blueprintjs/core"

class Language extends Component {
  render () {

    let genderWidth = {
			maleWidth:{
				width: '50%',
        backgroundColor: '#39afe2'
			},
			femaleWidth:{
				width: '50%',
        backgroundColor: '#CF3',
        color: '#000'
			}
		}

    return (
      <Card interactive={true} elevation={Elevation.TWO} className="no-pad-t">
        <div className="subtitle">
          Language

          <div className="question-tooltip question-tooltip__right">
            <Tooltip content="SpecificAudience Tooltip" position={Position.BOTTOM}>
              <div><i className="fas fa-question"></i></div>
            </Tooltip>
          </div>

        </div>

        <div className="chart-container gender">
            <div className="gender__item gender__item-female" style={ genderWidth.femaleWidth }>
              <div className="flex-holder">
                <span className="gender__item--">
                  { genderWidth.femaleWidth.width }
                </span>
              </div>
            </div>
            <div className="gender__item gender__item-male"  style={ genderWidth.maleWidth }>
              <div className="flex-holder">
                <span className="gender__item--">
                  { genderWidth.maleWidth.width }
                </span>
              </div>
            </div>
        </div>


      </Card>
    )

  }
}

export default Language;
