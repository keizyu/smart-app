import React, { Component } from 'react'
import { Card, Elevation, H6  } from "@blueprintjs/core"
import { connect } from 'react-redux'
import numeral from 'numeral'
import Swiper from 'react-id-swiper'

class SalesScore extends Component {
  constructor(props)
  {
      super(props);
      this.state = {
          selectedType: 'all',
          selectedPost: 0,
          selectedCat: 0,
          posts: [

          ]
      }

  }

  numberFormat(num, sign = '') {
    const pieces = parseFloat(num).toFixed(2).split('')
    let ii = pieces.length - 3
    while ((ii-=3) > 0) {
      pieces.splice(ii, 0, ',')
    }
    return (sign + pieces.join('')).split(".")[0]
  }


  renderPost(){
    return (
      <div className="post" style={{height:'100%', color: '#CF3'}}>
          <H6 style={{ color: '#CF3', textAlign: 'center' }}>Keyword</H6>
          <ol>
            <li>(not provided)</li>
            <li>(not set)</li>
            <li>Tienda de bicicletas en veracruz</li>
            <li>mtbike</li>
          </ol>
       </div>
    )
  }

  renderBest()
  {
    // let _cats = this.props.cats;

      return(
        <div>
          <div className="category-rank__item category-rank__item-active">

            <div className="category-rank__container">
              <div className="category-rank__item--title">Organic Search</div>
              <div className="category-rank__item--data">
                23%
              </div>
              <div className="category-rank__item--rank">#1</div>
            </div>

          </div>

          <div className="category-rank__item">

            <div className="category-rank__container">
              <div className="category-rank__item--title">Direct</div>
              <div className="category-rank__item--data">
                23%
              </div>
              <div className="category-rank__item--rank">#1</div>
            </div>

          </div>

          <div className="category-rank__item">

            <div className="category-rank__container">
              <div className="category-rank__item--title">Social</div>
              <div className="category-rank__item--data">
                23%
              </div>
              <div className="category-rank__item--rank">#1</div>
            </div>

          </div>

          <div className="category-rank__item">

            <div className="category-rank__container">
              <div className="category-rank__item--title">Referral</div>
              <div className="category-rank__item--data">
                23%
              </div>
              <div className="category-rank__item--rank">#1</div>
            </div>

          </div>
        </div>
      )
  }


  render () {

    return(

        <div className="ads-shell">

            <Card interactive={true} elevation={Elevation.TWO} className="no-pad-t">

                <div className="grid">
                  <div className="col-12">
                    <div className="subtitle">
                      Sales Score
                    </div>
                  </div>
                  <div className="col-4_sm-12">
                    <div className="category-rank">
                        { this.renderBest() }
                    </div>
                  </div>

                  <div className="col-4_sm-12">
                    <div className="category-data ads-data">

                      <div className="grid">

                        <div className="col-6">

                          <div className="category-data__item">
                            <div className="category-data__item--data">
                              { numeral(157).format('0,0') }
                            </div>
                            <div className="category-data__item--title">Sessions</div>
                          </div>

                        </div>

                        <div className="col-6">

                          <div className="category-data__item">
                            <div className="category-data__item--data">
                              { numeral(.2070).format('0.0%') }
                            </div>
                            <div className="category-data__item--title">% New Sessions</div>
                          </div>

                        </div>
                      </div>

                      <div className="grid">

                        <div className="col-6">

                          <div className="category-data__item">
                            <div className="category-data__item--data">
                              { numeral(1.34).format('0.00') }
                            </div>
                            <div className="category-data__item--title">Page / Sessions</div>
                          </div>

                        </div>

                        <div className="col-6">

                          <div className="category-data__item">
                            <div className="category-data__item--data">
                              { numeral(2.3).format('00:00:00') }
                            </div>
                            <div className="category-data__item--title">Session Duration</div>
                          </div>

                        </div>

                      </div>

                      <div className="grid">

                        <div className="col-6">

                          <div className="category-data__item">
                            <div className="category-data__item--data">
                              { numeral(448).format('0,0') }
                            </div>
                            <div className="category-data__item--title">Transactions</div>
                          </div>

                        </div>

                        <div className="col-6">

                          <div className="category-data__item">
                            <div className="category-data__item--data">
                              { numeral(0.241).format('$0,0') }
                            </div>
                            <div className="category-data__item--title">Revenue</div>
                          </div>

                        </div>

                      </div>


                    </div>
                  </div>

                  <div className="col-4_sm-12">
                    <div className="category-post" style={{height: '94%'}}>
                      { this.renderPost() }
                    </div>
                  </div>

                </div>

            </Card>

        </div>

    );
  }
}

function mapStateToProps( state )
{
  return {

  }
}

export default connect(mapStateToProps)( SalesScore );
