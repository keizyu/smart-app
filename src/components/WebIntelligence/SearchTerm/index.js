import React, { Component } from 'react'
import { Card, Elevation, Tooltip, Position } from "@blueprintjs/core"
import { connect } from 'react-redux'

class SeacrhTerm extends Component {

  render () {

    return(

        <Card interactive={true} elevation={Elevation.TWO} className="no-pad-t">
					<div className="subtitle">
						Seacrh Term

						<div className="question-tooltip question-tooltip__right">
	            <Tooltip content="SeacrhTerm Tooltip" position={Position.BOTTOM}>
	              <div><i className="fas fa-question"></i></div>
	            </Tooltip>
	          </div>


					</div>
        </Card>

    );
  }
}

function mapStateToProps( state )
{
  return {

  }
}

export default connect(mapStateToProps)( SeacrhTerm );
