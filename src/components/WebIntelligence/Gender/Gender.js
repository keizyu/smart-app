import React, { Component } from 'react'
import { Card, Elevation } from "@blueprintjs/core"

import './styles.css'

class Gender extends Component {
  render () {

    let genderWidth = {
			maleWidth:{
				width: '50%'
			},
			femaleWidth:{
				width: '50%',
			}
		}

    return (
      <Card interactive={true} elevation={Elevation.TWO} className="no-pad-t">
        <div className="subtitle">
          Gender
        </div>

        <div className="chart-container gender">
            <div className="gender__item gender__item-female" style={ genderWidth.femaleWidth }>
              <div className="flex-holder">
                <div className="gender__item--icon">
                  <i className="fas fa-female fa-4x"></i>
                </div>

                <span className="gender__item--data">
                  { genderWidth.femaleWidth.width }
                </span>
              </div>
            </div>
            <div className="gender__item gender__item-male"  style={ genderWidth.maleWidth }>
              <div className="flex-holder">
                <div className="gender__item--icon">
                  <i className="fas fa-male fa-4x"></i>
                </div>
                <span className="gender__item--data">
                  { genderWidth.maleWidth.width }
                </span>
              </div>
            </div>
        </div>


      </Card>
    )

  }
}

export default Gender;
