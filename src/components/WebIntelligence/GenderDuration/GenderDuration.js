import React, { Component } from 'react'
import { Card, Elevation } from "@blueprintjs/core"
import { Bar } from 'react-chartjs-2'

class GenderDuration extends Component {
  render () {

    let GenderDurationData = {
      labels: [
        'Female',
        'Male'
      ],
      datasets: [{
        data: [1.3, 1.3],
        backgroundColor: [
          '#CF3',
          '#39afe2'
        ]
      }],
    }

    let titleStyle = {
      textAlign: 'center'
    }

    return (
      <Card interactive={true} elevation={Elevation.TWO} className="no-pad-t">
        <div className="subtitle">
          Gender
        </div>

        <h3 style={titleStyle}>Page Duration</h3>

        <Bar
          data={ GenderDurationData }
          width={330}
          options={{
            legend: {
              display: false,
            },
            scales: {
              xAxes: [{
                barPercentage: 1,
                display: true,
                gridLines: {
                  display: true,
                  color: "#636363",
                  drawTicks: false,
                },
                ticks: {
                  fontColor: "#fff",
                  padding: 14,
                }
              }],
              yAxes: [{
                display: true,
                gridLines: {
                  display: true,
                  color: "#636363",
                  drawTicks: false,
                },
                ticks: {
                  fontColor: "#fff",
                  padding: 10,
                }
              }]
            }
          }}
        />

      </Card>
    )

  }
}

export default GenderDuration;
