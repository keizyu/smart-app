import React, { Component } from 'react'
import { Card, Elevation, Tooltip, Position } from "@blueprintjs/core"

class Devices extends Component {
  render () {

    let genderWidth = {
			laptopPercentage:{
				width: '53%',
        backgroundColor: '#39afe2'
			},
			mobilePercentage:{
				width: '42%',
        backgroundColor: '#CF3',
        color: '#36383a'
			},
      tabletPercentage:{
				width: '5%',
        backgroundColor: '#e2474f'
			}
		}

    return (
      <Card interactive={true} elevation={Elevation.TWO} className="no-pad-t">
        <div className="subtitle">
          Devices

          <div className="question-tooltip question-tooltip__right">
            <Tooltip content="SpecificAudience Tooltip" position={Position.BOTTOM}>
              <div><i className="fas fa-question"></i></div>
            </Tooltip>
          </div>

        </div>

        <div className="chart-container gender">
            <div className="gender__item gender__item-" style={ genderWidth.mobilePercentage }>
              <div className="flex-holder">
                <div className="gender__item--icon">
                  <i className="fas fa-mobile-alt fa-3x"></i>
                </div>
                <span className="gender__item--">
                  { genderWidth.mobilePercentage.width }
                </span>
              </div>
            </div>
            <div className="gender__item gender__item-"  style={ genderWidth.laptopPercentage }>
              <div className="flex-holder">
                <div className="gender__item--icon">
                  <i className="fas fa-laptop fa-3x"></i>
                </div>
                <span className="gender__item--">
                  { genderWidth.laptopPercentage.width }
                </span>
              </div>
            </div>
            <div className="gender__item gender__item-"  style={ genderWidth.tabletPercentage }>
              <div className="flex-holder">
                <div className="gender__item--icon">
                  <i className="fas fa-tablet-alt fa-3x"></i>
                </div>
                <span className="gender__item--">
                  { genderWidth.tabletPercentage.width }
                </span>
              </div>
            </div>
        </div>


      </Card>
    )

  }
}

export default Devices;
