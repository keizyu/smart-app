import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchPosts } from '../../../actions';
import { changeFilter } from '../../../actions';
import { changeOrder, fetchSocialBestCats, fetchSocialBestCatsGraph, fetchSocialPostDistribution,
          fetchSocialBestDays, fetchSocialTotalActivity, fetchBestHashtags } from '../../../actions';
import Cookies from 'universal-cookie';

import SmoothScroll from '../SmoothScroll'

import './styles.css'

class Navbar extends Component {

    constructor(props) {
      super(props);
      this.state = {
        selectedType: 'all',
        orderType: 'date',
      }
    }

    handleClick( e, isSocial=false )
    {
        if( e === this.state.selectedType )
        {
          return;
        }

        this.setState({
            selectedType: e
        });

        // si es social se llama la data
        if( isSocial )
        {
          //console.log("props de social navbar ----------->",e);
          const cookies = new Cookies();
          let range = cookies.get('range');
          this.props.fetchSocialBestCats( this.props.id, [ new Date(range[0]), new Date(range[1])], e );
          this.props.fetchSocialBestCatsGraph( this.props.id,  [ new Date(range[0]), new Date(range[1])], e );
          this.props.fetchSocialPostDistribution( this.props.id,  [ new Date(range[0]), new Date(range[1])], e );
          this.props.fetchSocialBestDays( this.props.id, [ new Date(range[0]), new Date(range[1])], e );
          this.props.fetchSocialTotalActivity( this.props.id, [ new Date(range[0]), new Date(range[1])], e );
          this.props.fetchBestHashtags( this.props.id, [ new Date(range[0]), new Date(range[1])], e );
        }

        this.props.changeFilter( e );
    }

    handleClickOrder( e )
    {
        if( e === this.state.orderType )
        {
          return;
        }

        this.setState({
            orderType: e
        });

        this.props.changeOrder( e );
    }

    renderOrderIcons(){
      if (this.props.social === true) {
        return (
          <div>
            <li className="menuItem">
              <SmoothScroll href='#audienceTarget'>
                <i className="fas fa-comments"></i>
                <div className="menuItemOnHover">Comm Strategy</div>
              </SmoothScroll>
            </li>
            <li className="menuItem">
              <i className="far fa-user"></i>
              <div className="menuItemOnHover">Audience</div>
            </li>
            <li className="menuItem">
              <i className="fas fa-assistive-listening-systems"></i>
              <div className="menuItemOnHover">Topic Data</div>
            </li>
            <li className="menuItem">
              <i className="far fa-star"></i>
              <div className="menuItemOnHover">Top Performers</div>
            </li>
          </div>
        )
      } else {
        return (
          <div>
            <li className="menuItem" onClick={ () => { this.handleClickOrder('date') } }>
              <i className={ this.state.orderType ==='date' ? "far fa-calendar-alt selectedItem" : "far fa-calendar-alt" }></i>
              <div className="menuItemOnHover">Day</div>
            </li>

            <li className="menuItem" onClick={ () => { this.handleClickOrder('eng-score') } }>
              <i className={ this.state.orderType==='eng-score' ? "far fa-heart selectedItem" : "far fa-heart" }></i>
              <div className="menuItemOnHover">ENG Score</div>
            </li>

            <li className="menuItem" onClick={ () => { this.handleClickOrder('eng-score--organic') } }>
              <i className={ this.state.orderType==='eng-score--organic' ? "fas fa-leaf selectedItem" : "fas fa-leaf" }></i>
              <div className="menuItemOnHover">ENG Score Organic</div>
            </li>

            <li className="menuItem" onClick={ () => { this.handleClickOrder('eng-score--paid') } }>
              <i className={ this.state.orderType==='eng-score--paid' ? "far fa-money-bill-alt selectedItem" : "far fa-money-bill-alt" }></i>
              <div className="menuItemOnHover">ENG Score Paid</div>
            </li>

            <li className="menuItem" onClick={ () => { this.handleClickOrder('reach-organic') } }>
              <i className={ this.state.orderType==='reach-organic' ? "fas fa-sitemap selectedItem" : "fas fa-sitemap" }></i>
              <div className="menuItemOnHover">Reach Organic</div>
            </li>

            <li className="menuItem" onClick={ () => { this.handleClickOrder('reach-paid') } }>
              <i className={ this.state.orderType==='reach-paid' ? "fas fa-globe-americas selectedItem" : "fas fa-globe-americas" }></i>
              <div className="menuItemOnHover">Reach Paid</div>
            </li>

            <li className="menuItem" onClick={ () => { this.handleClickOrder('category') } }>
              <i className={ this.state.orderType==='category' ? "fas fa-minus-circle selectedItem" : "fas fa-minus-circle" }></i>
              <div className="menuItemOnHover">No Category</div>
            </li>

            <li className="menuItem" onClick={ () => { this.handleClickOrder('interactions') } }>
              <i className={ this.state.orderType==='interactions' ? "fab fa-wpexplorer selectedItem" : "fab fa-wpexplorer" }></i>
              <div className="menuItemOnHover">Interactions</div>
            </li>

            <li className="menuItem" onClick={ () => { this.handleClickOrder('campaign-ads') } }>
              <i className={ this.state.orderType==='campaign-ads' ? "fas fa-dollar-sign selectedItem" : "fas fa-dollar-sign" }></i>
              <div className="menuItemOnHover">Campaign AD<small>s</small></div>
            </li>
          </div>
        )
      }
    }

  render () {

    console.log('FILTROS PROPS --->',this.props.social)

    return(
      <div className="navbar-container">

        <ul className="menu bp3-list-unstyled">
          <li className="menuItem" onClick={ () => { this.handleClick('facebook', this.props.social) } }>
            <i className={ this.state.selectedType==='facebook' ? "fab fa-facebook-f selected-item" : "fab fa-facebook-f" }></i>
          </li>

          <li className="menuItem" onClick={ () => { this.handleClick('instagram', this.props.social) } }>
            <i className={ this.state.selectedType==='instagram' ? "fab fa-instagram selected-item" : "fab fa-instagram" }></i>
          </li>

          <li className="menuItem" onClick={ () => { this.handleClick('twitter', this.props.social) } }>
            <i className={ this.state.selectedType==='twitter' ? "fab fa-twitter selected-item" : "fab fa-twitter" }></i>
          </li>

          <li className="menuItem" onClick={ () => { this.handleClick('linkedin', this.props.social) } }>
            <i className={ this.state.selectedType==='linkedin' ? "fab fa-linkedin-in selected-item" : "fab fa-linkedin-in" }></i>
          </li>
          <li className="menuItem" onClick={ () => { this.handleClick('all', this.props.social) } }>
            <i className={ this.state.selectedType==='all' ? "menuItemAll selectedItem" : "menuItemAll" }>ALL</i>
          </li>

          <li>
            <hr />
          </li>

          { this.renderOrderIcons() }

        </ul>

      </div>
    )
  }
}

function mapStateToProps( state )
{
  return {
    posts: state.posts,
    post_filter: state.changeFilter,
    post_order: state.changeOrder
  }
}

export default connect(mapStateToProps, { fetchPosts, changeFilter, changeOrder, fetchSocialBestCats,
              fetchSocialBestCatsGraph, fetchSocialPostDistribution, fetchSocialBestDays, fetchSocialTotalActivity,
              fetchBestHashtags})( Navbar );
