import React, { Component } from 'react';
import DashboardTop from './DashboardTop';

class TopExtras extends Component {
  render () {
    return(
      <DashboardTop id={ this.props.id } />
    );
  }
}

export default TopExtras;
