import React, { Component } from 'react';
import { Spinner, Button, H1 } from "@blueprintjs/core";
import { DateRangeInput } from "@blueprintjs/datetime";
import { fetchPosts } from '../../../actions'
import { connect } from 'react-redux';
import moment from 'moment';
import Cookies from 'universal-cookie';

import './styles.css'

class DashboardTop extends Component {
  constructor(props){
    super(props);

    this.state = {
      formatDate: null,
      range: [ new Date(moment().subtract(15, "days").format('YYYY-MM-DD')), new Date(moment().add(1, "days").format('YYYY-MM-DD'))]
    }
  }

  render () {
    if( this.props.posts.loading ) {
      return(
          <div className="spinner-container">
            <Spinner />
          </div>
      )
    }

    return(
      <div className="top-container">
        <div className="brands-added">
          <H1 className="brands-added__title">Dashboard</H1>
          <div className="searchContainer">
            <div className="dateSmart bp3-dark">
              <span className="searchCalendarIcon"><i className="far fa-calendar-alt"></i></span>
              <DateRangeInput
                  formatDate={date => moment(date).format("YYYY-MM-DD")}
                  onChange={this.handleRangeChange}
                  parseDate={str => new Date(str)}
                  value={ this.state.range  }
                  allowSingleDayRange={true}
                  maxDate= {new Date()}
              />
            <Button onClick={this.openSearchCalendar} minimal="true" className="button btn-outline" >Search</Button>
            </div>
          </div>
        </div>
      </div>
    );
  }

  openSearchCalendar = () =>  {
    // se guarda el filtro en una cookie
    const cookies = new Cookies();
    cookies.set('range', this.state.range, { path: '/' });
    this.props.fetchPosts( this.props.id, this.state.range );
  }

  handleRangeChange = (range) => {
    this.setState({ range });
    console.log(range);
  }
}

function mapStateToProps( state )
{
  return {
    posts: state.posts,
    start: state.posts.start,
    end: state.posts.end
  }
}

export default connect(mapStateToProps, { fetchPosts })( DashboardTop );
