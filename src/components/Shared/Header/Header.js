import React, { Component } from 'react';
import logoHeader from '../../../assets/img/logoTOP.png';
import { Alignment, Navbar } from "@blueprintjs/core";
import { Redirect } from 'react-router';

import './styles.css'

const header_user_info = {
  btn_logout: 'Logout'
}

class Header extends Component {
    constructor(props){
      super(props)

      this.state = {
        redirect: false
      }
    }

    redirectHandle = () => {
      this.setState({
        redirect: true
      })
    }

    renderRedirect = () => {
      if (this.state.redirect) {
        let home_route = "/home/";
        return <Redirect push to={ home_route } />;
      }
    }

    logOut = () => {
      window.localStorage.removeItem("api_access_token");
      window.location.href = "/";
    }

    render(){

      const fbResponse = JSON.parse(localStorage.getItem('resultObject'));
      const img_avatar = "https://graph.facebook.com/" + fbResponse.user.id + "/picture?type=large&width=70&height=70";

      return (

        <Navbar fixedToTop="true" className="header">

          {this.renderRedirect()}

          <Navbar.Group align={Alignment.LEFT} className="headerBrand" >
            <div className="headerBrandImage" onClick={this.redirectHandle}>
              <img src={logoHeader} alt="Smart Logo" />
            </div>
          </Navbar.Group>

          <Navbar.Group align={Alignment.RIGHT} className="headerSocial" >

            <div className="headerUser">
              <div className="headerUserUsername">{ fbResponse.user.name }</div>
              <div onClick={ this.logOut } className='cursorPointer'>{ header_user_info.btn_logout }</div>
              <div className={this.props.hide === false ? 'cursorPointer' : 'hidden'} onClick={ this.props.sidebar } >Settings</div>
            </div>

            <div className="header-user__avatar">
              <img src={ img_avatar } className="headerBrandAvatar" alt="Avatar" />
            </div>

          </Navbar.Group>

        </Navbar>

      )
    }

}

export default Header;
