import React,{ Component } from 'react'
import { Tag, Dialog, Button, Card, Classes, FormGroup, InputGroup, Icon, Elevation, Spinner } from "@blueprintjs/core"
import { connect } from 'react-redux'
import { fetchCats, addCat, fetchBrands, removeCat, fetchPosts, editCat } from '../../../actions'
import { Link } from 'react-router-dom'
import _ from 'lodash'

import './styles.css'

class SidebarContent extends Component {
  constructor(props){
    super(props)

    this.state = {
      tags: [],
      isOpen: false,
      text: '',
      rightIcon: true,
      isOpenEditTag: false,
      interactive: true,
      tagEditId: null,
      tagEdit: null,
      isOpenSettings: false,
    }
  }

  componentWillMount() {
    this.props.fetchCats( this.props.id );
    this.props.fetchBrands( );
  }

  onChange = (event) => {
      this.setState({text: event.target.value});
  }

  onChangeCat = (event) => {
      this.setState({tagEdit: event.target.value});
  }

  renderTags() {
    const tagProps = {
        large: false,
        removable: true,
        interactive: false
    }
    let removable = true;

    if( this.props.loading ) {
      return( <div>Cargando...</div> );
    }

    return this.props.categories.map(tag => {
        const onRemove = () => {
          let txtCategoria = this.state.text;
          this.props.removeCat( tag.id );

          setTimeout(() => {
              this.props.fetchPosts( this.props.id );
              alert( "The category was deleted" );
            }, 1000);
        };

        const onEdit = () => {
            //console.log( "Editar tag", tag );
        };

        removable = ( tag.id == 1 ? false : true );

        return (
            <Tag
                onRemove={removable && onRemove }
                key={tag.id}
                rightIcon={undefined}
                className=""
                {...tagProps}>
                {tag.name}

                <Button onClick={ ()=>{ this.openEditTag( tag ) }} minimal="true" ><Icon icon="edit" /></Button>
            </Tag>
        );
    });
  }

  fetchUserBrands()
  {
    console.log( "brands ->", this.props.brands );

    return this.props.brands.map( (page) => {
            return (
                <div key={ page._pagina } >
                    <img src={ page.fb_picture }  className="sidebar-avatar sidebar-avatar__brand" />
                </div>
            );
        });
  }

  fetchCurrentAccounts()
  {

    if( this.props.loading_brands )
    {
      return(
        <Spinner />
      );
    }
    else
    {
      console.log( this.props.brands );

      let that = this;
      // obtiene las páginas de de la marca actual
      that.currentAccount = _.find(this.props.brands, function( brand ) {
                                          return brand._pagina==that.props.id
                                        });
      console.log("Detalles", that.currentAccount)
      return (
        <div>Detalles</div>
        /*<div>
        { that.currentAccount.picture ? <img src={that.currentAccount.picture} className="sidebar-avatar" alt="fb avatar" /> : '' }
        { that.currentAccount.twitter_picture ? <img src={that.currentAccount.twitter_picture}  className="sidebar-avatar" alt="tw avatar" /> : '' }
        { that.currentAccount.instagram_profile_picture ? <img src={that.currentAccount.instagram_profile_picture}  className="sidebar-avatar" alt="insta avatar" /> : '' }
        </div>*/ );
    }
  }

  industrySettings() {
    return (
      <Dialog
                onClose={this.closeSettings}
                title="Settings"
                isOpen={this.state.isOpenSettings}
                className="bp3-dark industrySettingsDialog">
        <div className={Classes.DIALOG_BODY}>

          <div className="grid">
            <div className="col-4">
              <Card interactive={true} elevation={Elevation.TWO} className="no-pad-t">
                <div className="subtitle">
                  Names
                </div>

                <FormGroup
                    label="Your Brand"
                    labelFor="text-input"
                    inline={true}>
                    <InputGroup />
                </FormGroup>

                <FormGroup
                    label="Competitor 1"
                    labelFor="text-input"
                    inline={true}>
                    <InputGroup />
                </FormGroup>

                <FormGroup
                    label="Competitor 2"
                    labelFor="text-input"
                    inline={true}>
                    <InputGroup />
                </FormGroup>

                <FormGroup
                    label="Competitor 3"
                    labelFor="text-input"
                    inline={true}>
                    <InputGroup />
                </FormGroup>

              </Card>
            </div>
            <div className="col-4">

              <Card interactive={true} elevation={Elevation.TWO} className="no-pad-t">
                <div className="subtitle">
                  Facebook Handle
                </div>

                <FormGroup
                    label="Competitor 1"
                    labelFor="text-input"
                    inline={true}>
                    <InputGroup />
                </FormGroup>

                <FormGroup
                    label="Competitor 2"
                    labelFor="text-input"
                    inline={true}>
                    <InputGroup />
                </FormGroup>

                <FormGroup
                    label="Competitor 3"
                    labelFor="text-input"
                    inline={true}>
                    <InputGroup />
                </FormGroup>

              </Card>

            </div>
            <div className="col-4">

              <Card interactive={true} elevation={Elevation.TWO} className="no-pad-t">
                <div className="subtitle">
                  Instagram Handle
                </div>

                <FormGroup
                    label="Competitor 1"
                    labelFor="text-input"
                    inline={true}>
                    <InputGroup />
                </FormGroup>

                <FormGroup
                    label="Competitor 2"
                    labelFor="text-input"
                    inline={true}>
                    <InputGroup />
                </FormGroup>

                <FormGroup
                    label="Competitor 3"
                    labelFor="text-input"
                    inline={true}>
                    <InputGroup />
                </FormGroup>

              </Card>

            </div>
            <div className="col-4">
              component 4
            </div>
            <div className="col-4">
              <Card interactive={true} elevation={Elevation.TWO} className="no-pad-t">
                <div className="subtitle">
                  Twitter Handle
                </div>

                <FormGroup
                    label="Competitor 1"
                    labelFor="text-input"
                    inline={true}>
                    <InputGroup />
                </FormGroup>

                <FormGroup
                    label="Competitor 2"
                    labelFor="text-input"
                    inline={true}>
                    <InputGroup />
                </FormGroup>

                <FormGroup
                    label="Competitor 3"
                    labelFor="text-input"
                    inline={true}>
                    <InputGroup />
                </FormGroup>

              </Card>
            </div>
            <div className="col-4">
              <Card interactive={true} elevation={Elevation.TWO} className="no-pad-t">
                <div className="subtitle">
                  URL
                </div>

                <FormGroup
                    label="Competitor 1"
                    labelFor="text-input"
                    inline={true}>
                    <InputGroup />
                </FormGroup>

                <FormGroup
                    label="Competitor 2"
                    labelFor="text-input"
                    inline={true}>
                    <InputGroup />
                </FormGroup>

                <FormGroup
                    label="Competitor 3"
                    labelFor="text-input"
                    inline={true}>
                    <InputGroup />
                </FormGroup>

              </Card>
            </div>
          </div>

        </div>
        <div className={Classes.DIALOG_FOOTER}>
            <div className={Classes.DIALOG_FOOTER_ACTIONS}>
                <Button onClick={this.addBrandConfirm} minimal="true" large="true" className="button button-primary" >Save</Button>
            </div>
        </div>


      </Dialog>
    )
  }

  render (){
    const { tags, isOpen, tagEdit, isOpenEditTag, ...tagProps } = this.state;

    return (
        <div className="sidebar-shell">
          <div className="sidebar">
            <div className="sidebar-brands">
              <div className="sidebar-title">
                User Brands
              </div>
              { this.fetchUserBrands() }
            </div>

            <div className="sidebar-accounts">
              <div className="sidebar-title">
                Brand Accounts
              </div>
              <div>{ this.fetchCurrentAccounts() }</div>
            </div>

            <div className="sidebar-navegation">
              <div className="sidebar-title sidebar-title__left">
                Navegation
              </div>

              <ul className="sidebar-navegation__list bp3-list-unstyled">
                <li className="sidebar-navegation__list--item sidebar-navegation__list--item__overview"><i className="fas fa-user-circle"></i>Overview</li>
                <li className="sidebar-navegation__list--item sidebar-navegation__list--item__dashboard"><i className="fas fa-tachometer-alt"></i><Link to={`/dashboard/${this.props.id}`}>Dashboard</Link></li>
                <li className="sidebar-navegation__list--item sidebar-navegation__list--item__social"><i className="fas fa-chart-area"></i><Link to={`/social/${this.props.id}`}>Social Intelligence</Link></li>
                <li className="sidebar-navegation__list--item sidebar-navegation__list--item__ads"><i className="fas fa-dollar-sign"></i><Link to={`/ads/${this.props.id}`}>ADs Intelligence</Link></li>
                <li className="sidebar-navegation__list--item sidebar-navegation__list--item__web"><i className="fas fa-chart-bar"></i><Link to={`/web/${this.props.id}`}>Web Intelligence</Link></li>
                <li className="sidebar-navegation__list--item sidebar-navegation__list--item__web"><i className="fas fa-chart-bar"></i><Link to={`/calendar/${this.props.id}`}>Calendar</Link></li>
              </ul>
            </div>

            <div className="sidebar-categories">
              <div className="sidebar-title sidebar-title__left">
                Categories
              </div>
              <div className="sidebar-categories__container">
                  { this.renderTags() }
                  <Button onClick={this.openAddTag} minimal="true" className="button" ><Icon icon="add" elevation={Elevation.TWO} /></Button>
              </div>
            </div>

            <div className="sidebar-optimalPosts">
              <div className="sidebar-title sidebar-title__left">
                Optimal Posts Per Month
              </div>

              <FormGroup
                  label="facebook"
                  labelFor="text-input"
                  inline={true}>
                  <InputGroup id="fb-input" />
              </FormGroup>
              <FormGroup
                  label="instagram"
                  labelFor="text-input"
                  inline={true}>
                  <InputGroup id="ins-input" />
              </FormGroup>
              <FormGroup
                  label="twitter"
                  labelFor="text-input"
                  inline={true}>
                  <InputGroup id="tw-input" />
              </FormGroup>
              <Button minimal="true" className="button button-primary sidebar-button button__save" >SAVE</Button>
            </div>

            <div className="sidebar-industry">
              <div className="sidebar-title sidebar-title__left">
                Industry Settings
              </div>
              <div className="text-center">
                <Button minimal="true" className="button button-primary button__edit" onClick={ this.openSettings }>EDIT</Button>
              </div>
            </div>

            <div className="sidebar-ads">
              <div className="sidebar-title sidebar-title__left">
                ADs Settings
              </div>
              <div className="text-center">
                <Button minimal="true" className="button button-primary button__edit" >ADD</Button>
                <Button minimal="true" className="button button-primary button__edit" >CAMPAIGNS</Button>
              </div>
            </div>

            <Dialog
                    onClose={this.closeAddBrand}
                    isOpen={ this.state.isOpen }
                    className="bp3-dark input-modal">
              <div className={Classes.DIALOG_BODY}>
              <div className="grid-center-middle">
                <div className="col-10">
                  <FormGroup
                    label="Input Category"
                    labelFor="category-input">
                    <InputGroup id="category-input" placeholder="Category"
                      onChange={ this.onChange.bind(this) } />
                  </FormGroup>
                </div>
                <div className="col-2">
                  <Button minimal="true" className="button button-primary" onClick={this.addTagConfirm}>Add</Button>
                </div>
              </div>
              </div>

            </Dialog>

            <Dialog
                    onClose={this.closeEditBrand}
                    isOpen={ this.state.isOpenEditTag }
                    className="bp3-dark input-modal">
              <div className={Classes.DIALOG_BODY}>
              <div className="grid-center-middle">
                <div className="col-10">
                  <FormGroup
                    label="Input Category"
                    labelFor="category-input">
                    <InputGroup id="category-input" placeholder="Category"
                      value={ this.state.tagEdit }
                      onChange={ this.onChangeCat.bind(this) }/>
                  </FormGroup>
                </div>
                <div className="col-2">
                  <Button minimal="true" className="button button-primary" onClick={this.editTagConfirm}>Edit</Button>
                </div>
              </div>
              </div>

            </Dialog>

            { this.industrySettings() }

          </div>

          <div className="sidebar-controls">
                <div className="sidebar-controls__btn">
                    <Link to={`/home/${this.props.id}`}>Mision Control</Link>
                </div>
                <div className="sidebar-controls__btn">
                    <a className="" onClick={ () => {
                      window.localStorage.removeItem("api_access_token");
                      window.location.href = "/";
                    } }>Logout</a>
                </div>
          </div>



        </div>
    )
  }

  openAddTag = () => this.setState({ isOpen: true });
  closeAddBrand = () => this.setState({ isOpen: false });
  addTagConfirm = () => {
    let txtCategoria = this.state.text;
    let agregada = this.props.addCat( txtCategoria, this.props.id );

    setTimeout(() => {
        this.props.fetchCats( this.props.id );
      }, 1000);

    this.setState({ isOpen: false });
  };

  editTagConfirm = () => {
    let txtCategoria = this.state.tagEdit;
    let idCategoria = this.state.tagEditId;

    let agregada = this.props.editCat( idCategoria, txtCategoria );

    setTimeout(() => {
        this.props.fetchCats( this.props.id );

      }, 500);

      setTimeout(() => {
          this.props.fetchPosts( this.props.id );

        }, 1200);

    this.setState({ isOpenEditTag: false });
  }

  handleReportChange = (tagEdit) => this.setState({ tagEdit });


  openEditTag = ( tag ) =>{
    console.log( "tag -->", tag );
    this.setState({ isOpenEditTag: true, tagEdit: tag.categoria, tagEditId: tag.id })
  }
  closeEditBrand = () => this.setState({ isOpen: false });


  closeSettings = () => this.setState({ isOpenSettings: false })
  openSettings = () => this.setState({ isOpenSettings: true })



};

function mapStateToProps( state )
{
  return {
    loading: state.categories.loading,
    categories: state.categories.categories,
    loading_brands: state.brands.loading,
    brands: state.brands.brands,
    addingBrand: state.adding,
    removeCat: state.removeCat
  }
}

export default connect(mapStateToProps, { fetchCats, addCat, fetchBrands, removeCat, fetchPosts, editCat })( SidebarContent );
