import React, {
  Component
}from 'react'

class SmoothScroll extends Component {
  constructor(props) {
    super(props)

    this.smooth = this.smooth.bind(this)
  }

  smooth(event) {
    event.preventDefault()

    if ('querySelector' in document && 'addEventListener' in window) {
      let getElementOffset = (element) => {
        let top = 0
        let left = 0
        // Loop through the DOM tree
        // and add it's parent's offset to get page offset
        do {
          top += element.offsetTop || 0
          left += element.offsetLeft || 0
          element = element.offsetParent
        } while (element)

        return {
          top,
          left,
        }
      }

      let smoothScroll = (anchor, duration) => {
        let endLocation = getElementOffset(anchor).top
        let startLocation = window.pageXOffset !== undefined ? window.pageYOffset : document.documentElement.scrollTop
        let distance = endLocation - startLocation
        let increments = distance / (duration / 16)
        let stopAnimation

        console.log(anchor)
        // Scroll the page by an increment, and check if it's time to stop
        let animateScroll = function() {
          window.scrollBy(0, increments)
          stopAnimation()
        }

        // If scrolling down
        if (increments >= 0) {
          // Stop animation when you reach the anchor OR the bottom of the page
          stopAnimation = () => {
            let travelled = window.pageYOffset
            if ((travelled >= (endLocation - increments)) || ((window.innerHeight + travelled) >= document.body.offsetHeight)) {
              clearInterval(runAnimation)
            }
          }
        }
        // If scrolling up
        else {
          // Stop animation when you reach the anchor OR the top of the page
          stopAnimation = () => {
            let travelled = window.pageYOffset
            if (travelled <= (endLocation || 0)) {
              clearInterval(runAnimation)
            }
          }
        }

        // Loop the animation function
        let runAnimation = setInterval(animateScroll, 16)

      }

      // Define smooth scroll links
      let dataTarget = document.querySelector(this.props.href)

      if (dataTarget) {
        smoothScroll(dataTarget, this.props.speed || 500)
      }
    }

    this.props.onClick && this.props.onClick()
  }

  render() {
    return (
      <a { ...this.props} onClick={this.smooth}>
        {this.props.children}
      </a>
    )
  }
}

export default SmoothScroll
