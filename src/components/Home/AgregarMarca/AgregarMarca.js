import React, { Component } from 'react'
import { Button, Classes, Dialog, H1 } from "@blueprintjs/core"
import ListadoPaginas from '../ListadoPaginas/ListadoPaginas'
import { connect } from 'react-redux'
import { fetchBrands } from '../../../actions'
import * as actions from '../../../actions'
import axios from 'axios'
import { API_URL } from '../../../config.js'

import './styles.css'

class AgregarMarca extends Component {
  constructor(props){
    super(props)

    this.state = {
      isOpen: false,
      selectedBrand: {}
    }
  }

  handleBrand = ( page ) => {
    this.setState( {
        selectedBrand: page
    })
  }

  render() {
    const { isOpen } = this.state;

    return (
      <div className="brands-added">
        <H1 className="brands-added__title">YOUR BRANDS</H1>
        <Button onClick={this.openAddBrand} minimal="true" large="true" className="button button-primary brands-added__button" >Add a Brand</Button>
        <Dialog
                  icon="locate"
                  onClose={this.closeAddBrand}
                  title="Select Facebook Account"
                  isOpen={isOpen}
                  className="bp3-dark">
          <div className={Classes.DIALOG_BODY}>

            <ListadoPaginas onSelectBrand={this.handleBrand}/>

          </div>
          <div className={Classes.DIALOG_FOOTER}>
              <div className={Classes.DIALOG_FOOTER_ACTIONS}>
                  <Button onClick={this.addBrandConfirm}>Select</Button>
              </div>
          </div>


        </Dialog>
      </div>
    )
  }
  openAddBrand = () => this.setState({ isOpen: true });
  closeAddBrand = () => this.setState({ isOpen: false });
  addBrandConfirm = () => {
    this.props.addingBrand( true );

    // se envía la página a la BD
    const fbResponse = JSON.parse(localStorage.getItem('resultObject'));
    const token = fbResponse.authResponse.accessToken;
    const AUTH_TOKEN = localStorage.getItem( "api_access_token" );

    console.log( "agregando marca" );

    axios.post( API_URL + `marcas`,{
        page_id: this.state.selectedBrand.id,
        token: this.state.selectedBrand.access_token
      },{
        headers: {'Authorization': "bearer " + AUTH_TOKEN}
      })
      .then(response => {
        console.log( response );
        this.props.addingBrand( false );
      })
      .catch((error) => {
        console.log(error);
        this.props.addingBrand( false );
      });

      this.setState({ isOpen: false });
  }
}

function mapStateToProps( state )
{
  return {
    brands: state.brands.brands
  }
}

export default connect(mapStateToProps, actions)( AgregarMarca );
