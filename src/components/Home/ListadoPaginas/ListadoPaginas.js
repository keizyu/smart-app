import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Spinner } from "@blueprintjs/core"
import { selectPage } from '../../../actions/index'
import { bindActionCreators } from 'redux'
import { API_URL } from '../../../config.js'

import './styles.css'

class ListadoPaginas extends Component {
    constructor(props){
      super(props)

      this.state = {
          selected_page : {},
          _pages: []
      }
    }


    componentWillMount() {
      const fbResponse = JSON.parse(localStorage.getItem('resultObject'));
      const AUTH_TOKEN = localStorage.getItem( "api_access_token" );

      fetch( API_URL + 'facebook/listAccounts?token='+fbResponse.authResponse.accessToken, {
          method: 'GET',
          headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '+ AUTH_TOKEN
          }
        }).then(res=>res.json())
          .then(
            res => {
              if( res.status === 'error' )
              {
                window.location.href='/';
              }

                this.setState({
                  _pages: res.data
                })
              }
          ).catch((error) => {
            console.log(error);
          })
    }

    selectPage( page ) {
        this.setState({ selected_page: page.id });
        this.props.onSelectBrand( page );
    }

    renderList() {
        return this.state._pages.map( (page) => {
            return (
                <div
                    key={ page.id }
                    className="paginas-fb__elemento"
                    onClick={(e) => this.selectPage( page ) } >

                    <span className={ this.state.selected_page == page.id ? 'AgregarMarca__boton--seleccionado': null} >
                      <img src={ page.url_picture } />
                      { page.name }
                    </span>
                </div>
            );
        });
    }

    render() {
        return(
            <div className="paginas-fb">
                { this.state._pages.length > 0 ?
                    this.renderList() :
                      <Spinner />
                  }
            </div>
        )
    }
}

function mapStateToProps( state )
{
    return {
        pages: state.pages
    };
}

function mapDispatchToProps( dispatch )
{
    return bindActionCreators( { selectPage: selectPage }, dispatch );
}

export default connect( mapStateToProps, mapDispatchToProps )( ListadoPaginas );
