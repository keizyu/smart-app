import React, { Component } from 'react';
import { Alert, Button, Card , Elevation, H4, Intent, Toaster, Classes, Dialog } from "@blueprintjs/core";
import { connect } from 'react-redux';
import { fetchBrands } from '../../../actions'
import { Redirect } from 'react-router';
import { API_URL, API_ADS_URL } from '../../../config.js';
import { fire, facebookProvider, twitterProvider, googleProvider } from '../../../fire';
import ListadoAccountsADS from '../ListadoAccountsADS/ListadoAccountsADS';
import axios from 'axios';

//Logos
import FBIcon from '../../../assets/img/social-icons/facebook_hover.jpg';
import GPlusIcon from '../../../assets/img/social-icons/google_hover.jpg';
import InstaIcon from '../../../assets/img/social-icons/instagram_hover.jpg';
import InIcon from '../../../assets/img/social-icons/linkedin_hover.jpg';
import TwIcon from '../../../assets/img/social-icons/twitter_hover.jpg';

import './styles.css'

class Marca extends Component {
  constructor(props){
    super(props)

    this.state = {
      marca: null,
      isOpen: false,
      selectedBrand: 0,
      isInstagramOpen: false,
      instagramAccounts: [],
      items: {},
      twitterProfilePicture: '',
      isOpenSelectAccount: false,
      selectedAccount: {}
    }
  }

  handleAccount = ( account ) => {
    this.setState({
        selectedAccount: account
    })
  }

  authWithFacebook = () => {
      fire.auth().signInWithPopup(facebookProvider)
      .then((result,error) => {
          if(error){
              console.log('unable to signup with firebase')
          } else {
              this.setState({authenticated: true })
          }
      })
  }

  authWithTwitter = () => {
      fire.auth().signInWithPopup(twitterProvider)
      .then((result,error) => {
          if(error){
              console.log('unable to signup with firebase')
          } else {
              this.setState({authenticated: true })

            const token = result.credential.accessToken;
            const secret = result.credential.secret;
            const user = result.user;
            const uid = user.providerData[0].uid;
            const AUTH_TOKEN = localStorage.getItem( "api_access_token" );

            fetch( `${API_URL}twitter/insert_token/${this.props.marca.id}/${token}/${secret}/${uid}`, {
              method: 'GET',
              cache: 'default',
              headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json',
                'Authorization': 'Basic '+ AUTH_TOKEN,
              },
              credentials: "same-origin"
            }).then(response => response.json())
                .then(
                    (data) =>
                    {
                        if( data.error == 0 )
                        {
                            this.setState({
                                twitterProfilePicture: data.picture
                            });
                            alert( "Agregado correctamente" );

                            this.props.fetchBrands();
                        }
                        else
                        {
                            alert( "Hubo error, intenta de nuevo más tarde" );
                        }
                    }
                );

          }
      })
  }

  authWithGoogle = () => {
      fire.auth().signInWithPopup(googleProvider)
      .then((result,error) => {
          if(error){
              console.log('unable to signup with firebase')
          } else {
              this.setState({authenticated: true })

              let itemsRef = fire.database().ref('items')
              itemsRef.on('value', data=> {
                this.setState({
                  items: data.val()
                })
              })
          }
      })
  }

  confirmErase( provider ) {
    const r = window.confirm("Do you really want to erase this Account?");
    if( r===true  ) {
      // se borra tw de la marca
      this.eraseCall( provider );
    }
  }

  getAccountsFB = () => {
    const fbResponse = JSON.parse(localStorage.getItem('resultObject'));
    const token = fbResponse.authResponse.accessToken;
    const AUTH_TOKEN = localStorage.getItem( "api_access_token" );
    var formData = new FormData();

    var payload = {
      page_id: this.props.marca.id,
      _token: token
    };

    var data = new FormData();
    data.append( "json", JSON.stringify( payload ) );

    fetch( API_URL + 'set_instagram_account', {
      method: 'POST',
      cache: 'default',
      body: JSON.stringify( payload ),
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json',
        'Authorization': 'Basic '+ AUTH_TOKEN,
      },
      credentials: "same-origin"
    }).then(function( res ){
        this.props.fetchBrands();
        this.setState({ isOpen: false });
        this.toaster.show({ message: TOAST_ERASE_BRAND_MESSAGE });
      });
  }

  getInstagramAccount( that ) {
    const fbResponse = JSON.parse(localStorage.getItem('resultObject'));
    const token = fbResponse.authResponse.accessToken;
    const AUTH_TOKEN = localStorage.getItem( "api_access_token" );
    var formData = new FormData();

    var payload = {
      page_id: that.props.marca.id,
      _token: token
    };

    var data = new FormData();
    data.append( "json", JSON.stringify( payload ) );

    fetch( API_URL + 'marcas/instagram', {
      method: 'POST',
      cache: 'default',
      body: JSON.stringify( payload ),
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer '+ AUTH_TOKEN,
      },
      credentials: "same-origin"
    }).then(function( res ){
        that.props.fetchBrands();
        that.setState({ isOpen: false });
        that.toaster.show({ message: TOAST_INSTAGRAM_ADDED });
      });
  }

  eraseCall( provider ) {
    const fbResponse = JSON.parse(localStorage.getItem('resultObject'));
    const token = fbResponse.authResponse.accessToken;
    const AUTH_TOKEN = localStorage.getItem( "api_access_token" );
    const that = this;
    var formData = new FormData();

    var payload = {
      page_id: this.props.marca.id,
      provider: provider
    };

    var data = new FormData();
    data.append( "json", JSON.stringify( payload ) );

    fetch( API_URL + 'marcas/'+ provider.toLowerCase() +'/'+ this.props.marca.id, {
      method: 'DELETE',
      cache: 'default',
      body: JSON.stringify( payload ),
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer '+ AUTH_TOKEN,
      },
      credentials: "same-origin"
    }).then(function( res ){
        that.props.fetchBrands();
        that.setState({ isOpen: false });
        that.toaster.show({ message: TOAST_ERASE_BRAND_MESSAGE });
      });
  }

  renderTwitterImage() {
      return(
            this.props.marca.twitter_profile_picture === null ?
            <img src={ TwIcon } className="logosCircle" alt="Twitter" onClick={this.authWithTwitter} />
            :
            (
              <div className="marcaRedesAvatarLogged">
                <img src={ this.props.marca.twitter_profile_picture !== null ? this.props.marca.twitter_profile_picture : TwIcon } className="logos-circle" alt="Twitter" onClick={this.authWithTwitter} />
                <div className="brandIcons">
                  <i className="brandIconsIcon fas fa-trash-alt" onClick={ () => { this.confirmErase( 'TW' ) } }></i>
                  <i className="brandIconsIcon fab fa-twitter"></i>
                  <i className="brandIconsIcon fas fa-pencil-alt" onClick={this.authWithTwitter}></i>
                </div>
              </div>
          )
      )
  }

  renderInstagramImage() {

      return(
            this.props.marca.instagram_profile_picture === null ?
            <img src={ InstaIcon } className="logosCircle" alt="Instagran" onClick={ () => {
              this.getInstagramAccount( this )
            }} />
            :
            ( <div className="marcaRedesAvatarLogged">
                <img src={ this.props.marca.instagram_profile_picture } className="logosCircle" alt="Instagram" />
                <div className="brandIcons">
                  <i className="brandIconsIcon fas fa-trash-alt"  onClick={ () => { this.confirmErase( 'INS' ) } }></i>
                  <i className="brandIconsIcon fab fa-instagram"></i>
                  {/*<i className="brandIconsIcon fas fa-pencil-alt"></i>*/}
                </div>
              </div>
            )
      );
  }

  render() {
    if (this.state.redirect) {
      let dashboard_route = "/dashboard/" + this.state.selectedBrand;
      return <Redirect push to={ dashboard_route } />;
    }

    const { isOpen } = this.state;
    const { isOpenSelectAccount } = this.state;

    console.log("this.props.marca -->", this.props.marca)

    return (

      <Card interactive={true} elevation={Elevation.TWO} className="marcaListaItem" id={ this.props.marca.id }>
        <H4 className="bp3-dark">{this.props.marca.page_name}</H4>
        <div className="marcaListaRedes">
          <div className="marcaLista__fb">
              <div className="marcaRedesAvatar">
                <img src={ this.props.marca.fb_picture ? this.props.marca.fb_picture : {FBIcon} } className="logosCircle" alt="Facebook" />
              </div>
              <p className="marcaRedesUsuario">{this.props.marca.page_name}</p>
          </div>
          <div className="marcaLista__tw">
              <div className="marcaRedesAvatar">
                { this.renderTwitterImage() }
              </div>
              <p className="marcaRedesUsuario">
                Twitter
              </p>
          </div>
          <div className="marcaLista__insta">
              <div className="marcaRedesAvatar">
                { this.renderInstagramImage() }
              </div>
              <p className="marcaRedesUsuario">
                Instagram
              </p>
          </div>
          <div className="marcaLista__in">
              <div className="marcaRedesAvatar">
                <img src={InIcon} className="logosCircle" alt="Linkedin" />
              </div>
              <p className="marcaRedesUsuario">
                Linkedin
              </p>
          </div>
          <div className="marcaLista__gplus">
              <div className="marcaRedesAvatar">
                <img src={GPlusIcon} className="logosCircle" alt="Google"  onClick={this.authWithGoogle} />
              </div>
              <p className="marcaRedesUsuario">
                GPlus
              </p>
          </div>

          <div className="marcaLista__ads">
              <div className="marcaRedesAvatar">
                <img src={ this.props.marca.adds_account_id ? this.props.marca.fb_picture : {FBIcon} } className="logosCircle" alt="FB Ads" onClick={ () => this.setState({ isOpenSelectAccount: true }) } />
              </div>
              <p className="marcaRedesUsuario">
                + ADs
              </p>
          </div>
        </div>
        <div className="marcaActions">
           <Button intent="danger" icon="trash" text="Erase Brand" onClick={this.eraseBrandOpen} className="marcaActionsErase" />
           <Button text="Start" minimal="true" className="button button-primary marcaActionsStart" onClick={this.handleOnClick} />
           <Alert
                cancelButtonText="Cancel"
                confirmButtonText="Accept"
                icon="trash"
                intent={Intent.DANGER}
                isOpen={isOpen}
                onCancel={this.eraseBrandCancel}
                onConfirm={this.eraseBrandConfirm}
                className="bp3-dark"
            >
            <p>
              Do you want to erase this brand?
            </p>
            </Alert>
            <Toaster className="bp3-dark" ref={ref => (this.toaster = ref)} />
        </div>

        <Dialog
                  icon="locate"
                  onClose={ () => this.setState({ isOpenSelectAccount: false }) }
                  title="Select Facebook Ads Account"
                  isOpen={ isOpenSelectAccount }
                  className="bp3-dark">

          <div className={Classes.DIALOG_BODY}>
            <ListadoAccountsADS onSelectBrand={ this.handleAccount }/>
          </div>

          <div className={Classes.DIALOG_FOOTER}>
              <div className={Classes.DIALOG_FOOTER_ACTIONS}>
                  <Button onClick={
                    ()=>{
                    //this.props.addingBrand( true );

                    // se envía la página a la BD
                    const fbResponse = JSON.parse(localStorage.getItem('resultObject'));
                    const token = fbResponse.authResponse.accessToken;
                    const AUTH_TOKEN = localStorage.getItem( "api_access_token" );

                    axios.defaults.headers.common['Authorization'] = 'Bearer '+ AUTH_TOKEN;
                    axios.post( API_ADS_URL + `ads/add`,{
                        brand_id: this.props.marca.id,
                        account_id: this.state.selectedAccount.id,
                        token: token
                      })
                      .then(response => {
                        //this.props.addingBrand( false );
                        this.props.fetchBrands();
                      })
                      .catch((error) => {
                        console.log(error);
                        //this.props.addingBrand( false );
                      });

                      this.setState({ isOpenSelectAccount: false });
                  }

                  }>Select</Button>
              </div>
          </div>

        </Dialog>

      </Card>

    )
  }

  handleOnClick = () => {
    let marca = this.props.marca.id
    console.log( marca );

    this.setState({
      redirect: true,
      selectedBrand: marca
    })
  }


  openAddInstagram = () => {
      this.props.getInstagramBusinessAccounts();
      this.setState({ isInstagramOpen: true });
  }

  closeAddInstagram = () => this.setState({ isInstagramOpen: false });
  eraseBrandOpen = () => this.setState({ isOpen: true });
  eraseBrandCancel = () => this.setState({ isOpen: false });
  eraseBrandConfirm = () => {

    // Se borra la marca
    const fbResponse = JSON.parse(localStorage.getItem('resultObject'));
    const token = fbResponse.authResponse.accessToken;
    const AUTH_TOKEN = localStorage.getItem( "api_access_token" );

    const that = this;
    var formData = new FormData();
    formData.append('page_id', this.props.marca.id);

    fetch( API_URL + 'marcas/'+this.props.marca.id, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer '+ AUTH_TOKEN,
      },
      credentials: "same-origin"
    }).then(function( res ){
        that.props.fetchBrands();
      });

    this.setState({ isOpen: false });
    this.toaster.show({ message: TOAST_ERASE_BRAND_MESSAGE });
  }
}

const TOAST_ERASE_BRAND_MESSAGE = (
    <div>
        The <strong>Brand</strong> was deleted
    </div>
);

const TOAST_INSTAGRAM_ADDED = (
    <div>
        Se ha ligado la cuenta de instagram.
    </div>
);

function mapStateToProps( state ) {
  return {
    brands: state.brands.brands,
    selectedBrand: state.selectedBrand
  }
}


export default connect(mapStateToProps, { fetchBrands })( Marca );
