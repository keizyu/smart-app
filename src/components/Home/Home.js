import React, { Component } from 'react';
// import { Spinner } from "@blueprintjs/core";
import Header from '../Shared/Header/Header';
import ListadoMarcas from './ListadoMarcas/ListadoMarcas';
import AgregarMarca from './AgregarMarca/AgregarMarca';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import { API_URL } from '../../config.js';
import { Spinner } from '@blueprintjs/core';

class Home extends Component {
  state = {
    pages:[],
  }

  componentDidMount() {
    this.props.addingBrand( false );

    // se obtiene el usurio de FB
    const fbResponse = JSON.parse(localStorage.getItem('resultObject'));

    console.log( "=========================================================" );
    console.log( "---------------------- resultObject" );
    console.log( fbResponse );
    console.log( "=========================================================" );

    // console.log( fbResponse );

    // if( fbResponse )
    // {
    //   let user_id = fbResponse.authResponse.userID;
    //   let token = fbResponse.authResponse.accessToken;

    //   // http://159.89.142.130:5000
    //   // http://localhost:5000/
    //   fetch('http://159.89.142.130:5000/api/smart/v0.1/save?userid=' + user_id + "&token=" + token, {
    //       method: 'get',
    //       headers: {
    //         'Accept': 'application/json, text/plain, */*',
    //         'Content-Type': 'application/json',
    //         'Authorization': 'Basic '+btoa( token ),
    //         },
    //         credentials: "same-origin"
    //     }).then(res=>res.json())
    //       .then(
    //         res => {
    //           localStorage.setItem('api_access_token', res.auth_token );
    //             if( true ){ //res.validado ){

    //               console.log( res );

    //                 // se guarda el token
    //                 localStorage.setItem('api_access_token', res.auth_token );

    //                 this.props.changeAuth( true );

    //                 this.setState({
    //                 pages: res.pages
    //                 });

    //                 console.log( res.pages );
    //             }
    //             else

    //               console.log( "no validado por el administrador" );
    //             }
    //       );

    // }
  }

  searchStringInArray (str, strArray) {
    for (var j=0; j<strArray.length; j++) {
        if (strArray[j].match(str)) return j;
    }
    return -1;
  }

  renderHome()
  {
    if( this.props.brand )
    {
      return ( <div className="spinner-container"><Spinner />This process can take several minutes...</div> )
    }
    else
    {
      return(
        <ListadoMarcas />
      )
    }
  }

  render() {
      return (
        <div className="main-container">
          <Header />
          <div>
            <AgregarMarca />
            {this.renderHome()}
          </div>
        </div>
      );
  }
}

function mapStateToProps( state )
{
  return {
    auth: state.auth,
    brand: state.adding
  };
}

export default connect( mapStateToProps, actions )( Home );
