import React, { Component } from 'react';
import Marca from '../Marca/Marca';
import { connect } from 'react-redux';
import { fetchBrands } from '../../../actions';

class ListadoMarcas extends Component {
  componentWillMount()
  {
    this.props.fetchBrands();
  }

  renderBrands()
  {
    return this.props.brands.map( (brand) =>  {
        return( <Marca key={brand._pagina} marca={brand}/> );
      });
  }

  render() {
   return (
      <div className="marcaLista">
        { this.renderBrands() }
      </div>
    );
  }
}

function mapStateToProps( state )
{
  return {
    brands: state.brands.brands,
    loading: state.brands.loading
  }
}

export default connect(mapStateToProps, { fetchBrands })( ListadoMarcas );
