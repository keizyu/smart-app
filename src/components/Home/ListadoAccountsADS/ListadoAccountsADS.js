import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Spinner } from "@blueprintjs/core";
import { selectPage } from '../../../actions/index';
import { bindActionCreators } from 'redux';
import { API_ADS_URL } from '../../../config.js';

import './styles.css'

class ListadoAccountsADS extends Component {
    state = {
        selected_page : {},
        _pages: []
    }

    componentWillMount()
    {
      const fbResponse = JSON.parse(localStorage.getItem('resultObject'));
      const AUTH_TOKEN = localStorage.getItem( "api_access_token" );

      console.log( AUTH_TOKEN  );

      fetch( API_ADS_URL + 'ads/listAccounts?token=' + fbResponse.authResponse.accessToken, {
          method: 'get',
          headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '+ AUTH_TOKEN
          },
          credentials: "same-origin"
        }).then(res=>res.json())
          .then(
            res => {
              console.log( "Cuentas --> ", res );

              if( res.status === 'fail' )
              {
                //window.location.href='/';
              }

                this.setState({
                  _pages: res.data
                })
              }
          );
    }

    selectPage( page )
    {
        this.setState({ selected_page: page.id });
        this.props.onSelectBrand( page );
    }

    renderList() {
        return this.state._pages.map( (page) => {
            return (
                <div
                    key={ page.id }
                    className="paginas-fb__elemento"
                    onClick={(e) => this.selectPage( page ) } >

                    <span className={ this.state.selected_page == page.id ? 'AgregarMarca__boton--seleccionado': null} >
                      { page.name }
                    </span>
                </div>
            );
        });
    }

    render() {
        return(
            <div className="paginas-fb">
                { this.state._pages.length > 0 ?
                    this.renderList() :
                      <Spinner />
                  }
            </div>
        )
    }
}

function mapStateToProps( state )
{
    return {
        pages: state.pages
    };
}

function mapDispatchToProps( dispatch )
{
    return bindActionCreators( { selectPage: selectPage }, dispatch );
}

export default connect( mapStateToProps, mapDispatchToProps )( ListadoAccountsADS );
