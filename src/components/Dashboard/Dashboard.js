import React, { Component } from 'react'
import Sidebar from 'react-sidebar'

// COMPONENTES
import MainContainer from './MainContainer/MainContainer'
import Header from '../Shared/Header/Header'
import Navbar from '../Shared/Navbar/Navbar'
import SidebarContent from '../Shared/SidebarContent/SidebarContent'
import { connect } from 'react-redux'
import { fetchPosts, fetchCats, changeFilter, changeOrder } from '../../actions'

import './styles.css'

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      sidebarOpen: false,
      pullRight: true,
      shouldHide: false,
      selectedBrand: 0,
      cats: [],
    }

  }

  onSetSidebarOpen = (open) => {
    this.props.fetchCats( this.props.match.params.id );
    this.setState({ sidebarOpen: open });
  }

  render () {
    var sidebarContent = <SidebarContent id={ this.props.match.params.id } />;
    console.log( this.state.selectedBrand );

    return(

      <Sidebar sidebar={sidebarContent}
                 open={this.state.sidebarOpen}
                 onSetOpen={this.onSetSidebarOpen}
                 pullRight={this.state.pullRight}
                 rootClassName="sidebar__root"
                 sidebarClassName="sidebar__controller"
                 overlayClassName="sidebar__overlay"
                 id={ this.props.match.params.id }>
            <div className="dashboard-shell">
              <Header sidebar={this.onSetSidebarOpen} hide={this.state.shouldHide} />
              <Navbar id={ this.props.match.params.id } />
              <MainContainer id={ this.props.match.params.id }  post_filter={this.props.post_filter} post_order={this.props.post_order}  />
            </div>
    </Sidebar>

    );
  }
}

function mapStateToProps( state )
{
  return {
    selectedBrand: state.selectedBrand,
    cats: state.categories.categories,
    post_filter: state.post_filter,
    post_order: state.post_order
  }
}

export default connect(mapStateToProps, { fetchPosts, fetchCats, changeFilter, changeOrder })( Dashboard );
