import React, { Component } from 'react'
import TopExtras from '../../Shared/TopExtras/TopExtras'
import Posts from '../Posts/Posts'

import './styles.css'

class MainContainer extends Component {
  
  render () {
    return(
      <div className="main-container__dashboard">
        <TopExtras id={ this.props.id }/>
        <Posts id={ this.props.id }/>
      </div>
    );
  }
}

export default MainContainer;
