import React, { Component } from 'react'
import logoSmart from '../../../assets/img/logo.png'
import { Card, Elevation, H5, H6, Spinner } from "@blueprintjs/core"
import Select from 'react-select'
import _ from 'lodash'
import { connect } from 'react-redux'
import { updatePostCategory, fetchPosts, fetchCats } from '../../../actions'
import moment from 'moment'
import Truncate from '../Truncate/Truncate'

import './styles.css'

class Post extends Component {
  constructor( props ){
    super(props)

    this.state = {
      filterable: false,
      items: [],
      selectedOption: null,
      updated: false
    }
  }



  handleChange = (selectedOption) => {
    this.setState({
      selectedOption
    });

    this.props.updatePostCategory( selectedOption.value, this.props.post.id, this.props.post.provider );
  }

  setCats() {
    let options = [];
    this.props.categories.map(
      ( cat ) => {
        options.push( { 'label': cat.name, 'value': cat.id } )
        return '';
      }
    )

    return options;
  }

  renderSelect() {
    //console.log( "Actualizando post - marca ", this.props.addingBrand );

    if( !this.props.loading_cats ) {
      let that = this;
      const options = this.setCats();
      const selectPlaceholder = 'Categories';

      let sel_label = '';
      let sel_value = 0;

      //console.log( "======================================================== categorias " );
      //console.log( this.props.categories );
      //console.log( "======================================================== cateogoria id:" );
      //console.log( that.props.post.categoria_id );
      //console.log( "========================================================" );

      // se busca la categoría del post
      let selectedOption = _.findLast(this.props.categories, function(cat) {
                     //console.log( "cat ", cat );
                    return cat.id == that.props.post.categoria_id
                  });

      if( this.state.selectedOption!==null )
      {
        sel_label = this.state.selectedOption.label;
        sel_value = this.state.selectedOption.value;
      }
      else
      {
        sel_label = selectedOption.name;
        sel_value = selectedOption.id;
      }

      return(
        <Select
            value={ { 'label': sel_label, 'value': sel_value } }
            onChange={this.handleChange}
            options={options}
            placeholder={selectPlaceholder}
            className='react-select-container'
            classNamePrefix='react-select'
          />
      );
    }
    else {
      let that = this;
      const options = null;
      const selectPlaceholder = 'Categories';
      const selectStyle = {
        option: (base, state) => ({
          ...base,
          borderBottom: '1px dotted pink',
          padding: 5,
          backgroundColor: 'green'
        }),
        control: () => ({
          // none of react-selects styles are passed to <View />
          width: 100,
          backgroundColor: 'red'
        }),
        singleValue: (base, state) => {
          const opacity = state.isDisabled ? 0.5 : 1;
          const transition = 'opacity 300ms';

          return { ...base, opacity, transition };
        }
      }

      let sel_label = '';
      let sel_value = 0;
      sel_label = '';
      sel_value = 1;

      return(
        <Select
            onChange={this.handleChange}
            placeholder={selectPlaceholder}
            className='react-select-container'
            classNamePrefix='react-select'
          />
      );
    }
  }

  render () {
    return(

        <Card interactive={true} elevation={Elevation.TWO} className="postContainer" id={ this.props.post.id }>
          <div className="postIcon">
            <i className={ this.props.post.provider === 'facebook' ? 'fab fa-facebook-f' : 'hidden' }></i>
            <i className={ this.props.post.provider === 'instagram' ? 'fab fa-instagram' : 'hidden' }></i>
            <i className={ this.props.post.provider === 'twitter' ? 'fab fa-twitter' : 'hidden' }></i>
            <i className={ this.props.post.provider === 'linkedin' ? 'fab fa-linkedin-in' : 'hidden' }></i>
          </div>
          <div className="post">

            <div className="post-topContainer">
              <div className="postImage">
                  <a href={this.props.post.link} target="_blank">
                      <img src={ this.props.post.picture } alt="" />
                  </a>
              </div>

              <div className="postExtras">
                <p className="postExtrasType"> {this.props.post.ins_media_type} </p>
                <div className="postExtrasEngagment">
                  <H6>
                    Eng Score
                  </H6>
                  <p>
                    { this.props.post.es_calculated.toFixed(4) }
                  </p>
                </div>
                <div className="postExtrasEngagment">
                  <H6>
                    { /*this.props.post.postExtras.postEngagment.competition.title*/ }
                    ES Competition
                  </H6>
                  <p>
                    { this.props.post.es_competition.toFixed(4) }
                  </p>
                </div>
                <div className="postExtrasCategory">
                  { this.renderSelect() }
                </div>
              </div>

            </div>

            <div className="postMessage">

              <Truncate text={this.props.post.message} />

            </div>

            <div className="postDate">
              <p> <img src={logoSmart} alt="Logo Smart" className="logoSmartPost" />
                { moment(this.props.post.post_published_date).format("llll") }
                </p>
            </div>

            <div className="postInsights">

              <div className="postInsightsInfo">
                <H5>
                  { /*this.props.post.reach.title*/ }
                  REACH
                </H5>
                <div className="postInsightsData">

                  <div className="postDataItem">
                    <p className="postDataInfo">{ this.props.post.post_impressions_organic }</p>
                    <p className="postDataTitle">Organic</p>
                  </div>

                  <div className="postDataItem">
                    <p className="postDataInfo">{ this.props.post.post_impressions_paid }</p>
                    <p className="postDataTitle">Paid</p>
                  </div>

                  <div className="postDataItem">
                    <p className="postDataInfo">{ (this.props.post.post_impressions_organic+this.props.post.post_impressions_paid) }</p>
                    <p className="postDataTitle">Total</p>
                  </div>

                </div>

              </div>

              <div className="postInsightsInfo">
                <H5>
                  { /*this.props.post.postInsights.engagment.title*/ }
                  ENGAGEMENT
                </H5>
                <div className="postInsightsData">
                  <div className="postDataItem">
                    <p className="postDataInfo">{ (this.props.post.wow+this.props.post.likes+this.props.post.anger+this.props.post.haha+this.props.post.sorry+this.props.post.love+this.props.post.anger) }</p>
                    <p className="postDataTitle">Reactions</p>
                  </div>
                  <div className="postDataItem">
                    <p className="postDataInfo">{ this.props.post.shares }</p>
                    <p className="postDataTitle">Shares</p>
                  </div>
                  <div className="postDataItem">
                    <p className="postDataInfo">{ this.props.post.comments }</p>
                    <p className="postDataTitle">Comments</p>
                  </div>
                </div>
              </div>

              <div className="postInsightsInfo">
                <H5>
                  { /*this.props.post.postInsights.clicks.title*/ }
                  REACH
                </H5>
                <div className="postInsightsData">
                  <div className="postDataItem">
                    <p className="postDataInfo">{ this.props.post.linkclicks }</p>
                    <p className="postDataTitle">Links</p>
                  </div>
                  <div className="postDataItem">
                    <p className="postDataInfo">{ this.props.post.photoclicks }</p>
                    <p className="postDataTitle">Photo</p>
                  </div>
                  <div className="postDataItem">
                    <p className="postDataInfo">{ this.props.post.otherclicks }</p>
                    <p className="postDataTitle">Others</p>
                  </div>
                </div>
              </div>

              <div className="postInsightsInfo">
                <H5>
                  { /*this.props.post.postInsights.video.title*/ }
                  VIDEO
                </H5>
                <div className="postInsightsData">
                  <div className="postDataItem">
                    <p className="postDataInfo">{ this.props.post.videoclicks }</p>
                    <p className="postDataTitle">Links</p>
                  </div>
                  <div className="postDataItem">
                    <p className="postDataInfo">{ this.props.post.videosviewsorganics }</p>
                    <p className="postDataTitle">Photo</p>
                  </div>
                  <div className="postDataItem">
                    <p className="postDataInfo">{ this.props.post.videoviewspaid }0</p>
                    <p className="postDataTitle">Others</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Card>
    );
  }
}

function mapStateToProps( state )
{
  return {
    addingBrand: state.addingBrand,
    loading_cats: state.categories.loading,
  }
}

export default connect(mapStateToProps, { updatePostCategory, fetchPosts, fetchCats })( Post );
