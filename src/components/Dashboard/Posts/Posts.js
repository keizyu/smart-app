import React, { Component } from 'react';
import { Spinner } from '@blueprintjs/core';
import Post from '../Post/Post';
import { fetchPosts } from '../../../actions';
import { fetchCats } from '../../../actions';
import { connect } from 'react-redux';
// import LazyLoad from 'react-lazyload';
import { changeFilter } from '../../../actions';
import { changeOrder } from '../../../actions';
import _ from 'lodash';
import { List  } from 'react-virtualized';

class Posts extends Component {
    constructor(props)
    {
        super(props);
        this.state = {
            selectedType: 'all',
            postsActives: [],
            widthContainer: 0
        }

        this.onReflesh = this.onReflesh.bind(this)
        this.onResize = this.onResize.bind(this)
        this.handleChange = this.handleChange.bind(this)
    }

    componentWillMount()
    {
        this.props.fetchPosts( this.props.id );
        this.props.fetchCats( this.props.id );
    }

    componentDidMount(){
      window.addEventListener('resize',  _.debounce(this.onResize, 500))
    }

    componentWillUnmount(){
      window.removeEventListener('resize', ()=>{})
    }

    componentWillReceiveProps(props){
      if((props.posts && props.posts.loading) || (props.post_filter != this.props.post_filter) || (props.posts && props.posts.posts && props.posts.posts)){
          this.onReflesh(props)
      }
    }

    /*state = {
        listado_posts: [],
        listado_categorias: []
    }*/

    onReflesh(props){
      let listado_posts = props.posts.posts;
      const { offsetWidth: width } = this.posts

      // si el filtro no es all se procesa el listado
      if(props.post_filter!=='all' ){
        listado_posts = _.filter(props.posts.posts, {'provider': props.post_filter})

        /* width/416 =>  cuantos posts caben por cada fila (ancho del contenedor / ancho de cada post) */
        listado_posts = _.chunk(listado_posts, width/416);
      }
      else
      {
        listado_posts = _.chunk(listado_posts, width/416);
      }

      console.log( "-- ordenando" );
      console.log(props.post_order);

      // Se ordenan los posts
      switch (props.post_order) {
        case 'date':
        case '':
          listado_posts = _.orderBy(props.posts.posts, ['post_published_date'], ['desc']);
          listado_posts = _.chunk(listado_posts, width/416);
          break;
        case 'eng-score':
          listado_posts = _.orderBy(props.posts.posts, ['es_calculated'], ['desc']);
          listado_posts = _.chunk(listado_posts, width/416);
          break;
        case 'interactions':
          listado_posts = _.orderBy(props.posts.posts, ['engagment.reactions'], ['desc']);
          listado_posts = _.chunk(listado_posts, width/416);
          break;
        case 'eng-score--organic':
          listado_posts = _.orderBy(props.posts.posts, ['es_calculated'], ['desc']);
          listado_posts = _.filter(listado_posts, item => {
                return item.post_impressions_paid == 0;
            });
          listado_posts = _.chunk(listado_posts, width/416);
          break;
        case 'eng-score--paid':
          listado_posts = _.orderBy(props.posts.posts, ['es_calculated'], ['desc']);
          listado_posts = _.filter(listado_posts, item => {
                return item.post_impressions_paid > 0;
            });
          listado_posts = _.chunk(listado_posts, width/416);
          break;
        case 'reach-organic':
          listado_posts = _.orderBy(props.posts.posts, ['post_impressions_organic'], ['desc']);
          listado_posts = _.chunk(listado_posts, width/416);
          break;
        case 'reach-paid':
          listado_posts = _.orderBy(props.posts.posts, ['post_impressions_paid'], ['desc']);
          listado_posts = _.chunk(listado_posts, width/416);
          break;
        case 'category':
          listado_posts = _.orderBy(props.posts.posts, ['es_calculated'], ['desc']);
          listado_posts = _.filter(listado_posts, item => {
                return item.categoria_id == 1;
            });
          listado_posts = _.chunk(listado_posts, width/416);
          break;
      }



      this.setState({
        widthContainer: width,
        postsActives: listado_posts
      })
    }

    onResize(event){
      if(this.posts){
        this.onReflesh(this.props)
      }
    }

    handleChange(selectedOption){}
    handleClickOrder(){}

    render() {
      //console.log( "render() cats " );
      //console.log( this.props.categories );

        const { postsActives, widthContainer } = this.state
        return (
          <div className="posts" ref={(posts)=>(this.posts = posts)}>
            {(this.props.posts.loading) ? (
              <div className="spinner-container">
                <Spinner />
              </div>
            ) : (
              <div className="postsContainer">
                <List
                  width={widthContainer}
                  height={615}
                  rowHeight={533}
                  rowCount={postsActives.length}
                  rowRenderer={({key, index, isScrolling, isVisible, style})=>{
                    return (
                      <div
                        key={key}
                        style={style} >
                        <div className="postsContainerRow">
                          {postsActives[index].map((post)=>{
                            return(
                              <Post key={post.id}
                                post={post}
                                brand_id={this.props.id}
                                handleChange={this.handleChange}
                                categories={this.props.categories} />
                            )
                          })}
                        </div>
                      </div>
                    )}}
                />
              </div>
            )}
          </div>
        );
      }
    }

  function mapStateToProps( state ){
    return {
      posts: state.posts,
      loading_cats: state.categories.loading,
      categories: state.categories.categories,
      post_filter: state.post_filter,
      post_order: state.post_order,
    }
  }

export default connect(mapStateToProps, { fetchPosts, fetchCats, changeFilter, changeOrder })( Posts );
