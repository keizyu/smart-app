import React, { Component } from 'react'
import { Card, Elevation, H1, H6, Button } from "@blueprintjs/core"
import logoSmart from '../../../assets/img/logo.png'
import { connect } from 'react-redux'
import { Spinner } from '@blueprintjs/core'
// import Truncate from './Truncate'
import moment from 'moment'

class Post extends Component {
  constructor(props)
  {
      super(props);
  }

  render () {
    if( this.props.post == null )
    return( <div>No data</div> )

    return(
      <div className="post">

        <div className="post-topContainer">

          <div className="postImage">
              <a href="" target="_blank">
                  <img src={ this.props.post.postImage.url } alt="" />
              </a>
          </div>

          <div className="postExtras">
            <p className="postExtras__type"> </p>
            <div className="postExtrasEngagment">
              <H6>ENG Score</H6>
              <p>{ this.props.post.postExtras.postEngagment.score.data }</p>
            </div>
            <div className="postExtrasEngagment">
              <H6>ES Competition</H6>

                <p>{ this.props.post.postExtras.postEngagment.competition.data }</p>

            </div>
            <div className="postExtrasCategory">
                { this.props.post.categoria }
            </div>
          </div>
        </div>
        <div className="postMessage">

          <Truncate text={this.props.post.post} />

        </div>

        <div className="postDate">
          <p>
            <img src={logoSmart} alt="Logo Smart" className="logoSmartPost" />
            {  moment(this.props.post.postDate).format("llll") }
          </p>
        </div>
      </div>
    )
  }
}

export default Post;
