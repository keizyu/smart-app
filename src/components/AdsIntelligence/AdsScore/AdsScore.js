import React, { Component } from 'react'
import { Card, Elevation, H6  } from "@blueprintjs/core"
import { connect } from 'react-redux'
// import Post from './Post';
import logoSmartCom from '../../../assets/img/loginsmart.png'
import numeral from 'numeral'
import Swiper from 'react-id-swiper'

import { fetchCampaignADS } from '../../../actions'

import './styles.css'


class AdsScore extends Component {
  constructor(props){
    super(props);
    this.state = {
        selectedType: 'all',
        selectedPost: 0,
        selectedCat: 0,
        posts: []
    }
  }

  numberFormat(num, sign = '') {
    const pieces = parseFloat(num).toFixed(2).split('')
    let ii = pieces.length - 3
    while ((ii-=3) > 0) {
      pieces.splice(ii, 0, ',')
    }
    return (sign + pieces.join('')).split(".")[0]
  }


  renderPost(){
    return (
      <div className="post">
        <div className="post-topContainer">
           <div className="postImage">
               <a href="" target="_blank">
                   <img src="" alt="" />
               </a>
           </div>

           <div className="postExtras">
             <p className="postExtras__type"> </p>

             <div className="postExtrasEngagment">
               <H6>ENG Score</H6>
               <p>{ numeral(312).format('0,0') }</p>
             </div>

             <div className="postExtrasEngagment">
                <H6>ES Competition</H6>
                 <p>{ numeral(0.01).format('0,0') }</p>
             </div>
             <div className="postExtrasCategory">

             </div>
           </div>
        </div>

         <div className="postMessage">

           <p className="postMessageParrafo">
             pensando en la bici de tus sue\xf1os? Ven por la tuya a tus tiendas @SpecializedBicycles y estrena, porque aceptamos tarjetas American Express.
             <span className="postMessage__hashtag"> #hashtag</span>
           </p>

         </div>

         <div className="postDate">
           <p> <img src={logoSmartCom} alt="Logo Smart" className="logoSmartPost" /> Thu, 02 Aug 2018 00:00:00 GMT</p>
         </div>
       </div>
    )
  }

  renderSlider() {
    const swiperParams = {
      pagination: {
        el: '.swiper-pagination',
        type: 'fraction',
        clickable: true
      },
      navigation: {
        nextEl: '.swiper-button-next.fas.fa-caret-right',
        prevEl: '.swiper-button-prev.fas.fa-caret-left'
      },
      spaceBetween: 0
    }

    let sliderItems = this.state.posts.map(function(post){
        return (
          <div key={post.id} className="postsMap">

              posteo
          </div>

        );
    })

    return(

      <div className="postsSlider">
        <Swiper {...swiperParams}>
          {sliderItems}
        </Swiper>
     </div>
     )
  }

  renderBest() {
    let campaigns = this.props.campaignAds.result;
    let i=0;
    let that=this;

      return(
        <div>
            {
              campaigns.map(function(camp) {
                i++;
                if( i<=4 )
                  return (
                    <div className="category-rank__item { i==that.state.selectedPost ? 'category-rank__item-active' : '' }" onClick={ ()=>{that.setState( { selectedPost : i })} }>
                      <div className="category-rank__container" key="{ camp.id }">
                        <div className="category-rank__item--title">{ camp.name }</div>
                        <div className="category-rank__item--data">
                          { camp.ads_scroce }%
                        </div>
                        <div className="category-rank__item--rank">#{ i }</div>
                      </div>
                    </div>
                  );
              })
            }

        </div>
      )
  }

  renderResult(){
    return (
      numeral( this.props.campaignAds.result[ this.state.selectedPost ].results ).format('0,0')
    )
  }

  render () {
    let that = this;

    return(
        <div className="ads-shell">

            <Card interactive={true} elevation={Elevation.TWO} className="no-pad-t">

                <div className="grid">
                  <div className="col-12">
                    <div className="subtitle">
                      ADs Score
                    </div>
                  </div>
                  <div className="col-4_sm-12">
                    <div className="category-rank">
                        { this.renderBest() }
                    </div>
                  </div>

                  <div className="col-4_sm-12">
                    <div className="category-data ads-data">

                      <div className="grid">

                        <div className="col-4">

                          <div className="category-data__item">
                            <div className="category-data__item--data">
                              { this.renderResult() }
                            </div>
                            <div className="category-data__item--title">Results</div>
                          </div>

                        </div>

                        <div className="col-4">

                          <div className="category-data__item">
                            <div className="category-data__item--data">
                              { numeral(2070).format('0,0') }
                            </div>
                            <div className="category-data__item--title">Objetive</div>
                          </div>

                        </div>

                        <div className="col-4">

                          <div className="category-data__item">
                            <div className="category-data__item--data">
                              { numeral(2070).format('0,0') }
                            </div>
                            <div className="category-data__item--title">Date</div>
                          </div>

                        </div>

                      </div>

                      <div className="grid">

                        <div className="col-4">

                          <div className="category-data__item">
                            <div className="category-data__item--data">
                              { numeral(4.75).format('0,0') }
                            </div>
                            <div className="category-data__item--title">Relevance</div>
                          </div>

                        </div>

                        <div className="col-4">

                          <div className="category-data__item">
                            <div className="category-data__item--data">
                              { numeral(2.3).format('0,0') }
                            </div>
                            <div className="category-data__item--title">Frequency</div>
                          </div>

                        </div>

                        <div className="col-4">

                          <div className="category-data__item">
                            <div className="category-data__item--data">
                              { numeral(16).format('0,0') }
                            </div>
                            <div className="category-data__item--title">Campaign ADs</div>
                          </div>

                        </div>

                      </div>

                      <div className="grid">

                        <div className="col-4">

                          <div className="category-data__item">
                            <div className="category-data__item--data">
                              MXN { numeral(448).format('0,0') }
                            </div>
                            <div className="category-data__item--title">Spend</div>
                          </div>

                        </div>

                        <div className="col-4">

                          <div className="category-data__item">
                            <div className="category-data__item--data">
                              MXN { numeral(0.241).format('0,0') }
                            </div>
                            <div className="category-data__item--title">CPR</div>
                          </div>

                        </div>

                        <div className="col-4">

                          <div className="category-data__item">
                            <div className="category-data__item--data">
                              MX { numeral(0).format('0,0') }
                            </div>
                            <div className="category-data__item--title">Campaign ADs</div>
                          </div>

                        </div>

                      </div>


                    </div>
                  </div>

                  <div className="col-4_sm-12">
                    <div className="category-post">
                      { this.renderPost() }
                    </div>
                  </div>

                </div>

            </Card>

        </div>
    )
  }
}

function mapStateToProps( state )
{
  return {
    loading: state.campaing_ads.loading,
    campaignAds: state.campaing_ads.campaigns,
  }
}

export default connect(mapStateToProps, {fetchCampaignADS})( AdsScore )
