import React, { Component } from 'react';
import { Alert, Icon, Elevation } from "@blueprintjs/core";
import WordItems from "./WordItems";


class TopicDataItem extends Component {

  constructor(props) {
    super(props);

    this.state = {
      wordItems: [],
      wordEmpty: false,
    };

    this.addWordItem = this.addWordItem.bind(this);
    this.deleteWordItem = this.deleteWordItem.bind(this);
  }


  render (props) {

    return(

        <div className="topicData-item">
          <div className="topic-item__title">
            {this.props.topicData.wordTitle}
          </div>
          <div className="topic-item__count">
            COUNT: {this.props.topicData.count}
          </div>

          <div className="topic-item__word">

            <form onSubmit={this.addWordItem}>
              <div className="grid-center-middle">
                <div className="col-10">
                  <div className="bp3-input-group">
                    <input  className="bp3-input topic-item__input"
                            ref={(a) => this._inputElement = a}
                            placeholder="new word" >
                    </input>
                  </div>

                </div>
                <div className="col-1">
                  <Icon icon="add" elevation={Elevation.TWO} onClick={this.addWordItem} />
                </div>
              </div>
            </form>

          </div>

          <WordItems entries={this.state.wordItems} delete={this.deleteWordItem} />

          <Alert
              confirmButtonText="Okay"
              isOpen={this.state.wordEmpty}
              onClose={this.handleWordEmptyClose}
              className="bp3-dark input-modal"
          >
              <p>
                You need to add a word
              </p>
          </Alert>



        </div>

    );
  }

  addWordItem = (e) => {
    if (this._inputElement.value !== "") {
      var newItem = {
        text: this._inputElement.value,
        key: Date.now()
      };

      this.setState((prevState) => {
        return {
          wordItems: prevState.wordItems.concat(newItem)
        };
      });

      this._inputElement.value = "";
    } else {
      this.setState(() => {
        return {
          wordEmpty: true
        };
      });
    }

    console.log(this.state.wordItems);
    e.preventDefault();

  }

  deleteWordItem(key) {
    var filteredWordItems = this.state.wordItems.filter(function (wordItem) {
      return (wordItem.key !== key);
    });

    this.setState({
      wordItems: filteredWordItems
    });
  }

  handleWordEmptyClose = () => this.setState({ wordEmpty: false });


}

export default TopicDataItem;
