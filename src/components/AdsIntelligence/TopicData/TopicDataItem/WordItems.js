import React, { Component } from "react";

class WordItems extends Component {
  constructor(props) {
    super(props);

    this.createTasks = this.createTasks.bind(this);
  }

  createTasks(wordItem) {
    return (
      <div key={wordItem.key} className="topic-item__list--item">

          <div className="topic-item__list--name">
            {wordItem.text}
          </div>

          <div className="topic-item__list--total">
            0
          </div>

          <div className="topic-item__list--delete">
            <i className="fas fa-times" onClick={() => this.delete(wordItem.key)}></i>
          </div>

      </div>
    )
  }

  delete(key) {
    this.props.delete(key);
  }

  render() {
    var wordEntries = this.props.entries;
    var wordListItems = wordEntries.map(this.createTasks);

    return (
      <div className="topic-item__list">
          {wordListItems}
      </div>
    );
  }
};

export default WordItems;
