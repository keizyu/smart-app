import React, { Component } from 'react'
import { Card, Elevation, H2 } from "@blueprintjs/core"
import { connect } from 'react-redux'
import numeral from 'numeral'

import './styles.css'

class Placement extends Component {

  render () {

      return(
          <Card interactive={true} elevation={Elevation.TWO} className="no-pad-t">
            <div className="subtitle">
              Placement
            </div>
            <div className="total-activity placement-container">

              <div className="flex-holder">

                <div className="total-activity__item placement-container__item">
                  <div className="total-activity__icon placement-container__item--fb">
                    <i class="fab fa-facebook-f"></i>
                  </div>
                  <div className="total-activity__text">
                    <H2 className="total-activity__text--data">
                      { numeral(189.00).format('0,0') }
                    </H2>

                  </div>
                </div>

                <div className="total-activity__item placement-container__item">
                  <div className="total-activity__icon placement-container__item--instagram">
                    <i class="fab fa-instagram"></i>
                  </div>
                  <div className="total-activity__text">
                    <H2 className="total-activity__text--data">
                      { numeral(16.01).format('0,0') }
                    </H2>

                  </div>
                </div>

                <div className="total-activity__item placement-container__item">
                  <div className="total-activity__icon placement-container__item--audience">
                    <i class="fas fa-users"></i>
                  </div>
                  <div className="total-activity__text">
                    <H2 className="total-activity__text--data">
                      { numeral(39.09).format('0,0') }
                    </H2>

                  </div>
                </div>

                <div className="total-activity__item placement-container__item">
                  <div className="total-activity__icon placement-container__item--comments">
                    <i class="fas fa-comment"></i>
                  </div>
                  <div className="total-activity__text">
                    <H2 className="total-activity__text--data">
                      { numeral(0.1).format('0,0') }
                    </H2>

                  </div>
                </div>

              </div>
            </div>
          </Card>
      );
  }
}


function mapStateToProps( state )
{
  return {
  }
}

export default connect(mapStateToProps)( Placement );
