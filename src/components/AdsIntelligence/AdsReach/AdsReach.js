import React, { Component } from 'react'
import { Card, Elevation } from "@blueprintjs/core"

class AdsReach extends Component {
  render () {
    return (
      <Card interactive={true} elevation={Elevation.TWO} className="no-pad-t">
        <div className="subtitle">
          Ads Reach
        </div>

        <div className="grid">
          <div className="col-6">
            <div className="competition-investment__item">
              <p className="competition-investment__data">
                MXN 1,419
              </p>
              <img src="http://smart2.t6hdigital.com/images/competitors/296260597436180_competitor3.png" alt="logo" />
            </div>
          </div>
          <div className="col-6">
            <div className="competition-investment__item">
              <p className="competition-investment__data">
                MXN 1,419
              </p>
              <img src="http://smart2.t6hdigital.com/images/competitors/296260597436180_competitor3.png" alt="logo" />
            </div>
          </div>
          <div className="col-6">
            <div className="competition-investment__item">
              <p className="competition-investment__data">
                MXN 1,419
              </p>
              <img src="http://smart2.t6hdigital.com/images/competitors/296260597436180_competitor3.png" alt="logo" />
            </div>
          </div>
          <div className="col-6">
            <div className="competition-investment__item">
              <p className="competition-investment__data">
                MXN 1,419
              </p>
              <img src="http://smart2.t6hdigital.com/images/competitors/296260597436180_competitor3.png" alt="logo" />
            </div>
          </div>
        </div>



      </Card>
    )
  }
}

export default AdsReach;
