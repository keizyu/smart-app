import React, { Component } from 'react'
import { Card, Elevation } from "@blueprintjs/core"
import { Column, Table, AutoSizer } from 'react-virtualized'
import { connect } from 'react-redux'
import numeral from 'numeral'

class Demographics extends Component {

  constructor(props)
  {
      super(props);
      this.state = {
        page_fans_country: [
          {country: "MX", fans: 8765},
          {country: "US", fans: 33},
          {country: "CA", fans: 12},
          {country: "ES", fans: 7},
          {country: "IN", fans: 6},
          {country: "CO", fans: 5},
          {country: "HN", fans: 3},
          {country: "BR", fans: 3},
          {country: "PE", fans: 3}
        ]
      }

  }

  renderPaises(){
      return (
        <div className="paises">
         { this.state.page_fans_country.map(function(pais){

             return (
               <div key={pais.country} className="paisesMap">
                 <img src={require('../../../assets/img/flags/'+ pais.country.toLowerCase() +'.png')} alt={pais.country}/>
               </div>

             );
         })}
       </div>
      )
  }

  render () {

    let ciudadesLista = [
      {city: "Veracruz, Mexico", fans: 2988},
      {city: "Xalapa, Veracruz, Mexico", fans: 1006},
      {city: "Mexico City, Distrito Federal, Mexico", fans: 566},
      {city: "Córdoba, Veracruz, Mexico", fans: 339},
      {city: "Orizaba, Veracruz, Mexico", fans: 335},
      {city: "Boca del Río, Veracruz, Mexico", fans: 264}
    ]

    return(

          <Card interactive={true} elevation={Elevation.TWO} className="no-pad-t">
            <div className="subtitle">
              Demographics
            </div>

            <div className="grid">
              <div className="col-6">

                <div className="subtitle subtitle__no-border followers">
                  ADs Reach <br /><br />
                  <div className="followers-data">
                    { numeral(99).format('0,0') }
                  </div>
                </div>

              </div>

              <div className="col-6">

                <div className="subtitle subtitle__no-border followers">
                  Paid Followers <br /><br />
                  <div className="followers-data">
                    { numeral(14).format('0,0') }
                  </div>
                </div>

              </div>
            </div>

            { this.renderPaises() }

            <AutoSizer disableHeight>
            	{
              	({ width }) => {

                	return (

                    <Table
                      width={width}
                      height={164}
                      headerHeight={26}
                      rowHeight={26}
                      rowCount={ciudadesLista.length}
                      rowGetter={({ index }) => ciudadesLista[index]}
                    >
                      <Column
                        label='City'
                        dataKey='city'
                        width={300}
                      />
                      <Column
                        width={150}
                        label='Fans'
                        dataKey='fans'
                      />
                    </Table>

									)
              	}
            	}
            </AutoSizer>
          </Card>
    );
  }

}

function mapStateToProps( state )
{
  return {

  }
}

export default connect(mapStateToProps)( Demographics );
