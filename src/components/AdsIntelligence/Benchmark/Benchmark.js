import React, { Component } from 'react'
import { Card, Elevation } from "@blueprintjs/core"
import { Doughnut } from 'react-chartjs-2'
import { connect } from 'react-redux'

class BenchMark extends Component {

	renderDonut(){
			let Data = {
				labels: [
					'Brand 1',
			    'Brand 2',
          'Brand 3',
          'Brand 4'
				],
				datasets: [{
			    label: 'Population',
					data: [3,1,1,2],
					backgroundColor: [
            '#b3ff26',
						'#2aa9e9',
						'#ff9615',
			  		'#00cf56'
			    ],
			    borderColor: '#363737',
					borderWidth: 0
				}],
			};

			return(
					<div>
						<div className="subtitle">
							Benchmark
						</div>

						<Doughnut
							data={ Data }
							width={180}
							options={{
								legend: {
									display: false,
								}
							}}
						/>
					</div>
				);
	}

  render () {

    return(

        <Card interactive={true} elevation={Elevation.TWO} className="no-pad-t">
          { this.renderDonut() }
        </Card>

    );
  }
}

function mapStateToProps( state )
{
  return {

  }
}

export default connect(mapStateToProps)( BenchMark );
