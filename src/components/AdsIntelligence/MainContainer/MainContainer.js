import React, { Component } from 'react'
import { Button, H1, Spinner } from "@blueprintjs/core"
import { DateRangeInput } from "@blueprintjs/datetime"
import Cookies from 'universal-cookie'
import moment from 'moment'
import { connect } from 'react-redux'

//Componentes
import AdsScore from '../AdsScore/AdsScore'
import Demographics from '../Demographics/Demographics'
import Audience from '../Audience/Audience'
import Languages from '../Languages/Languages'
import Sentiment from '../Sentiment/Sentiment'
import BestPerformingAds from '../BestPerformingAds/BestPerformingAds'
import Interactions from '../Interactions/Interactions'
import AdsDistribution from '../AdsDistribution/AdsDistribution'
import TotalActivity from '../TotalActivity/TotalActivity'
import TopicData from '../TopicData/TopicData'
import TopInterests from '../TopInterests/TopInterests'
import TotalSpend from '../TotalSpend/TotalSpend'
import Placement from '../Placement/Placement'
import PaidMedia from '../PaidMedia/PaidMedia'
import Benchmark from '../Benchmark/Benchmark'
import CompetitionInvestment from '../CompetitionInvestment/CompetitionInvestment'
import AdsReach from '../AdsReach/AdsReach'
import BestPerformingAdsBottom from '../BestPerformingAdsBottom/BestPerformingAdsBottom'

import { fetchCampaignADS } from '../../../actions'

import './styles.css'

class MainContainer extends Component {

  constructor(props) {
    super(props)

    // se obtiene la fecha de la cookie
    const cookies = new Cookies();
    let range = cookies.get('range')

    if( !range )
    {
      range = [ new Date(moment().subtract(15, "days").format('YYYY-MM-DD')), new Date(moment().format('YYYY-MM-DD'))]
    }
    else
    {
      range = [ new Date(range[0]), new Date(range[1])];
    }

    cookies.set('range', range, { path: '/' });

    this.state = {
        range: range,
    };

  }

  componentWillMount()
  {
    this.props.fetchCampaignADS( this.props.id, this.state.range );
  }


  render () {
    if( this.props.loading )
    {
      return(<Spinner/>);
    }

    console.log( "============================================== Campains" );
    console.log( this.props.campaignAds );
    console.log( "==============================================" );

    return(


      <div className="main-container__ads">

        <div className="top-container">
          <div className="brands-added">

            <div className="grid">

              <div className="col-5_sm-12">
                <H1 className="brands-added__title">ADs Intelligence</H1>
              </div>

              <div className="col-7_sm-12">

                <div className="searchContainer">
                  <div className="dateSmart bp3-dark">
                    <span className="searchCalendarIcon"><i className="far fa-calendar-alt"></i></span>
                    <DateRangeInput
                        formatDate={date => moment(date).format("YYYY-MM-DD")}
                        onChange={this.handleRangeChange}
                        parseDate={str => new Date(str)}
                        value={this.state.range}
                        placeholder={"YYYY-mm-dd"}
                    />
                    <Button onClick={this.openSearchCalendar} minimal="true" className="button btn-outline" >Search</Button>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>

        <div className="social-intelligence__container">


          <div className="grid">
            <div className="col-12">
                <AdsScore id={ this.props.id } range={this.state.range} />
            </div>

            <div className="col-8_sm-12">
                <TopInterests />
            </div>

            <div className="col-4_sm-12 no-pad-b">

              <div className="col-12">
                <div className="best-days-shell">
                  <div className="grid">

                      <div className="col-12 no-pad-l-f">
                        <TotalSpend range={this.state.range} />
                      </div>

                      <div className="col-12 no-pad-l-f no-pad-b">
                        <Placement range={this.state.range} />
                      </div>

                  </div>
                </div>
              </div>

            </div>

            <div className="col-4_sm-12">
                <Demographics id={ this.props.id } />
            </div>

            <div className="col-4_sm-12">
                <Audience id={this.props.id} />
            </div>

            <div className="col-4_sm-12 no-pad-b">
                <div className="col-12">
                  <div className="best-days-shell">
                    <div className="grid">

                        <div className="col-12 no-pad-l-f">
                          <Languages range={this.state.range} />
                        </div>

                        <div className="col-12 no-pad-l-f no-pad-b">
                          <Sentiment range={this.state.range} />
                        </div>

                    </div>
                  </div>
                </div>
            </div>

            <div className="col-12">
                <BestPerformingAds id={this.props.id} />
            </div>

            <div className="col-12">
                <TopicData id={this.props.id} />
            </div>

            <div className="col-4_sm-12">
                <Interactions id={this.props.id} />
            </div>

            <div className="col-4_sm-12">
                <AdsDistribution id={this.props.id} />
            </div>

            <div className="col-4_sm-12">
                <TotalActivity id={this.props.id} />
            </div>

            <div className="col-12">
                <PaidMedia id={this.props.id} />
            </div>

            <div className="col-4_sm-12">
                <Benchmark id={this.props.id} />
            </div>

            <div className="col-4_sm-12">
                <CompetitionInvestment id={this.props.id} />
            </div>

            <div className="col-4_sm-12">
                <AdsReach id={this.props.id} />
            </div>

            <div className="col-12">
                <BestPerformingAdsBottom id={this.props.id} />
            </div>

          </div>
        </div>
      </div>
    );
  }


  openSearchCalendar = () =>  {

  }

  handleRangeChange = (range) => {
    const cookies = new Cookies();
    cookies.set('range', range, { path: '/' });
    this.setState({ range });
  }

}

function mapStateToProps( state )
{
  return {
    loading: state.campaing_ads.loading,
    campaignAds: state.campaing_ads.campaigns,
  }
}

export default connect(mapStateToProps, { fetchCampaignADS })( MainContainer );
