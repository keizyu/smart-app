import React, {	Component } from 'react'
import { Card, Elevation } from "@blueprintjs/core"

class TotalSpend extends Component {
	render() {

		const dataStyle = {
		  margin: '47px 0',
		  textAlign: 'center'
		}

		return (
      <Card interactive={true} elevation={Elevation.TWO} className="no-pad-t">
        <div className="subtitle">
          Total Spend
        </div>


        <h1 style={dataStyle}>MXN 500</h1>

      </Card>
		)
	}
}

export default TotalSpend;
