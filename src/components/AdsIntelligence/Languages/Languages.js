import React, { Component } from 'react'
import { Card, Elevation } from "@blueprintjs/core"
import { connect } from 'react-redux'


class Languages extends Component {

  render () {

    const dataStyle = {
		  margin: '93px 0',
		  textAlign: 'center'
		}

    return(

        <Card interactive={true} elevation={Elevation.TWO} className="no-pad-t">
          <div className="subtitle">
            Languages
          </div>


          <h3 style={dataStyle}>Use all LANGUAGES available</h3>

        </Card>

    )
  }

}

function mapStateToProps( state )
{
  return {
  }
}

export default connect(mapStateToProps)( Languages );
