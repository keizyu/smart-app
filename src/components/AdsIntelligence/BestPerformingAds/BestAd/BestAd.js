import React, { Component } from 'react';
import logoSmart from '../../../../assets/img/logo.png';
import { Card, Elevation } from "@blueprintjs/core";
import Truncate from './Truncate'
import moment from 'moment'

class BestAd extends Component {
  
  render (props) {

    return(

        <Card interactive={true} elevation={Elevation.TWO} className="bestpostContainer">
          <div className="bestPost">
            <div className="post-topContainer">
              <div className="postImage bestpostImage">
                 <img alt={this.props.bestAd.postImage.img} src={ this.props.bestAd.postImage.url } />
              </div>
            </div>

            <div className="postMessage">

                <Truncate text={this.props.bestAd.postMessage.message} />

            </div>

            <div className="postDate">
              <p>{moment(this.props.bestAd.postDate).format("ll")}</p>
              <img src={logoSmart} alt="Logo Smart" className="logoSmartPost" />
            </div>

          </div>
        </Card>

    );
  }

}

export default BestAd;
