import React, { Component } from 'react';
import { Card, Elevation, H2 } from "@blueprintjs/core";
import { connect } from 'react-redux';
import numeral from 'numeral'


class TotalActivity extends Component {

  render () {

      return(
          <Card interactive={true} elevation={Elevation.TWO} className="no-pad-t">
            <div className="subtitle">
              Total Activity
            </div>
            <div className="total-activity">

              <div className="flex-holder">

                <div className="total-activity__item total-activity__item--impressions">
                  <div className="total-activity__icon">
                    <i className="far fa-eye"></i>
                  </div>
                  <div className="total-activity__text">
                    <H2 className="total-activity__text--data">
                      { numeral(189.00).format('0,0') }
                    </H2>
                    <span className="total-activity__text--name">Impresions</span>
                  </div>
                </div>

                <div className="total-activity__item total-activity__item--engagement">
                  <div className="total-activity__icon">
                    <i className="far fa-thumbs-up"></i>
                  </div>
                  <div className="total-activity__text">
                    <H2 className="total-activity__text--data">
                      { numeral(16.01).format('0,0') }
                    </H2>
                    <span className="total-activity__text--name">Engagements</span>
                  </div>
                </div>

                <div className="total-activity__item total-activity__item--posts">
                  <div className="total-activity__icon">
                    <i className="fas fa-edit"></i>
                  </div>
                  <div className="total-activity__text">
                    <H2 className="total-activity__text--data">
                      { numeral(39.09).format('0,0') }
                    </H2>
                    <span className="total-activity__text--name">ADs</span>
                  </div>
                </div>

                <div className="total-activity__item total-activity__item--video">
                  <div className="total-activity__icon">
                    <i className="fas fa-play"></i>
                  </div>
                  <div className="total-activity__text">
                    <H2 className="total-activity__text--data">
                      { numeral(0.1).format('0,0') }
                    </H2>
                    <span className="total-activity__text--name">Video Views</span>
                  </div>
                </div>

              </div>
            </div>
          </Card>
      );
  }
}


function mapStateToProps( state )
{
  return {
  }
}

export default connect(mapStateToProps)( TotalActivity );
