import React, { Component } from 'react';
import { Card, Elevation } from "@blueprintjs/core";
import { Bar } from 'react-chartjs-2';
import { connect } from 'react-redux';

class Sentiment extends Component {

  constructor(props){
		super(props);
		this.state = {}
	}

  render () {

    let audienceData = {
      labels: [
        'positive',
        'neutral',
        'negative'
      ],
      datasets: [{
        data: [2, 20, 80 ],
        backgroundColor: [
          '#CF3',
          '#5ebaeb',
          '#e2474f'
        ]
      }],
    }

    return(

          <Card interactive={true} elevation={Elevation.TWO} className="no-pad-t">
            <div className="subtitle">
              Sentiment
            </div>

            <Bar
              data={ audienceData }
              width={330}
              options={{
                legend: {
                  display: false,
                },
                scales: {
      						xAxes: [{
      							barPercentage: 1,
      							display: true,
      							gridLines: {
      								display: true,
      								color: "#636363",
      								drawTicks: false,
      							},
      							ticks: {
      								fontColor: "#fff",
      								padding: 14,
      							}
      						}],
      						yAxes: [{
      							display: true,
      							gridLines: {
      								display: true,
      								color: "#636363",
      								drawTicks: false,
      							},
      							ticks: {
      								fontColor: "#fff",
      								padding: 10,
      							}
      						}]
      					}
              }}
            />

          </Card>

    );
  }

}

function mapStateToProps( state )
{
  return {

  }
}

export default connect(mapStateToProps)( Sentiment );
