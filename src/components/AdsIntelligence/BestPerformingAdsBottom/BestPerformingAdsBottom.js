import React, { Component } from 'react'
import { Card, Elevation } from '@blueprintjs/core'
import { connect } from 'react-redux'
import _ from 'lodash'
import logoSmartCom from '../../../assets/img/loginsmart.png'
import logoSmart from '../../../assets/img/logo.png'
import Truncate from './Truncate'
import moment from 'moment'

import './styles.css'

class BestPerformingAdsBottom extends Component {

    renderBestAd(bestAd, index){

      const views = bestAd.map((bestAd, _index) => {
        // const position = (4 * index) + _index

        return (
          <Card key={_index} interactive={true} elevation={Elevation.TWO} className="bestpostContainer">
            <div className="bestPost">
              <div className="post-topContainer">
                <div className="postImage bestpostImage">
                   <img alt={bestAd.postImage.img} src={ bestAd.postImage.url } />
                </div>
              </div>

              <div className="postMessage">
                  <Truncate text={bestAd.postMessage.message} />
              </div>

              <div className="postDate">
                <p>Fecha </p>
                <p>{moment(bestAd.postDate).format("ll")}</p>
                <img src={logoSmart} alt="Logo Smart" className="logoSmartPost" />
              </div>

            </div>
          </Card>
        )

      })
      const placeholder = _.range(4 - bestAd.length).map((element, i)=>{

        return (
          <Card key={ "item" + i } interactive={true} elevation={Elevation.TWO} className="bestpostContainer placeholderAd">
            <div className="bestPost">
              <img src={logoSmartCom} alt="Logo Smart" className="placeholderAd-image" />
            </div>
          </Card>

        )

      })

      return _.concat(views, placeholder)

    }

    render() {

      let listado_bestPerformingAds = [
        {categoria: "Sin categoría",categoria_id: 1,engScore: 18,id: "fb301",post: "Te esperamos en la Expo del Triatlón Veracruz WTC, Boca del Río. ¡No te lo pierdas! #specializedbicycles #astriveracruz",postDate: "Fri, 31 Aug 2018 15:42:46 GMT",postExtras: {postCategory: "Post Category"},postEngagment: {competition: {data: 4.7202, title: "ES Competition"},},score: {data: 18.52,title: "ENG Score"},postType:"",postImage:{img: "image",url: "https://scontent.xx.fbcdn.net/v/t1.0-9/s720x720/40463912_617826151946288_3454092349611704320_n.jpg?_nc_cat=0&oh=8bd9977b3263bde99fcdde389c9a7790&oe=5C2635C6"},postMessage: {hashTag: "#astriveracruz, #specializedbicycles",message: "Te esperamos en la Expo del Triatlón Veracruz WTC,…! #specializedbicycles #astriveracruz"},provider:"fb"},
        {categoria: "Sin categoría",categoria_id: 1,engScore: 18,id: "fb301",post: "Te esperamos en la Expo del Triatlón Veracruz WTC, Boca del Río. ¡No te lo pierdas! #specializedbicycles #astriveracruz",postDate: "Fri, 31 Aug 2018 15:42:46 GMT",postExtras: {postCategory: "Post Category"},postEngagment: {competition: {data: 4.7202, title: "ES Competition"},},score: {data: 18.52,title: "ENG Score"},postType:"",postImage:{img: "image",url: "https://scontent.xx.fbcdn.net/v/t1.0-9/s720x720/40463912_617826151946288_3454092349611704320_n.jpg?_nc_cat=0&oh=8bd9977b3263bde99fcdde389c9a7790&oe=5C2635C6"},postMessage: {hashTag: "#astriveracruz, #specializedbicycles",message: "Te esperamos en la Expo del Triatlón Veracruz WTC,…! #specializedbicycles #astriveracruz"},provider:"fb"},
        {categoria: "Sin categoría",categoria_id: 1,engScore: 18,id: "fb301",post: "Te esperamos en la Expo del Triatlón Veracruz WTC, Boca del Río. ¡No te lo pierdas! #specializedbicycles #astriveracruz",postDate: "Fri, 31 Aug 2018 15:42:46 GMT",postExtras: {postCategory: "Post Category"},postEngagment: {competition: {data: 4.7202, title: "ES Competition"},},score: {data: 18.52,title: "ENG Score"},postType:"",postImage:{img: "image",url: "https://scontent.xx.fbcdn.net/v/t1.0-9/s720x720/40463912_617826151946288_3454092349611704320_n.jpg?_nc_cat=0&oh=8bd9977b3263bde99fcdde389c9a7790&oe=5C2635C6"},postMessage: {hashTag: "#astriveracruz, #specializedbicycles",message: "Te esperamos en la Expo del Triatlón Veracruz WTC,…! #specializedbicycles #astriveracruz"},provider:"fb"},
      ]

      let group = _.chunk(listado_bestPerformingAds, 4)

        return (

          <Card interactive={true} elevation={Elevation.TWO} className="no-pad-t best-competitor">
            <div className="subtitle">
              Best Performing ADs
            </div>

            <div className="grid">

              <div className="col-3">
                <div className="best-competitor-image-holder">
                  <div className="best-competitor__image">
                    <img src="http://smart2.t6hdigital.com/images/competitors/296260597436180_competitor1.png" alt="" />
                  </div>
                </div>
              </div>
              <div className="col-9">
                <div className="bestPost-shell">

                    { group.map((bestAd, index) => {
                        return this.renderBestAd(bestAd, index)
                      })
                    }

                </div>
              </div>

            </div>

          </Card>

        );
    }
}

function mapStateToProps( state )
{
  return {
  }
}

export default connect(mapStateToProps)( BestPerformingAdsBottom );
