import React, { Component } from 'react'
import { Card, Elevation } from "@blueprintjs/core"

import './styles.css'

class PaidMedia extends Component {
  render () {
    return (
      <Card interactive={true} elevation={Elevation.TWO} className="no-pad-t">
        <div className="subtitle">
          Paid Media ER Competition
        </div>

        <div className="grid">
          <div className="col-3_sm-12">
            <div className="ads-competitor">
              <div className="ads-competitor__avatar">
               <img src="http://smart2.t6hdigital.com/images/competitors/296260597436180_brand.png" alt="logo" />
              </div>
              <div className="ads-competitor__info">
              <p className="ads-competitor__data">9.071</p>
              <p className="ads-competitor__rank">#1</p>
              </div>
            </div>
          </div>

          <div className="col-3_sm-12">
            <div className="ads-competitor">
              <div className="ads-competitor__avatar">
                <img src="http://smart2.t6hdigital.com/images/competitors/296260597436180_brand.png" alt="logo" />
              </div>
              <div className="ads-competitor__info">
              <p className="ads-competitor__data">9.071</p>
              <p className="ads-competitor__rank">#2</p>
              </div>
            </div>
          </div>

          <div className="col-3_sm-12">
            <div className="ads-competitor">
              <div className="ads-competitor__avatar">
                <img src="http://smart2.t6hdigital.com/images/competitors/296260597436180_brand.png" alt="logo" />
              </div>
              <div className="ads-competitor__info">
              <p className="ads-competitor__data">9.071</p>
              <p className="ads-competitor__rank">#3</p>
              </div>
            </div>
          </div>

          <div className="col-3_sm-12">
            <div className="ads-competitor">
              <div className="ads-competitor__avatar">
                <img src="http://smart2.t6hdigital.com/images/competitors/296260597436180_brand.png" alt="logo" />
              </div>
              <div className="ads-competitor__info">
              <p className="ads-competitor__data">9.071</p>
              <p className="ads-competitor__rank">#4</p>
              </div>
            </div>
          </div>


        </div>




      </Card>
    )
  }
}

export default PaidMedia;
