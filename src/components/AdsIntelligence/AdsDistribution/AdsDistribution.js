import React, { Component } from 'react'
import { Card, Elevation } from "@blueprintjs/core"
import { Doughnut } from 'react-chartjs-2'
import { connect } from 'react-redux'

class AdsDistribution extends Component {

	renderDonut()
	{
			let Data = {
				labels: [
					'Tue',
			    'Thu'
				],
				datasets: [{
			    label: 'Population',
					data: [1, 1],
					backgroundColor: [
						'#b3ff26',
						'#2aa9e9'
			    ],
			    borderColor: '#363737',
					borderWidth: 0
				}],
			};

			return(
					<div>
						<div className="subtitle">
							Ads Distribution
						</div>
						<Doughnut
							data={ Data }
							width={130}
							options={{
								legend: {
									display: false,
								}
							}}
						/>
					</div>
				);
	}

  render () {

    return(

        <Card interactive={true} elevation={Elevation.TWO} className="no-pad-t">
          { this.renderDonut() }
        </Card>

    );
  }
}

function mapStateToProps( state )
{
  return {

  }
}

export default connect(mapStateToProps)( AdsDistribution );
