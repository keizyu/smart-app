import React, { Component } from 'react';
import { Card, Elevation } from "@blueprintjs/core";
import { Column, Table, AutoSizer } from 'react-virtualized';
import { Bar } from 'react-chartjs-2';
import { connect } from 'react-redux';
import { fetchSocialBestCats, fetchSocialBestCatsGraph } from '../../../actions';
import { Spinner } from '@blueprintjs/core';

class Interactions extends Component {

	constructor(props){
		super(props);
		this.state = {
			engagementData : {
				labels: [],
				datasets: [{
					data: [],
					backgroundColor: [],
				}],
			}
		}
	}

	componentWillMount()
  {
		//this.props.fetchSocialBestCatsGraph( this.props.id );
  }

	renderEngage()
	{
		if( this.props.loading_graph )
		{
			return( <Spinner /> )
		}
		else
		{
			return(
				<div>
					<div className="engagement-main engagement-score">

						<h3 className="engagement-main__title">ENG Score</h3>
						<div className="engagement-main__data">
							4.97%
						</div>
					</div>

					<div className="engagement-main engagement-competition">
						<h3 className="engagement-main__title">ENG Competition</h3>
						<div className="engagement-main__data">
							4.97%
						</div>
					</div>
				</div>
			)
		}
	}

	renderTable()
	{
		if( this.props.loading_graph )
		{
			return( <Spinner /> )
		}
		else
		{
      let table_data = [
				["Sin categoria", 331],
				["Precio", 15]
			];

			return (
				<AutoSizer disableHeight>
					{
						({ width }) => {

							return (
								<Table
							    width={width}
							    height={140}
							    headerHeight={30}
							    rowHeight={26}
							    rowCount={table_data.length}
							    rowGetter={({ index }) => table_data[index]}
							  >
							    <Column
							      label='Category'
							      dataKey='0'
							      width={width / 2}
							    />
							    <Column
							      width={width / 2}
							      label='E Score'
							      dataKey='1'
							    />
							  </Table>
							)
						}
					}
				</AutoSizer>
			)
		}
	}

	renderGraph()
	{

				// let renderTableData = this.props.graph.graph_data;

				let engagementData={
					labels: [
						"Sin Categoria",
						"Precio"
					],
					datasets: [{
						data: [
							200,
							50
						],
						backgroundColor: [
							'#CF3',
							'#5ebaeb'
						],
					}],
				}


				// console.log("data: ------>", this.props.graph.graph_data)

				return(
				<Bar
					data={ engagementData }
					width={230}
					options={{
						layout: {
							padding: {
									left: 0,
									right: 20,
									top: 0,
									bottom: 0
							}
						},
						legend: {
							display: false,
						},
						scales: {
							xAxes: [{
								barPercentage: 1,
								display: true,
								gridLines: {
									display: true,
									color: "#636363",
									drawTicks: false,
								},
								ticks: {
									fontColor: "#fff",
									padding: 14,
	                callback: function(value) {
	                    return value.substr(0, 7) + "..."; //truncate
	               	}
            		}
							}],
							yAxes: [{
								display: true,
								gridLines: {
									display: true,
									color: "#636363",
									drawTicks: false,
								},
								ticks: {
									fontColor: "#fff",
									padding: 10,
								}
							}]
						}
					}}
				/>
			)
	}

	renderComponent()
	{
		if( this.props.loading_graph )
		{
			return( <Card interactive={true} elevation={Elevation.TWO} className="no-pad-t"><Spinner /></Card> )
		}
		else
		{
		return(

			<Card interactive={true} elevation={Elevation.TWO} className="no-pad-t">
				<div className="subtitle">
					Interactions
				</div>

				<div className="engagement">
					{ this.renderEngage() }
				</div>
				<div className="chart-container">
					{ this.renderGraph() }
				</div>
				{ this.renderTable() }
			</Card>

			);
		}
	}

  render () {

    return(
        <div>{ this.renderComponent() }</div>
    );
  }


}

function mapStateToProps( state )
{
  return {
		graph: state.graph.graph,
  }
}

export default connect(mapStateToProps, { fetchSocialBestCats, fetchSocialBestCatsGraph })( Interactions );
