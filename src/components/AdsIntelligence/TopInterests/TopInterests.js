import React, {	Component } from 'react'
import { Card, Elevation } from "@blueprintjs/core"
import { Column, Table, AutoSizer } from 'react-virtualized'

class TopInterests extends Component {
	render() {

		let adsInterestRank = [
      {interests: "1. Interests: CyclingTips", adsScore: 0.03},
      {interests: "2. Interests: CyclingTips", adsScore: 0.03},
      {interests: "3. Interests: CyclingTips", adsScore: 0.03},
      {interests: "4. Interests: CyclingTips", adsScore: 0.03},
      {interests: "5. Interests: CyclingTips", adsScore: 0.03},
      {interests: "6. Interests: CyclingTips", adsScore: 0.03},
			{interests: "7. Interests: CyclingTips", adsScore: 0.03},
      {interests: "8. Interests: CyclingTips", adsScore: 0.03},
      {interests: "9. Interests: CyclingTips", adsScore: 0.03},
      {interests: "10. Interests: CyclingTips", adsScore: 0.03},
      {interests: "11. Interests: CyclingTips", adsScore: 0.03},
      {interests: "12. Interests: CyclingTips", adsScore: 0.03},
			{interests: "13. Interests: CyclingTips", adsScore: 0.03},
      {interests: "14. Interests: CyclingTips", adsScore: 0.03},
      {interests: "15. Interests: CyclingTips", adsScore: 0.03}
    ]

		let adsInterestRankHalf = [
      {interests: "16. Interests: CyclingTips", adsScore: 0.03},
      {interests: "17. Interests: CyclingTips", adsScore: 0.03},
      {interests: "18. Interests: CyclingTips", adsScore: 0.03},
      {interests: "19. Interests: CyclingTips", adsScore: 0.03},
      {interests: "20. Interests: CyclingTips", adsScore: 0.03},
      {interests: "21. Interests: CyclingTips", adsScore: 0.03},
			{interests: "22. Interests: CyclingTips", adsScore: 0.03},
      {interests: "23. Interests: CyclingTips", adsScore: 0.03},
      {interests: "24. Interests: CyclingTips", adsScore: 0.03},
      {interests: "25. Interests: CyclingTips", adsScore: 0.03},
      {interests: "26. Interests: CyclingTips", adsScore: 0.03},
      {interests: "27. Interests: CyclingTips", adsScore: 0.03},
			{interests: "28. Interests: CyclingTips", adsScore: 0.03},
      {interests: "29. Interests: CyclingTips", adsScore: 0.03},
      {interests: "30. Interests: CyclingTips", adsScore: 0.03}
    ]

		let tableStyle = {
			backgroundColor : '#2b2b2b',
			padding : '3px 0'
		}


		return (
      <Card interactive={true} elevation={Elevation.TWO} className="no-pad-t">
        <div className="subtitle">
          Top Interests
        </div>


        <div className="grid">

					<div className="col-6_sm-12">
						<div style={tableStyle}>
							<AutoSizer disableHeight>
								{
									({ width }) => {

										return (

											<Table
												width={width}
												height={420}
												headerHeight={26}
												rowHeight={26}
												rowCount={adsInterestRank.length}
												rowGetter={({ index }) => adsInterestRank[index]}
											>
												<Column
													label='Interests'
													dataKey='interests'
													width={300}
												/>
												<Column
													width={100}
													label='ADs Score'
													dataKey='adsScore'
												/>
											</Table>

										)
									}
								}
							</AutoSizer>
						</div>
					</div>

					<div className="col-6_sm-12">
						<div style={tableStyle}>
							<AutoSizer disableHeight>
								{
									({ width }) => {

										return (

											<Table
												width={width}
												height={420}
												headerHeight={26}
												rowHeight={26}
												rowCount={adsInterestRankHalf.length}
												rowGetter={({ index }) => adsInterestRankHalf[index]}
											>
												<Column
													label='Interests'
													dataKey='interests'
													width={300}
												/>
												<Column
													width={100}
													label='ADs Score'
													dataKey='adsScore'
												/>
											</Table>

										)
									}
								}
							</AutoSizer>
						</div>
					</div>

				</div>

      </Card>
		)
	}
}

export default TopInterests;
