import React, { Component } from 'react'
import { Card, Elevation } from "@blueprintjs/core"
import { Bar } from 'react-chartjs-2'
import { connect } from 'react-redux'

class Audience extends Component {

  render () {

		let genderWidth = {
			maleWidth:{
				width: '50%'
			},
			femaleWidth:{
				width: '50%',
			}
		}

		let audienceGraphData = {
			labels: [
				'17-55'
			],
			datasets: [{
				data: [3500, 2500, 500 ],
				backgroundColor: [
					'#b3ff26'
				]
			}],
		}

    return(
          <Card interactive={true} elevation={Elevation.TWO} className="no-pad-t">
            <div className="subtitle">
              Audience
            </div>

						<div className="subtitle subtitle__no-border">
              Gender
            </div>

						<div className="chart-container gender">
								<div className="gender__item gender__item-female" style={ genderWidth.femaleWidth }>
									<div className="flex-holder">
										<div className="gender__item--icon">
											<i className="fas fa-female fa-4x"></i>
										</div>

										<span className="gender__item--data">
											{ genderWidth.femaleWidth.width }
										</span>
								</div>
								</div>


								<div className="gender__item gender__item-male"  style={ genderWidth.maleWidth }>
									<div className="flex-holder">
										<div className="gender__item--icon">
											<i className="fas fa-male fa-4x"></i>
										</div>
										<span className="gender__item--data">
											{ genderWidth.maleWidth.width }
										</span>
									</div>
								</div>

						</div>

						<div className="subtitle subtitle__no-border">
              Ages
            </div>

						<Bar
							data={ audienceGraphData }
							width={330}
							options={{
								legend: {
									display: false,
								},
								scales: {
									xAxes: [{
										barPercentage: 1,
										display: true,
										gridLines: {
											display: true,
											color: "#636363",
											drawTicks: false,
										},
										ticks: {
											fontColor: "#fff",
											padding: 14,
										}
									}],
									yAxes: [{
										display: true,
										gridLines: {
											display: true,
											color: "#636363",
											drawTicks: false,
										},
										ticks: {
											fontColor: "#fff",
											padding: 10,
										}
									}]
								}
							}}
						/>


          </Card>

    );
  }

}

function mapStateToProps( state )
{
  return {

  }
}

export default connect(mapStateToProps)( Audience );
