import React, { Component } from 'react'
import { Card, Elevation, H2, Tooltip, Position } from "@blueprintjs/core"
import { connect } from 'react-redux'
import { fetchSocialTotalActivity } from '../../../actions'
import { Spinner } from '@blueprintjs/core'
import numeral from 'numeral'

import './styles.css'


class TotalActivity extends Component {
  // constructor(props){
  //   super(props);
  // }

  componentWillMount()
  {
    //this.props.fetchSocialTotalActivity( this.props.id );
  }

  render () {
    // console.log( "Cargando? ", this.props.total.loading );

  		if( this.props.loading )
  		{
  			return( <Spinner /> )
  		}
  		else
  		{

        console.log("total activyt -> ");
        console.log( this.props.total );

        return(
            <Card interactive={true} elevation={Elevation.TWO} className="no-pad-t">
              <div className="subtitle">
                Total Activity
                <div className="question-tooltip">
  								<Tooltip content="Quick View de los KPIs logrados en el tiempo analizado." position={Position.BOTTOM}>
  									<div><i className="fas fa-question"></i></div>
  								</Tooltip>
  							</div>
              </div>
              <div className="total-activity">

                <div className="flex-holder">

                  <div className="total-activity__item total-activity__item--impressions">
                    <div className="total-activity__icon">
                      <i className="far fa-eye"></i>
                    </div>
                    <div className="total-activity__text">
                      <H2 className="total-activity__text--data">
                        { numeral(this.props.total.impressions).format('0,0') }
                      </H2>
                      <span className="total-activity__text--name">Impresions</span>
                    </div>
                  </div>

                  <div className="total-activity__item total-activity__item--engagement">
                    <div className="total-activity__icon">
                      <i className="far fa-thumbs-up"></i>
                    </div>
                    <div className="total-activity__text">
                      <H2 className="total-activity__text--data">
                        { numeral( this.props.total.engagements).format('0,0') }
                      </H2>
                      <span className="total-activity__text--name">Engagements</span>
                    </div>
                  </div>

                  <div className="total-activity__item total-activity__item--posts">
                    <div className="total-activity__icon">
                      <i className="fas fa-edit"></i>
                    </div>
                    <div className="total-activity__text">
                      <H2 className="total-activity__text--data">
                        { numeral(this.props.total.posts).format('0,0') }
                      </H2>
                      <span className="total-activity__text--name">Posts</span>
                    </div>
                  </div>

                  <div className="total-activity__item total-activity__item--video">
                    <div className="total-activity__icon">
                      <i className="fas fa-play"></i>
                    </div>
                    <div className="total-activity__text">
                      <H2 className="total-activity__text--data">
                        { numeral(this.props.total.video_views).format('0,0') }
                      </H2>
                      <span className="total-activity__text--name">Video Views</span>
                    </div>
                  </div>

                </div>
              </div>
            </Card>
          );
      }
  }
}


function mapStateToProps( state )
{
  return {
		total: state.total.total,
		loading: state.total.loading
  }
}

export default connect(mapStateToProps, { fetchSocialTotalActivity })( TotalActivity );
