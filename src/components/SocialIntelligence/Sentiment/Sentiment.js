import React, { Component } from 'react';
import { Card, Elevation, Spinner, Tooltip, Position } from "@blueprintjs/core";
import { Bar } from 'react-chartjs-2';
import { connect } from 'react-redux';
import { fetchAudience } from '../../../actions';

class Sentiment extends Component {
  componentWillMount()
	{
		//this.props.fetchAudience( this.props.id );
	}

  constructor(props){
		super(props);
		this.state = {
			bestDaysData : {
				labels: [
					'positive',
					'neutral',
					'negative'
				],
				datasets: [{
					data: [2, 20, 80 ],
					backgroundColor: [
						'#CF3',
						'#5ebaeb',
						'#e2474f'
			    ]
				}],
			}

		}
	}

  render () {
    if( this.props.loading )
		{
			return( <Spinner /> );
		}

    let bestDaysData = {
      labels: [
        'positive',
        'neutral',
        'negative'
      ],
      datasets: [{
        data: this.props.audience.sentiment,
        backgroundColor: [
          '#CF3',
          '#5ebaeb',
          '#e2474f'
        ]
      }],
    }

    return(

          <Card interactive={true} elevation={Elevation.TWO} className="no-pad-t">
            <div className="subtitle">
              Sentiment

              <div className="question-tooltip question-tooltip__left">
								<Tooltip content="Análisis de comentarios en FB, reportado en contexto de español Mexicano." position={Position.BOTTOM}>
									<div><i className="fas fa-question"></i></div>
								</Tooltip>
							</div>

            </div>

            <Bar
              data={ bestDaysData }
              width={330}
              options={{
                legend: {
                  display: false,
                },
                scales: {
      						xAxes: [{
      							barPercentage: 1,
      							display: true,
      							gridLines: {
      								display: true,
      								color: "#636363",
      								drawTicks: false,
      							},
      							ticks: {
      								fontColor: "#fff",
      								padding: 14,
      							}
      						}],
      						yAxes: [{
      							display: true,
      							gridLines: {
      								display: true,
      								color: "#636363",
      								drawTicks: false,
      							},
      							ticks: {
      								fontColor: "#fff",
      								padding: 10,
      							}
      						}]
      					}
              }}
            />

          </Card>

    );
  }

}

function mapStateToProps( state )
{
  return {
		audience: state.audience.audience,
		loading: state.audience.loading
  }
}

export default connect(mapStateToProps, { fetchAudience })( Sentiment );
