import React, { Component } from 'react'
import { Card, Elevation, Spinner, Tooltip, Position } from '@blueprintjs/core'
import BestPost from './BestPost/BestPost'
import { connect } from 'react-redux'
import { fetchBestPosts } from '../../../actions'

import './styles.css'

class BestPosts extends Component {
    componentWillMount()
  	{
  		this.props.fetchBestPosts( this.props.id );
  	}

    state = {
        listado_bestPosts: []
    }

    render() {
      if( this.props.loading )
      {
        return( <Spinner /> );
      }

      let listado_bestPosts = this.props.bestposts.bestposts_organic;

        return (

          <Card interactive={true} elevation={Elevation.TWO} className="no-pad-t">
            <div className="subtitle">
              Best Posts

              <div className="question-tooltip">
								<Tooltip content="Los mejores posteos orgánicos de la semana." position={Position.BOTTOM}>
									<div><i className="fas fa-question"></i></div>
								</Tooltip>
							</div>

            </div>

            <div className="bestPost-shell">

                { listado_bestPosts.map(function(bestPost) {
                    return (
                        <BestPost key={bestPost.id} bestPost={bestPost }/>
                    );
                  })
                }

            </div>
          </Card>

        );
    }
}

function mapStateToProps( state )
{
  return {
		bestposts: state.bestposts.bestposts,
		loading: state.bestposts.loading
  }
}

export default connect(mapStateToProps, { fetchBestPosts })( BestPosts );
