import React, { Component } from 'react'
import { Card, Elevation, Spinner, Tooltip, Position } from "@blueprintjs/core"
import { Column, Table, AutoSizer } from 'react-virtualized'
import { Bar } from 'react-chartjs-2'
import { connect } from 'react-redux'
import { fetchSocialBestCats, fetchSocialBestCatsGraph } from '../../../actions'

class PerfectMix extends Component {

	constructor(props){
		super(props);
		this.state = {
			engagementData : {
				labels: [ ],
				datasets: [{
					data: [],
					backgroundColor: [],
				}],
			}
		}
	}

	componentWillMount()
	{
		//this.props.fetchSocialBestCats( this.props.id );
	}

	renderGraph()
	{
		if( this.props.loading_graph )
		{
			return( <Spinner /> )
		}

		this.tableData = this.props.perfect_mix.perfect_mix_graph_data.map(([categoryName, eScore, NoPosts]) => ({ categoryName, eScore, NoPosts}));

		let engagementData = {
			labels: this.props.perfect_mix.perfect_mix_labels_data,
			datasets: [{
				data: this.props.perfect_mix.perfect_mix_values_data,
				backgroundColor: [
					'#b3ff26',
					'#2aa9e9',
					'#ff9615',
					'#00cf56',
					'#c400ff',
					'#2aa9e9',
					'#ff9615',
					'#00cf56',
					'#c400ff'
				],
			}],
		}

		return(
			<Bar
				data={ engagementData }
				width={180}
				options={{
					legend: { display: false, },
					scales: {
						xAxes: [{
							barPercentage: 1,
							display: true,
							gridLines: {
								display: true,
								color: "#636363",
								drawTicks: false,
							},
							ticks: {
								fontColor: "#fff",
								padding: 14,
								callback: function(value) {
										return value.substr(0, 7) + "..."; //truncate
								}
							}
						}],
						yAxes: [{
							display: true,
							gridLines: {
								display: true,
								color: "#636363",
								drawTicks: false,
							},
							ticks: {
								fontColor: "#fff",
								padding: 10,
							}
						}]
					}
				}}
			/>);
	}

	fetchTable()
	{
		if( this.props.loading_graph )
		{
			return( <Spinner /> )
		}

		// console.log("Perfect Mix Table Data ------>", this.props.perfect_mix)
	}


  render () {

    return(

          <Card interactive={true} elevation={Elevation.TWO} className="no-pad-t">
            <div className="subtitle">
              Perfect Mix

							<div className="question-tooltip">
								<Tooltip content="Categorías organizadas por ER, reportando también el número de posts por categorías que se deben realizar al mes para obtener el mayor perfomance posible." position={Position.BOTTOM}>
									<div><i className="fas fa-question"></i></div>
								</Tooltip>
							</div>

            </div>

						<div className="chart-container">
								{ this.renderGraph() }
						</div>

						{this.fetchTable()}

          </Card>

    );
  }

}

function mapStateToProps( state )
{
  return {
		perfect_mix: state.graph.graph.perfect_mix,
		loading_graph: state.graph.loading
  }
}

export default connect(mapStateToProps, { fetchSocialBestCats })( PerfectMix );
