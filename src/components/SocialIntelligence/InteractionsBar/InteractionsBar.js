import React, { Component } from 'react'
import { Card, Elevation, Spinner, Tooltip, Position } from "@blueprintjs/core"
import { Column, Table, AutoSizer } from 'react-virtualized'
import { Bar } from 'react-chartjs-2'
import { connect } from 'react-redux'
import { fetchSocialTotalActivity } from '../../../actions'

class InteractionsBar extends Component {

	constructor(props){
		super(props);
		this.state = {
			bestDaysData : {
				labels: [],
				datasets: [{
					data: [],
					backgroundColor: []
				}],
			}
		}
	}

  render () {
		if( this.props.loading )
		{
			return(
							<Card interactive={true} elevation={Elevation.TWO} className="no-pad-t">
								<div className="subtitle">
									Interactions per Post

									<div className="question-tooltip">
										<Tooltip content="Comportamiento promedio por Post." position={Position.BOTTOM}>
											<div><i className="fas fa-question"></i></div>
										</Tooltip>
									</div>

								</div>
								<div className="chart-container">
									<div className="spinner-container">
										<Spinner />
									</div>
								</div>
						 	</Card>);
		}

		let graph_data =  this.props.total.table_data;
		let table_data =  this.props.total.table_interactions;
		this.tableData = this.props.total.table_interactions.map(([actionsPerPost, Avg]) => ({ actionsPerPost, Avg}));

		// console.log("Interactions Table data ----> ",this.props)


		let InteractionsData = {
				labels: [
					'Reactions',
					'Comments',
					'Shares'
				],
				datasets: [{
					data: graph_data,
					backgroundColor: [
						'#b3ff26',
						'#2aa9e9',
						'#ff9615'
			    ]
				}],
			};

    return(
              <Card interactive={true} elevation={Elevation.TWO} className="no-pad-t">
                <div className="subtitle">
                  Interactions per Post

									<div className="question-tooltip">
										<Tooltip content="Comportamiento promedio por Post." position={Position.BOTTOM}>
											<div><i className="fas fa-question"></i></div>
										</Tooltip>
									</div>

                </div>

                <div className="chart-container">

                  <Bar
                    data={ InteractionsData }
                    width={230}
                    options={{
                      legend: {
                        display: false,
                      },
											scales: {
												xAxes: [{
													barPercentage: 1,
													display: true,
													gridLines: {
														display: true,
														color: "#636363",
														drawTicks: false,
													},
													ticks: {
														fontColor: "#fff",
														padding: 14,
					            		}
												}],
												yAxes: [{
													display: true,
													gridLines: {
														display: true,
														color: "#636363",
														drawTicks: false,
													},
													ticks: {
														fontColor: "#fff",
														padding: 10,
													}
												}]
											}
                    }}
                  />

                </div>


								<AutoSizer disableHeight>
									{
										({ width }) => {

											return (
												<Table
													width={width}
													height={140}
													headerHeight={30}
													rowHeight={26}
													rowCount={table_data.length}
													rowGetter={({ index }) => table_data[index]}
												>
													<Column
														label='Actions per Post'
														dataKey='0'
														width={width / 2}
													/>
													<Column
														width={width / 2}
														label='Avg'
														dataKey='1'
													/>
												</Table>
											)
										}
									}
								</AutoSizer>
              </Card>
    );
  }

}

function mapStateToProps( state )
{
  return {
		total: state.total.total,
		loading: state.total.loading
  }
}

export default connect(mapStateToProps, { fetchSocialTotalActivity })( InteractionsBar );
