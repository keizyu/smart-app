import React, { Component } from 'react'
import { Card, Elevation, Tooltip, Position, Spinner } from "@blueprintjs/core"
import { Column, Table, AutoSizer } from 'react-virtualized'
import { Bubble } from 'react-chartjs-2'
import { connect } from 'react-redux'
import { fetchAudience } from '../../../actions'

import './styles.css'

// Table data as an array of objects
let list = [
  { title: 'Hour',
    dataCol0: '0',
    dataCol1: '1',
    dataCol2: '2',
    dataCol3: '3',
    dataCol4: '4',
    dataCol5: '5',
    dataCol6: '6',
    dataCol7: '7',
    dataCol8: '8',
    dataCol9: '9',
    dataCol10: '10',
    dataCol11: '11',
    dataCol12: '12',
    dataCol13: '13',
    dataCol14: '14',
    dataCol15: '15',
    dataCol16: '16',
    dataCol17: '17',
    dataCol18: '18',
    dataCol19: '19',
    dataCol20: '20',
    dataCol21: '21',
    dataCol22: '22',
    dataCol23: '23'
   },
	{ title: 'E Score', dataCol1: '8.08%', dataCol2: '8.08%', dataCol3: '8.08%', dataCol4: '8.08%', dataCol5: '8.08%',dataCol6: '8.08%',dataCol7: '8.08%',dataCol8: '8.08%', dataCol9: '8.08%' },
	{ title: 'Mo Posts', dataCol1: '6', dataCol2: '6', dataCol3: '6', dataCol4: '6', dataCol5: '6',dataCol6: '6',dataCol7: '6',dataCol8: '6', dataCol9: '6' },
];

class BestDaysBubble extends Component {

	constructor(props){
		super(props);
		this.state = {
			bestDaysData : {
				datasets: [{
					data: [],
					backgroundColor: '#CF3',
				}],
			}
		}
	}

  render () {
    if( this.props.loading )
		{
			return( <Card interactive={true} elevation={Elevation.TWO} className="no-pad-t"><Spinner /></Card> );
		}

    console.log( "==========================================================", "-- best hours" );
    console.log( this.props.audience );
    console.log( "==========================================================" );

    list[1] = this.props.audience.arr_metrics_hours_table
    list[2] = this.props.audience.arr_metrics_hours_no_table

    let bestDaysData = {
      datasets: [{
        data: this.props.audience.arr_metrics_hours_graph,
        backgroundColor: '#CF3',
      }],
    }

    return(

          <Card interactive={true} elevation={Elevation.TWO} className="no-pad-t">

						<div className="subtitle">
              Best Hours
              <div className="question-tooltip">
								<Tooltip content="Las mejores horas del día respecto al ER (engagement rate) reportado." position={Position.BOTTOM}>
									<div><i className="fas fa-question"></i></div>
								</Tooltip>
							</div>
            </div>

						<div className="chart-container">
							<Bubble
								width={330}
	              data={ bestDaysData }
	              options={{
                  layout: {
                    padding: {
                        left: 0,
                        right: 0,
                        top: 20,
                        bottom: 0
                    }
                	},
	                legend: {
	                  display: false,
	                },
                  scales: {
                    xAxes: [{
                      display: true,
                      gridLines: {
                        display: true,
                        color: "#636363",
                        drawTicks: false,
                      },
                      ticks: {
      									fontColor: "#fff",
      									padding: 14,
                  		}
                    }],
                    yAxes: [{
                      display: true,
                      gridLines: {
                        display: true,
                        color: "#636363",
                        drawTicks: false,
                      },
                      ticks: {
      									fontColor: "#fff",
      									padding: 10,
      								}
                    }]
                  }
	              }}
	            />
						</div>

						<AutoSizer disableHeight>
              	{
                	({ width }) => {

                  	return (
											<Table
										    width={width}
										    height={120}
										    headerHeight={20}
										    rowHeight={30}
										    rowCount={list.length}
										    rowGetter={({ index }) => list[index]}
                        className="table-bubble"
										  >
										    <Column
													className="table-bubble__label"
										      dataKey='title'
										      width={130}
										    />
										    <Column
										      width={100}
										      dataKey='dataCol0'
										    />
										    <Column
										      width={100}
										      dataKey='dataCol1'
										    />
												<Column
													width={100}
													dataKey='dataCol2'
												/>
												<Column
													width={100}
													dataKey='dataCol3'
												/>
												<Column
													width={100}
													dataKey='dataCol4'
												/>
												<Column
													width={100}
													dataKey='dataCol5'
												/>
												<Column
													width={100}
													dataKey='dataCol6'
												/>
												<Column
													width={100}
													dataKey='dataCol7'
												/>
												<Column
													width={100}
													dataKey='dataCol8'
												/>
												<Column
													width={100}
													dataKey='dataCol9'
												/>
												<Column
													width={100}
													dataKey='dataCol10'
												/>
												<Column
													width={100}
													dataKey='dataCol11'
												/>
												<Column
													width={100}
													dataKey='dataCol12'
												/>
												<Column
													width={100}
													dataKey='dataCol13'
												/>
												<Column
													width={100}
													dataKey='dataCol14'
												/>
												<Column
													width={100}
													dataKey='dataCol15'
												/>
												<Column
													width={100}
													dataKey='dataCol16'
												/>
												<Column
													width={100}
													dataKey='dataCol17'
												/>
												<Column
													width={100}
													dataKey='dataCol18'
												/>
												<Column
													width={100}
													dataKey='dataCol19'
												/>
												<Column
													width={100}
													dataKey='dataCol20'
												/>
												<Column
													width={100}
													dataKey='dataCol21'
												/>
												<Column
													width={100}
													dataKey='dataCol22'
												/>
												<Column
													width={100}
													dataKey='dataCol23'
												/>
										  </Table>
										)
                	}
              	}
              	</AutoSizer>

          </Card>

    );
  }
}

function mapStateToProps( state )
{
  return {
		audience: state.audience.audience,
		loading: state.audience.loading
  }
}

export default connect(mapStateToProps, { fetchAudience })( BestDaysBubble );
