import React, { Component } from 'react'
import { Card, Elevation, Spinner, Tooltip, Position } from "@blueprintjs/core"
import { Column, Table, AutoSizer } from 'react-virtualized'
import { connect } from 'react-redux'
import { fetchBestHashtags } from '../../../actions'
import _ from 'lodash'

import './styles.css'

class BestHashtags extends Component {

	componentWillMount()
	{
		//this.props.fetchBestHashtags( this.props.id );
	}

	constructor(props){
		super(props);
		this.state = {

		}
	}

  render () {
		if( this.props.loading )
		{
				return(
						<div className="spinner-container">
							<Spinner />
						</div>
				)
		}

		let table_data = this.props.hashtags;

		if( table_data.length > 5 )
		{
			table_data = _.chunk( this.props.hashtags, 5)[0];
		}

		// console.log( "table_data", table_data );

		//table_data = this.props.hashtags;
		this.tableData = table_data.map(([categoryName, eScore, MoPosts]) => ({ categoryName, eScore, MoPosts}));

    return(

          <Card interactive={true} elevation={Elevation.TWO} className="no-pad-t">
            <div className="subtitle">
              Best Hashtags

							<div className="question-tooltip">
								<Tooltip content="Los mejores Hashtags en el tiempo analizado." position={Position.BOTTOM}>
									<div><i className="fas fa-question"></i></div>
								</Tooltip>
							</div>


            </div>

						<div className="chart-container hastag-chart">
								<div className="hastag-chart__item">
									<span className="hastag-chart__item--txt">
										{ table_data.length > 0 ? this.props.hashtags[0][0] : '' }
									</span>
								</div>
								<div className="hastag-chart__item">
									<span className="hastag-chart__item--txt">
										{ table_data.length > 1 ? this.props.hashtags[1][0] : '' }
									</span>
								</div>
								<div className="hastag-chart__item">
									<span className="hastag-chart__item--txt">
										{ table_data.length > 2 ? this.props.hashtags[2][0] : '' }
									</span>
								</div>
								<div className="hastag-chart__item">
									<span className="hastag-chart__item--txt">
										{ table_data.length > 3 ? this.props.hashtags[3][0] : '' }
									</span>
								</div>
								<div className="hastag-chart__item">
									<span className="hastag-chart__item--txt">
										{ table_data.length > 4 ? this.props.hashtags[4][0] : '' }
									</span>
								</div>
						</div>


						<AutoSizer disableHeight>
							{
								({ width }) => {

									return (
										<Table
											width={width}
											height={160}
											headerHeight={30}
											rowHeight={26}
											rowCount={table_data.length}
											rowGetter={({ index }) => table_data[index]}
										>
											<Column
												label='Hashtag'
												dataKey='0'
												width={width / 2}
											/>
											<Column
												width={width / 2}
												label='E Score'
												dataKey='1'
											/>
										</Table>
									)
								}
							}
						</AutoSizer>
          </Card>

    );
  }

}

function mapStateToProps( state )
{
  return {
		hashtags: state.hashtags.hashtags,
		loading: state.hashtags.loading
  }
}

export default connect(mapStateToProps, { fetchBestHashtags })( BestHashtags );
