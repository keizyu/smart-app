import React, { Component } from 'react'
import { Card, Elevation, Spinner, Tooltip, Position } from "@blueprintjs/core"
import { Bar } from 'react-chartjs-2'
import { connect } from 'react-redux'
import { fetchAudience } from '../../../actions'

import './styles.css'

class Audience extends Component {
	componentWillMount()
	{
		// console.log( "audience  will mount --> ", this.props.id );
		//this.props.fetchAudience( this.props.id );
	}

  render () {
		if( this.props.loading )
		{
			return( <Spinner /> );
		}

		let genderWidth = {
			maleWidth:{
				width: '0%'
			},
			femaleWidth:{
				width: '0%',
			}
		}

		// console.log( "audience ++++---> ", this.props.audience );
		genderWidth.maleWidth.width = `${this.props.audience.gender_male}%`;
		let female = 100 - this.props.audience.gender_male;
		genderWidth.femaleWidth.width = `${female}%`;

		// console.log( "Audience ++++---> ", this.props.audience );

		let bestDaysData = {
			labels: [
				'18-24',
				'25-34',
				'35-44'
			],
			datasets: [{
				data: this.props.audience.fans_gender_age,
				backgroundColor: [
					'#b3ff26',
					'#2aa9e9',
					'#ff9615'
				]
			}],
		}

    return(
          <Card id="audienceTarget" interactive={true} elevation={Elevation.TWO} className="no-pad-t">
            <div className="subtitle">
              Audience

							<div className="question-tooltip question-tooltip__left">
								<Tooltip content="Reporte de Edades y Sexo de audiencia." position={Position.LEFT}>
									<div><i className="fas fa-question"></i></div>
								</Tooltip>
							</div>

            </div>

						<div className="subtitle subtitle__no-border">
              Gender
            </div>

						<div className="chart-container gender">
								<div className="gender__item gender__item-female" style={ genderWidth.femaleWidth }>
									<div className="flex-holder">
										<div className="gender__item--icon">
											<i className="fas fa-female fa-4x"></i>
										</div>

										<span className="gender__item--data">
											{ genderWidth.femaleWidth.width }
										</span>
								</div>
								</div>


								<div className="gender__item gender__item-male"  style={ genderWidth.maleWidth }>
									<div className="flex-holder">
										<div className="gender__item--icon">
											<i className="fas fa-male fa-4x"></i>
										</div>
										<span className="gender__item--data">
											{ genderWidth.maleWidth.width }
										</span>
									</div>
								</div>

						</div>

						<div className="subtitle subtitle__no-border">
              Ages
            </div>

						<Bar
							data={ bestDaysData }
							width={330}
							options={{
								legend: {
									display: false,
								},
								scales: {
									xAxes: [{
										barPercentage: 1,
										display: true,
										gridLines: {
											display: true,
											color: "#636363",
											drawTicks: false,
										},
										ticks: {
											fontColor: "#fff",
											padding: 14,
										}
									}],
									yAxes: [{
										display: true,
										gridLines: {
											display: true,
											color: "#636363",
											drawTicks: false,
										},
										ticks: {
											fontColor: "#fff",
											padding: 10,
										}
									}]
								}
							}}
						/>


          </Card>

    );
  }

}

function mapStateToProps( state )
{
  return {
		audience: state.audience.audience,
		loading: state.audience.loading
  }
}

export default connect(mapStateToProps, { fetchAudience })( Audience );
