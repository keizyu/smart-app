import React, { Component } from 'react';
import { Card, Elevation, Tooltip, Position } from "@blueprintjs/core";
import { Column, Table, AutoSizer } from 'react-virtualized';
import { Bar } from 'react-chartjs-2';
import { connect } from 'react-redux';
import { fetchSocialBestCats, fetchSocialBestCatsGraph } from '../../../actions';
import { Spinner } from '@blueprintjs/core';
import numeral from 'numeral'

import './styles.css'

class Engagement extends Component {

	constructor(props){
		super(props);
		this.state = {
			engagementData : {
				labels: [],
				datasets: [{
					data: [],
					backgroundColor: [],
				}],
			}
		}
	}

	componentWillMount()
  {
		//this.props.fetchSocialBestCatsGraph( this.props.id );
  }

	renderEngage()
	{
		if( this.props.loading_graph )
		{
			return( <Spinner /> )
		}
		else
		{
			console.log("engagement --->");
			console.log( this.props.graph );

			return(
				<div>
					<div className="engagement-main engagement-score">

						<h3 className="engagement-main__title">ENG Score</h3>
						<div className="engagement-main__data">
							{ numeral(this.props.graph.totals.es_calculated).format('0,0.0000') }
						</div>
					</div>

					<div className="engagement-main engagement-competition">
						<h3 className="engagement-main__title">ENG Competition</h3>
						<div className="engagement-main__data">
							{ numeral(this.props.graph.totals.es_competition).format('0,0.0000') }
						</div>
					</div>
				</div>
			)
		}
	}

	renderTable()
	{
		if( this.props.loading_graph )
		{
			return( <Spinner /> )
		}
		else
		{
      let table_data = this.props.graph.data;
			// console.log("Engagement table data ----> ",this.props.graph.data)

			return (
				<AutoSizer disableHeight>
					{
						({ width }) => {

							return (
								<Table
							    width={width}
							    height={140}
							    headerHeight={30}
							    rowHeight={26}
							    rowCount={table_data.length}
							    rowGetter={({ index }) => table_data[index]}
							  >
							    <Column
							      label='Category'
							      dataKey='0'
							      width={width / 2}
							    />
							    <Column
							      width={width / 2}
							      label='E Score'
							      dataKey='1'
							    />
							  </Table>
							)
						}
					}
				</AutoSizer>
			)
		}
	}

	renderGraph()


	{
		if( this.props.loading_graph )
		{
			return( <Spinner /> )
		}
		else
		{
				// let renderTableData = this.props.graph.graph_data;

				let engagementData={
					labels: this.props.graph.labels,
					datasets: [{
						data: this.props.graph.graph_data,
						backgroundColor: this.props.graph.backgroundColor,
					}],
				}


				// console.log("data: ------>", this.props.graph.graph_data)

				return(
				<Bar
					data={ engagementData }
					width={230}
					options={{
						layout: {
							padding: {
									left: 0,
									right: 20,
									top: 0,
									bottom: 0
							}
						},
						legend: {
							display: false,
						},
						scales: {
							xAxes: [{
								barPercentage: 1,
								display: true,
								gridLines: {
									display: true,
									color: "#636363",
									drawTicks: false,
								},
								ticks: {
									fontColor: "#fff",
									padding: 14,
	                callback: function(value) {
	                    return value.substr(0, 7) + "..."; //truncate
	               	}
            		}
							}],
							yAxes: [{
								display: true,
								gridLines: {
									display: true,
									color: "#636363",
									drawTicks: false,
								},
								ticks: {
									fontColor: "#fff",
									padding: 10,
								}
							}]
						}
					}}
				/>
			)
		}
	}

	renderComponent()
	{
		if( this.props.loading_graph )
		{
			return( <Card interactive={true} elevation={Elevation.TWO} className="no-pad-t"><Spinner /></Card> )
		}
		else
		{
		return(

			<Card interactive={true} elevation={Elevation.TWO} className="no-pad-t">
				<div className="subtitle">
					Engagement

					<div className="question-tooltip">
						<Tooltip content="Organiza las categorías de acuerdo a su ER." position={Position.BOTTOM}>
							<div><i className="fas fa-question"></i></div>
						</Tooltip>
					</div>

				</div>

				<div className="engagement">
					{ this.renderEngage() }
				</div>
				<div className="chart-container">
					{ this.renderGraph() }
				</div>
				{ this.renderTable() }
			</Card>

			);
		}
	}

  render () {

    return(
        <div>{ this.renderComponent() }</div>
    );
  }


}

function mapStateToProps( state )
{
  return {
		graph: state.graph.graph,
		loading_graph: state.graph.loading,
  }
}

export default connect(mapStateToProps, { fetchSocialBestCats, fetchSocialBestCatsGraph })( Engagement );
