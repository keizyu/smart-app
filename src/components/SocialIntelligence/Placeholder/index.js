import React, { Component } from 'react'

class Placeholder extends Component {
  render () {
    return (
      <div>
        <h1>NO DATA</h1>
      </div>
    )
  }
}

export default Placeholder;
