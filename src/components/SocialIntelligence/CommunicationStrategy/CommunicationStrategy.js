import React, { Component } from 'react'
import { Card, Elevation, Position, Tooltip  } from "@blueprintjs/core"
import { connect } from 'react-redux'
import { fetchSocialBestCats } from '../../../actions'
import { Spinner } from '@blueprintjs/core'
import Post from './Post'
import Cookies from 'universal-cookie'
import logoSmartCom from '../../../assets/img/loginsmart.png'
import numeral from 'numeral'
import Swiper from 'react-id-swiper'
import _ from 'lodash'

import './styles.css'

class CommunicationStrategy extends Component {
  constructor(props) {
      super(props)
      this.state = {
          selectedType: 'all',
          selectedPost: 0,
          selectedCat: 0,
      }

      this.printDiv = this.printDiv.bind(this);
  }

  componentWillMount()
  {
    //this.props.fetchSocialBestCats( this.props.id, this.props.range );
    //console.log( "----> ", this.props.id, this.props.range, this.props.post_filter );
    //this.props.fetchSocialBestCats( this.props.id, this.props.range, this.props.post_filter );
  }

  renderInteractions()
  {
    if( this.props.loading )
    {
      return(
        <div><Spinner /></div>
      )
    }
    else
    {
      return(
        <div>
            { numeral(this.props.cats[ this.state.selectedCat ].interactions).format('0,0') }
        </div>
      )
    }
  }

  numberFormat(num, sign = '') {
    const pieces = parseFloat(num).toFixed(2).split('')
    let ii = pieces.length - 3
    while ((ii-=3) > 0) {
      pieces.splice(ii, 0, ',')
    }
    return (sign + pieces.join('')).split(".")[0]
}

  renderImpressions()
  {
    if( this.props.loading )
    {
      return(
        <div><Spinner /></div>
      )
    }
    else
    {
      return(
        <div> { this.numberFormat( this.props.cats[ this.state.selectedCat ].impressions ) } </div>
      )
    }
  }

  renderSelectedPost()
  {
    if( this.props.loading )
    {
      return( <div><Spinner /></div> )
    }
    else
    {
      let CurrentPost = this.props.cats[ this.state.selectedCat ].posts[ this.state.selectedPost ]
      // console.log( "CurrentPost ------------->", CurrentPost );

      return( <Post post={CurrentPost} /> )
    }
  }

  renderSlider(){
    const swiperParams = {
      rebuildOnUpdate: true,
      pagination: {
        el: '.swiper-pagination',
        type: 'fraction',
        clickable: true
      },
      navigation: {
        nextEl: '.swiper-button-next.fas.fa-caret-right',
        prevEl: '.swiper-button-prev.fas.fa-caret-left'
      },
      spaceBetween: 0
    }

    if(this.props.loading){
      return( <div><Spinner /></div> )
    }else{
      const posts = this.props.cats[this.state.selectedCat].posts
      const sliderItems = posts.map((post, index)=>{
          return (
            <div key={index} className="postsMap">
                <Post post={post} />
            </div>
          )
      })

      return(
        <div className="postsSlider">
          <Swiper {...swiperParams}>
            {sliderItems}
          </Swiper>
       </div>
      )
    }
  }


  renderPostNumer()
  {
    if( this.props.loading )
    {
      return(
        <div><Spinner /></div>
      )
    }
    else
    {
      return(
        <div> { this.numberFormat( this.props.cats[ this.state.selectedCat ].total_posts ) } </div>
      )
    }
  }

  renderBest(categories, index){
    console.log("categories->");
    console.log(categories);

    const { selectedCat } = this.state

    const views = categories.map((category, _index)=>{
      const position = (4 * index) + _index
      return (
        <div key={_index} className={selectedCat === position ? "category-rank__item category-rank__item-active" : "category-rank__item" }>
          {this.printDiv(category, position)}
        </div>
      )
    })
    const placeholder = _.range(4 - categories.length).map((element, i)=>{

      return (
        <div key={ "i" + i } className="category-rank__item">
          <img src={logoSmartCom} alt="Logo Smart" className="logo-smart__com" />
        </div>
      )
    })

    return _.concat(views, placeholder)
  }

  printDiv({categoria, es_calculated, total_posts}, index){
    return(
      <div
        className="category-rank__container"
        onClick={()=>{this.setState({ selectedCat: index })}}>
        <p className="category-rank__item--title">#{categoria}</p>
        <p className="category-rank__item--data">
          {es_calculated === 0 ? "No data" : Math.round((es_calculated) * 100) / 100}%
        </p>
        <p className="category-rank__item--rank">#{index+1}</p>
      </div>
    )
  }


  render () {
    let group = _.chunk(this.props.cats, 4)

    const swiperBestParams = {
      rebuildOnUpdate: true,
      direction: 'vertical',
      autoHeight: true,
      height: 400,
      pagination: false,
      navigation: {
        nextEl: '.swiper-button-next.fas.fa-caret-down',
        prevEl: '.swiper-button-prev.fas.fa-caret-up'
      },
      spaceBetween: 0
    }

    return(



        <div className="strategy">

            <Card interactive={true} elevation={Elevation.TWO} className="no-pad-t">

                <div className="grid">
                  <div className="col-12">
                    <div className="subtitle">
                      Communication Strategy

                      <div className="question-tooltip">
                        <Tooltip content="Categorías organizadas de mejor a peor Performance Comparativa de los mejores posts por categoría y saber el Engagement Rate de cada Categoria." position={Position.BOTTOM}>
                          <div><i className="fas fa-question"></i></div>
                        </Tooltip>
                      </div>

                    </div>
                  </div>
                  <div className="col-4_sm-12 renderBest">
                    <Swiper {...swiperBestParams}>
                      {
                        group.map((categories, index)=>{
                          return (
                            <div key={index} className="category-rank">
                              <div className="swiperContainer">
                                {this.renderBest(categories, index)}
                              </div>
                            </div>
                          )
                        })
                      }
                    </Swiper>
                  </div>

                  <div className="col-4_sm-12">
                    <div className="category-data">

                      <div className="category-data__item">
                        <div className="category-data__item--data">
                          { this.renderPostNumer() }
                        </div>
                        <div className="category-data__item--title">Posts</div>
                      </div>

                      <div className="category-data__item">
                        <div className="category-data__item--data">
                          { this.renderInteractions() }
                        </div>
                        <div className="category-data__item--title">Interactions</div>
                      </div>

                      <div className="category-data__item">
                        <div className="category-data__item--data">
                          { this.renderImpressions() }
                        </div>
                        <div className="category-data__item--title">Impressions</div>
                      </div>



                    </div>
                  </div>

                  <div className="col-4_sm-12">
                    <div className="category-post">
                      { this.renderSlider() }
                    </div>
                  </div>
                </div>

            </Card>

        </div>

    );
  }
}

function mapStateToProps( state )
{
  return {
    cats: state.cats.posts,
    loading: state.cats.loading
  }
}

export default connect(mapStateToProps, { fetchSocialBestCats })( CommunicationStrategy );
