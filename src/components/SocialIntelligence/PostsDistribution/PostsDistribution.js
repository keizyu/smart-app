import React, { Component } from 'react';

import { Card, Elevation, Tooltip, Position } from "@blueprintjs/core";
import {Doughnut} from 'react-chartjs-2';
import { connect } from 'react-redux';
import { fetchSocialPostDistribution } from '../../../actions';
import { Spinner } from '@blueprintjs/core';

class PostsDistribution extends Component {

	constructor(props){
		super(props);
		this.state = {
			postsDistributionData: {
				labels: [
					'Mon',
					'Tue',
					'Wed',
			    'Thu',
			    'Fri',
			    'Sat',
			    'Sun'
				],
				datasets: [{
			    label: 'Population',
					data: [1, 2, 3, 4, 5, 6, 7],
					backgroundColor: [
						'#b3ff26',
						'#2aa9e9',
						'#ff9615',
			  		'#00cf56',
			      '#c400ff',
			      '#f7223e',
			      '#2aa9e9'
			    ],
			    borderColor: '#363737',
					borderWidth: 0
				}],
			}
		}
	}

	componentWillMount()
  {
		//this.props.fetchSocialPostDistribution( this.props.id );
  }

	renderDonut()
	{
		//console.log( "Cargando dona..." );

		if( this.props.loading )
		{
			return( <Spinner /> )
		}
		else
		{
			//console.log( "···· ZZZ", this.props.days );
			//console.log( "···· labels", this.props.days.labels );
			//console.log( "···· values", this.props.days.values );
			//console.log( "values", _values )

			let _values = this.props.days.values;
			let Data = this.state.postsDistributionData;
			Data.datasets[0].data = _values;

			return(
					<div>
						<div className="subtitle">
							Posts Distribution
							<div className="question-tooltip">
								<Tooltip content="El número total de Posts realizados, distribuidos por dias de la semana." position={Position.BOTTOM}>
									<div><i className="fas fa-question"></i></div>
								</Tooltip>
							</div>
						</div>
						<Doughnut
							data={ Data }
							width={130}
							options={{
								legend: {
									display: false,
								}
							}}
						/>
					</div>
				);
		}
	}

  render () {


    return(

          <Card interactive={true} elevation={Elevation.TWO} className="no-pad-t">
            { this.renderDonut() }
          </Card>

    );
  }
}

function mapStateToProps( state )
{
  return {
		days: state.days.days,
		loading: state.loading
  }
}

export default connect(mapStateToProps, { fetchSocialPostDistribution })( PostsDistribution );
