import React, { Component } from 'react';
import logoSmart from '../../../../assets/img/logo.png';
import { Card, Elevation } from "@blueprintjs/core";
import Truncate from './Truncate'
import moment from 'moment'

class BestPaidPost extends Component {
  state = {
    bestPaidPost: null,
    items: []
  };

  render (props) {

    return(

        <Card interactive={true} elevation={Elevation.TWO} className="bestpostContainer">
          <div className="bestPost">

            <div className="post-topContainer">
              <div className="postImage bestpostImage">
                 <img alt={this.props.bestPaidPost.postImage.img}  src={ this.props.bestPaidPost.postImage.url } />
              </div>
            </div>

            <div className="postMessage">

              <Truncate text={this.props.bestPaidPost.postMessage.message} />

            </div>

            <div className="postDate">
              <p> {moment(this.props.bestPaidPost.postDate).format("ll")} </p>
              <img src={logoSmart} alt="Logo Smart" className="logoSmartPost" />
            </div>
          </div>
        </Card>
    );
  }

}

export default BestPaidPost;
