import React, { Component } from 'react'
import { Card, Elevation, Spinner, Tooltip, Position } from '@blueprintjs/core'
import BestPaidPost from './BestPaidPost/BestPaidPost'
import { connect } from 'react-redux'
import { fetchBestPosts } from '../../../actions'

import './styles.css'

class BestPaidPosts extends Component {

    constructor(props){
      super(props)

      this.state = {
          listado_bestPaidPosts: []
      }
    }

    componentWillMount()
    {
      this.props.fetchBestPosts( this.props.id );
    }

    render() {
      if( this.props.loading )
      {
        return( <Spinner /> );
      }

      let listado_bestPaidPosts = this.props.bestposts.bestposts_paid;

        return (

          <Card interactive={true} elevation={Elevation.TWO} className="no-pad-t">
            <div className="subtitle">
              Best Paid Posts

              <div className="question-tooltip">
                <Tooltip content="Los mejores posteos pagados de la semana." position={Position.BOTTOM}>
                  <div><i className="fas fa-question"></i></div>
                </Tooltip>
              </div>

            </div>

            <div className="grid bestPaidPost-shell">


                { listado_bestPaidPosts.map(function(bestPaidPost) {
                    return (
                        <BestPaidPost key={bestPaidPost.id} bestPaidPost={bestPaidPost }/>
                    );
                  })
                }


            </div>

          </Card>

        );
    }
}

function mapStateToProps( state )
{
  return {
		bestposts: state.bestposts.bestposts,
		loading: state.bestposts.loading
  }
}

export default connect(mapStateToProps, { fetchBestPosts })( BestPaidPosts );
