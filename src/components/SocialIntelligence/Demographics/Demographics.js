import React, { Component } from 'react'
import { Card, Elevation, Spinner, Tooltip, Position } from "@blueprintjs/core"
import { Column, Table, AutoSizer } from 'react-virtualized'
import { connect } from 'react-redux'
import { fetchAudience } from '../../../actions'
import numeral from 'numeral'

import './styles.css'

let list = [];


class Demographics extends Component {

  constructor(props) {
      super(props);
      this.state = {}

  }

  componentWillMount()
	{
		//this.props.fetchAudience( this.props.id );
	}

  renderPaises(){

      // console.log("this.props.audience.page_fans_country ---->", this.props.audience.page_fans_country)

      return (
        <div className="paises">
         { this.props.audience.page_fans_country.map(function(pais){

             return (
               <div key={pais.country} className="paisesMap">
                 <img src={require('../../../assets/img/flags/'+ pais.country.toLowerCase() +'.png')} alt={pais.country}/>
               </div>

             );
         })}
       </div>
      )
  }

  render () {
    if( this.props.loading )
		{
			return( <Spinner /> );
		}

    list = this.props.audience.fb_demo_city;

    // console.log("Demographics props --------->", list)

    return(

          <Card interactive={true} elevation={Elevation.TWO} className="no-pad-t">
            <div className="subtitle">
              Demographics

              <div className="question-tooltip question-tooltip__left">
								<Tooltip content="Distribución Países y ciudades de la audiencia de la marca." position={Position.BOTTOM}>
									<div><i className="fas fa-question"></i></div>
								</Tooltip>
							</div>

            </div>

            <div className="subtitle subtitle__no-border followers">
              Followers <br /><br />
              <div className="followers-data">
                { numeral(this.props.audience.fan_count).format('0,0') }
              </div>
            </div>

            { this.renderPaises() }

            <AutoSizer disableHeight>
            	{
              	({ width }) => {

                	return (

                    <Table
                      width={width}
                      height={164}
                      headerHeight={26}
                      rowHeight={26}
                      rowCount={list.length}
                      rowGetter={({ index }) => list[index]}
                    >
                      <Column
                        label='City'
                        dataKey='city'
                        width={300}
                      />
                      <Column
                        width={150}
                        label='Fans'
                        dataKey='fans'
                      />
                    </Table>

									)
              	}
            	}
            </AutoSizer>
          </Card>
    );
  }

}

function mapStateToProps( state )
{
  return {
		audience: state.audience.audience,
		loading: state.audience.loading
  }
}

export default connect(mapStateToProps, { fetchAudience })( Demographics );
