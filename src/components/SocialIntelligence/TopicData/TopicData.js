import React, {Component} from 'react'
import _ from 'lodash'
import { Button, Card, Classes, Elevation, Dialog, Tooltip, Position } from '@blueprintjs/core'
import TopicDataItem from './TopicDataItem/TopicDataItem'
import {Bar} from 'react-chartjs-2'

import './styles.css'

class TopicData extends Component {

  constructor(props) {
    super(props)
    this.state = {
      listado_topicData: [],
      isOpen: false,
      hide: false,
      graphData: {
        labels: [],
        datasets: [
          {
            data: [],
            backgroundColor: ['#b3ff26', '#2aa9e9', '#ff9615']
          }
        ]
      }
    }

    this.closeAddTopicDataItem = this.closeAddTopicDataItem.bind(this)
    this.openAddTopicDataItem = this.openAddTopicDataItem.bind(this)
    this.handleAddTopicDataItem = this.handleAddTopicDataItem.bind(this)

  }

  handleAddTopicDataItem(event) {
    const {graphData} = this.state
    const {labels, datasets} = graphData
    const {value} = this._inputElement
    let newGraphData = {}

    if (value !== "") {
      // create a clone of your array of players don't modify objects on the state directly
      const addTopicDataItem = this.state.listado_topicData.slice(0)
      const graphLabels = this.state.graphData.labels

      addTopicDataItem.push({id: Date.now(), wordTitle: this._inputElement.value, count: '0'})

      newGraphData = {
        labels: _.concat(labels, [value]),
        datasets: [
          {
            data: _.concat(datasets[0].data, [_.random(100, 1000)]),
            backgroundColor: _.concat(datasets[0].backgroundColor, '#ffffff')
          }
        ]
      }

      this.setState({listado_topicData: addTopicDataItem, graphData: newGraphData, isOpen: false})

      event.preventDefault()
    } else {
      this.setState({hide: true})
    }
  }

  openAddTopicDataItem() {
    this.setState({isOpen: true})
  }

  closeAddTopicDataItem() {
    this.setState({isOpen: false})
  }

  renderAgain() {
    return <Bar data={this.state.graphData} width={530} options={{
        legend: {
          display: false
        },
        scales: {
          xAxes: [
            {
              barPercentage: 1,
              display: true,
              gridLines: {
                display: true,
                color: "#636363",
                drawTicks: false
              },
              ticks: {
                fontColor: "#fff",
                padding: 14
              }
            }
          ],
          yAxes: [
            {
              display: true,
              gridLines: {
                display: true,
                color: "#636363",
                drawTicks: false
              },
              ticks: {
                fontColor: "#fff",
                padding: 10,
                beginAtZero:true
              }
            }
          ]
        }
      }}/>
  }

  render() {
    const {listado_topicData} = this.state
    return (<div>
      <Card interactive={true} elevation={Elevation.TWO} className="no-pad-t">
        <div className="subtitle">
          Topic Data

          <div className="question-tooltip">
            <Tooltip content="Análisis de palabras claves dentro organizada por categorías, de los comentarios en FB de la marca." position={Position.BOTTOM}>
              <div>
                <i className="fas fa-question"></i>
              </div>
            </Tooltip>
          </div>

        </div>

        {this.renderAgain()}

        <div className="grid topicData-shell">
          {
            listado_topicData.map(function(topicData) {
              return (<div className="col-3" key={topicData.id}>
                <TopicDataItem topicData={topicData}/>
              </div>)
            })
          }
          <div className="col-3" onClick={this.openAddTopicDataItem}>
            <div className="topicData-item topicData-item__last grid-center-middle">
              <div className="col-12">
                <i className="fas fa-plus topicData-item__addIcon"></i>
              </div>
            </div>
          </div>
        </div>
      </Card>

      <Dialog onClose={this.closeAddTopicDataItem} isOpen={this.state.isOpen} className="bp3-dark input-modal">
        <div className={Classes.DIALOG_BODY}>

          <form onSubmit={this.handleAddTopicDataItem}>
            <div className="bp3-input-group">
              <label className="bp3-label">Input Category Name</label>
              <div className={this.state.hide === true
                  ? 'dialog-add__error'
                  : 'dialog-add__error hidden'}>Enter category name</div>
              <div className="grid-center-middle">
                <div className="col-10">
                  <input className="bp3-input topic-item__input" ref={(a) => this._inputElement = a} placeholder="Category"></input>
                </div>
                <div className="col-2">
                  <Button minimal="true" large="true" className="button button-primary" onClick={this.handleAddTopicDataItem}>Add</Button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </Dialog>
    </div>)
  }
}

export default TopicData
