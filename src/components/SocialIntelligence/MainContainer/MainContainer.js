import React, { Component } from 'react'
import { Button, H1 } from "@blueprintjs/core"
import { DateRangeInput } from "@blueprintjs/datetime"


// COMPONENTES
import CommunicationStrategy from '../CommunicationStrategy/CommunicationStrategy'
import Engagement from '../Engagement/Engagement'
import PostsDistribution from '../PostsDistribution/PostsDistribution'
import TotalActivity from '../TotalActivity/TotalActivity'
import BestDaysBar from '../BestDaysBar/BestDaysBar'
import BestHoursBubble from '../BestHoursBubble/BestHoursBubble'
import InteractionsBar from '../InteractionsBar/InteractionsBar'
import PerfectMix from '../PerfectMix/PerfectMix'
import BestHashtags from '../BestHashtags/BestHashtags'
import Audience from '../Audience/Audience'
import Demographics from '../Demographics/Demographics'
import Reactions from '../Reactions/Reactions'
import Sentiment from '../Sentiment/Sentiment'
import TopicData from '../TopicData/TopicData'
import BestPosts from '../BestPosts/BestPosts'
import BestPaidPosts from '../BestPaidPosts/BestPaidPosts'
import Placeholder from '../Placeholder'


import Cookies from 'universal-cookie'
import moment from 'moment'
import { connect } from 'react-redux'
import { fetchPosts, fetchSocialBestCats, fetchSocialBestCatsGraph, changeFilter, changeOrder,
          fetchSocialPostDistribution, fetchSocialBestDays, fetchSocialTotalActivity,
          fetchBestHashtags, fetchAudience } from '../../../actions'

import './styles.css'

class MainContainer extends Component {

  constructor(props) {
    super(props);

    // se obtiene la fecha de la cookie
    const cookies = new Cookies();
    let range = cookies.get('range')

    if( !range ) {
      range = [ new Date(moment().subtract(15, "days").format('YYYY-MM-DD')), new Date(moment().format('YYYY-MM-DD'))]
    } else {
      range = [ new Date(range[0]), new Date(range[1])];
    }

    cookies.set('range', range, { path: '/' });

    this.state = {
        range: range,
    };

    this.post_filter="";

  }

  componentWillMount() {
    this.post_filter = this.props.post_filter;

    this.props.fetchSocialBestCats( this.props.id, this.state.range, this.props.post_filter );
    this.props.fetchSocialBestCatsGraph( this.props.id, this.state.range, this.props.post_filter );
    this.props.fetchSocialPostDistribution( this.props.id, this.state.range, this.props.post_filter );
    this.props.fetchSocialTotalActivity( this.props.id, this.state.range, this.props.post_filter );
    this.props.fetchSocialBestDays( this.props.id, this.state.range, this.props.post_filter );
    this.props.fetchAudience( this.props.id, this.state.range, this.props.post_filter );

    //this.props.fetchBestHashtags( this.props.id, this.state.range, this.props.post_filter );
    //
  }

  render () {

    const AreData = this.props.cats && this.props.cats.length > 0 && this.props.cats[0].total_posts > 0;

    return(

      <div>
      {AreData ? (
        <div className="main-container__dashboard">

          <div className="top-container">
            <div className="brands-added">

              <div className="grid">

                <div className="col-5_sm-12">
                  <H1 className="brands-added__title">Social Intelligence ( {this.props.post_filter} )</H1>
                </div>

                <div className="col-7_sm-12">

                  <div className="searchContainer">
                    <div className="dateSmart bp3-dark">
                      <span className="searchCalendarIcon"><i className="far fa-calendar-alt"></i></span>
                      <DateRangeInput
                          formatDate={date => moment(date).format("YYYY-MM-DD")}
                          onChange={this.handleRangeChange}
                          parseDate={str => new Date(str)}
                          value={this.state.range}
                          placeholder={"YYYY-mm-dd"}
                      />
                      <Button onClick={this.openSearchCalendar} minimal="true" className="button btn-outline" >Search</Button>
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>

          <div className="social-intelligence__container">


            <div className="grid">

              <div className="col-12">
                <CommunicationStrategy id={ this.props.id } range={this.state.range} post_filter={this.props.post_filter} post_order={this.props.post_order} />
              </div>

              <div className="col-4_sm-12">
                <Engagement id={this.props.id}  range={this.state.range} post_filter={this.post_filter} post_order={this.post_order}/>
              </div>

              <div className="col-4_sm-12">
                <PostsDistribution id={this.props.id} range={this.state.range} post_filter={this.post_filter} post_order={this.post_order} />
              </div>

              <div className="col-4_sm-12">
                <TotalActivity id={this.props.id} range={this.state.range} post_filter={this.post_filter} post_order={this.post_order} />
              </div>

                <div className="col-4_sm-12">
                  <BestDaysBar id={this.props.id} range={this.state.range} post_filter={this.post_filter} post_order={this.post_order} />
                </div>

                <div className="col-8_sm-12">
                  <BestHoursBubble id={this.props.id} range={this.state.range} post_filter={this.post_filter} post_order={this.post_order} />
                </div>

              <div className="col-4_sm-12">
                {/*
                  <BestHashtags id={this.props.id} range={this.state.range} post_filter={this.post_filter} post_order={this.post_order}/>
                /*}
              </div>

              <div className="col-4_sm-12">
                {/*
                  <InteractionsBar id={this.props.id} range={this.state.range} post_filter={this.post_filter} post_order={this.post_order}/>
                */}
              </div>

              <div className="col-4_sm-12">
              {/*
                  <PerfectMix id={this.props.id} range={this.state.range} post_filter={this.post_filter} post_order={this.post_order}/>
                */}
              </div>

              <div className="col-4_sm-12">
                {/*
                  <Audience id={this.props.id} range={this.state.range} post_filter={this.post_filter} post_order={this.post_order}/>
                */}
              </div>

              <div className="col-4_sm-12">
                {/*
                  <Demographics id={this.props.id} range={this.state.range} post_filter={this.post_filter} post_order={this.post_order}/>
                */}
              </div>

                <div className="col-4_sm-12 no-pad-b">
                    <div className="col-12">
                      <div className="best-days-shell">
                        <div className="grid">

                      <div className="col-12">
                        {/*
                          <Reactions range={this.state.range} post_filter={this.post_filter} post_order={this.post_order} />
                        */}
                      </div>

                      <div className="col-12">
                        {/*
                          <Sentiment range={this.state.range} post_filter={this.post_filter} post_order={this.post_order} />
                        */}
                      </div>

                        </div>
                      </div>
                    </div>
                </div>

              <div className="col-12">
                  {/*
                    <TopicData id={this.props.id} range={this.state.range} post_filter={this.post_filter} post_order={this.post_order}/>
                  */}
              </div>

              <div className="col-12">
                <BestPosts id={this.props.id} range={this.state.range} post_filter={this.post_filter} post_order={this.post_order}/>
              </div>

              <div className="col-12">
                {/*
                  <BestPaidPosts id={this.props.id} range={this.state.range} post_filter={this.post_filter} post_order={this.post_order}/>
                */}
              </div>

            </div>
          </div>
        </div>
      ) : (
        <div className="main-container__placeholder">
          <Placeholder />
        </div>
      )}
    </div>
    )
  }


  openSearchCalendar = () =>  {
    this.props.fetchSocialBestCats( this.props.id, this.state.range, this.props.post_filter );
  }

  // handleFormatChange = (format: IDateFormatProps) => this.setState({ format });
  handleRangeChange = (range) => {
    const cookies = new Cookies();
    cookies.set('range', range, { path: '/' });
    this.setState({ range });
  }

}

function mapStateToProps( state )
{
  return {
    cats: state.cats.posts,
    loading: state.cats.loading,
    post_filter: state.post_filter,
    post_order: state.post_order
  }
}

export default connect(mapStateToProps, { fetchPosts, fetchSocialBestCats, changeFilter, changeOrder,
              fetchSocialPostDistribution, fetchSocialBestCatsGraph, fetchSocialBestDays, fetchSocialTotalActivity,
              fetchBestHashtags, fetchAudience })( MainContainer );
