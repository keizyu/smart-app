import React, { Component } from 'react'
import { Card, Elevation, Spinner, Tooltip, Position } from "@blueprintjs/core"
import { connect } from 'react-redux'
import { fetchAudience } from '../../../actions'

import './styles.css'

class Reactions extends Component {
  componentWillMount()
	{
		//this.props.fetchAudience( this.props.id );
	}

  render () {
    if( this.props.loading )
		{
			return( <Spinner /> );
		}

		let reactionWidth = {
			negativeWidth:{
				width: '0%'
			},
			positiveWidth:{
				width: '0%',
			}
		}

    reactionWidth.negativeWidth.width = this.props.audience.reactions_negative+'%'
    reactionWidth.positiveWidth.width = this.props.audience.reactions_positive+'%'

    return(

          <Card interactive={true} elevation={Elevation.TWO} className="no-pad-t">
            <div className="subtitle">
              Reactions

              <div className="question-tooltip question-tooltip__left">
								<Tooltip content="Reacciones positivas y negativas obtenidos en los posts." position={Position.BOTTOM}>
									<div><i className="fas fa-question"></i></div>
								</Tooltip>
							</div>


            </div>

						<div className="chart-container reaction">
								<div className="reaction__item reaction__item-positive" style={ reactionWidth.positiveWidth }>
									<div className="flex-holder">
										<div className="reaction__item--icon">
											<i className="far fa-thumbs-up fa-2x"></i>
										</div>

										<span className="reaction__item--data">
											POSITIVE { this.props.audience.reactions_positive }%
										</span>
								</div>
								</div>


								<div className="reaction__item reaction__item-negative"  style={ reactionWidth.negativeWidth }>
									<div className="flex-holder">
                    <div className="reaction__item--icon">
											<i className="far fa-thumbs-down fa-2x"></i>
										</div>
										<span className="reaction__item--data">
											NEGATIVE { this.props.audience.reactions_negative }%
										</span>
									</div>
								</div>

						</div>

          </Card>

    );
  }

}

function mapStateToProps( state )
{
  return {
		audience: state.audience.audience,
		loading: state.audience.loading
  }
}

export default connect(mapStateToProps, { fetchAudience })( Reactions );
