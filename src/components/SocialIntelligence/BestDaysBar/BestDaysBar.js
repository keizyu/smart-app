import React, { Component } from 'react';
import { Card, Elevation, Spinner, Tooltip, Position } from "@blueprintjs/core";
import { Column, Table, AutoSizer } from 'react-virtualized';
import { Bar } from 'react-chartjs-2';
import { connect } from 'react-redux';
import { fetchSocialBestDays } from '../../../actions';

class BestDaysBar extends Component {

	componentWillMount()
	{
		//this.props.fetchSocialBestDays( this.props.id );
	}

	constructor(props){
		super(props);
		this.state = {
			bestDaysData : {
				labels: [],
				datasets: [{
					data: [],
					backgroundColor: [],
				}],
			}
		}
	}

	fetchTable()
	{
		let table_data = this.props.bestdays.table_data;
		this.tableData = table_data.map(([categoryName, eScore, MoPosts]) => ({ categoryName, eScore, MoPosts}));

		return(
			<AutoSizer disableHeight>
				{
					({ width }) => {

						return (
							<Table
								width={width}
								height={214}
								headerHeight={30}
								rowHeight={26}
								rowCount={table_data.length}
								rowGetter={({ index }) => table_data[index]}
							>
								<Column
									label='Category'
									dataKey='0'
									width={width / 3}
								/>
								<Column
									width={width / 3}
									label='E Score'
									dataKey='1'
								/>
								<Column
									width={width / 3}
									label='Mo Posts'
									dataKey='2'
								/>
							</Table>
						)
					}
				}
			</AutoSizer>
		);
	}

	fetchGraph()
	{
		let bestDaysData={
			labels: this.props.bestdays.labels,
			datasets: [{
				data: this.props.bestdays.eng,
				backgroundColor: this.props.bestdays.backgroundColor,
			}],
		}

		return(
		<div className="chart-container">

			<Bar
				data={ bestDaysData }
				width={230}
				options={{
					legend: {
						display: false,
					},
					scales: {
						xAxes: [{
							barPercentage: 1,
							display: true,
							gridLines: {
								display: true,
								color: "#636363",
								drawTicks: false,
							},
							ticks: {
								fontColor: "#fff",
								padding: 14,
								callback: function(value) {
										return value.substr(0, 3); //truncate
								}
							}
						}],
						yAxes: [{
							display: true,
							gridLines: {
								display: true,
								color: "#636363",
								drawTicks: false,
							},
							ticks: {
								fontColor: "#fff",
								padding: 10,
							}
						}]
					}
				}}
			/>

		</div>);
	}

  render () {
		if( this.props.loading )
		{
				return(
						<div className="spinner-container">
							<Spinner />
						</div>
				)
		}


	console.log("distribution -> ");
	console.log(this.props.bestdays);

    return(
        <Card interactive={true} elevation={Elevation.TWO} className="no-pad-t">
          <div className="subtitle">
            Best Days
						<div className="question-tooltip">
							<Tooltip content="Los mejores días de la semana respecto al ER (engagement rate) reportado." position={Position.BOTTOM}>
								<div><i className="fas fa-question"></i></div>
							</Tooltip>
						</div>
          </div>

					{ this.fetchGraph() }

					{ this.fetchTable() }
        </Card>
    );
  }

}

function mapStateToProps( state )
{
  return {
		bestdays: state.bestdays.bestdays,
		loading: state.bestdays.loading
  }
}

export default connect(mapStateToProps, { fetchSocialBestDays })( BestDaysBar );
