import firebase from 'firebase';

const config = {
  apiKey: "AIzaSyAZDuPYfbQ40DE9ZchJaRMK-FMUanZdgyQ",
    authDomain: "smart-v2.firebaseapp.com",
    databaseURL: "https://smart-v2.firebaseio.com",
    projectId: "smart-v2",
    storageBucket: "smart-v2.appspot.com",
    messagingSenderId: "36621544370"
};

const fire = firebase.initializeApp(config)
const facebookProvider = new firebase.auth.FacebookAuthProvider();
const twitterProvider = new firebase.auth.TwitterAuthProvider();
const googleProvider = new firebase.auth.GoogleAuthProvider();


export { fire, facebookProvider, twitterProvider, googleProvider }
