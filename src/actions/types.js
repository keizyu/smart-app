
export const CHANGE_AUTH = 'change_auth';
export const SELECT_PAGE = 'select_page';

// listado de marcas (paginas guardadas)
export const LOADING_BRANDS = 'loading_brands';
export const FETCH_BRANDS = 'fetch_brands';

// obtener el listado de posts de una marca
export const LOADING_POSTS = 'loading_posts';
export const FETCH_POSTS = 'fetch_posts';

// obtener el listado de categorias de un usuario
export const LOADING_CATS = 'loading_cats';
export const FETCH_CATS = 'fetch_cats';

// Agregar una marca
export const ADDING_BRAND = 'adding_brand';

// eliminar categoríaf
export const DELETE_CAT = 'delete_cat';

// Actualiza la categoria de un post
export const UPDATE_BRAND = 'update_brand';

// Filtrado de posts
export const FILTER_POSTS = 'filter_posts';
export const ORDER_POSTS = 'filter_posts';

////////////////////////////////////////////////////////////////////////////////
// Social intelligence
export const LOADING_COM_ST = 'loading_com_st'; // comunication strategy
export const FETCH_COM_ST = 'fetch_com_st';

export const LOADING_COM_ST_G = 'loading_com_st_g'; // comunication strategy - graaphs
export const FETCH_COM_ST_G = 'fetch_com_st_g';

export const LOADING_COM_PD = 'loading_com_pd'; // post distribution
export const FETCH_COM_PD = 'fetch_com_pd';

export const LOADING_COM_TA = 'loading_com_ta'; // total activity
export const FETCH_COM_TA = 'fetch_com_ta';

export const LOADING_COM_BD = 'loading_com_bd'; // Best days
export const FETCH_COM_BD = 'fetch_com_bd';

export const LOADING_COM_BHT = 'loading_com_bht'; // Best hashtags
export const FETCH_COM_BHT = 'fetch_com_bht';

export const LOADING_COM_AUD = 'loading_com_aud'; // Best hashtags
export const FETCH_COM_AUD = 'fetch_com_aud';

export const LOADING_COM_BESTP = 'loading_com_bestp'; // Best posts
export const FETCH_COM_BESTP = 'fetch_com_bestp';

////////////////////////////////////////////////////////////////////////////////
// Ads Intelligence
export const LOADING_CAMPAINGS_ADS = 'loading_campaigns_ads'; // comunication strategy
export const FETCH_CAMPAINGS_ADS = 'fetch_campaigns_ads';
export const ERROR_CAMPAINGS_ADS = 'error_campaigns_ads';
