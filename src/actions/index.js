import axios from 'axios';
import Moment from 'moment';
import _ from 'lodash';

import { CHANGE_AUTH } from './types';
import { SELECT_PAGE } from './types';
import { FETCH_BRANDS } from './types';
import { LOADING_BRANDS } from './types';
import { FETCH_POSTS } from './types';
import { LOADING_POSTS } from './types';
import { FETCH_CATS } from './types';
import { LOADING_CATS } from './types';
import { ADDING_BRAND } from './types';

import { DELETE_CAT } from './types';

// Dashboard
import { UPDATE_BRAND } from './types';

import { FILTER_POSTS } from './types';
import { ORDER_POSTS } from './types';

// Social intelligence
import { FETCH_COM_ST } from './types';
import { LOADING_COM_ST } from './types';
import { FETCH_COM_ST_G } from './types';
import { LOADING_COM_ST_G } from './types';
import { FETCH_COM_PD } from './types';
import { LOADING_COM_PD } from './types';
import { FETCH_COM_TA } from './types';
import { LOADING_COM_TA } from './types';
import { FETCH_COM_BD } from './types';
import { LOADING_COM_BD } from './types';
import { FETCH_COM_BHT } from './types';
import { LOADING_COM_BHT } from './types';
import { FETCH_COM_AUD } from './types';
import { LOADING_COM_AUD } from './types';
import { FETCH_COM_BESTP } from './types';
import { LOADING_COM_BESTP } from './types';

////////////////////////////////////////////////////////////////////////////////
// Ads intel
import { LOADING_CAMPAINGS_ADS } from './types';
import { FETCH_CAMPAINGS_ADS } from './types';
import { ERROR_CAMPAINGS_ADS } from './types';


import { API_URL, API_ADS_URL } from '../config.js';

const AUTH_TOKEN = localStorage.getItem( "api_access_token" );

export function changeAuth(isLoggedIn) {
  return {
    type: CHANGE_AUTH,
    payload: isLoggedIn
  };
}

export function changeFilter( filter ) {
  return {
    type: FILTER_POSTS,
    payload: filter
  };
}

export function changeOrder( filter ) {
  return {
    type: ORDER_POSTS,
    payload: filter
  };
}

export function addingBrand( adding ) {
  return {
    type: ADDING_BRAND,
    payload: adding
  };
}

export function selectPage(page) {
  return {
    type: SELECT_PAGE,
    payload: page
  };
}

export function fetchBrands2() {
  let brands = [];

  console.log( "prueba" );
  // Se obtiene el listado de marcas

  console.log( 'localStorage.getItem( "api_access_token" )', localStorage.getItem( "api_access_token" ) );

  axios.defaults.baseURL = API_URL;
  axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;

  let AUTH_TOKEN = localStorage.getItem( "api_access_token" );

  axios.get( API_URL + '/get_saved_pages' )
    .then(  (response) =>
    {
      return( {
        type: FETCH_BRANDS,
        payload: response.data.result
      } );
      //console.log( response );
      //brands = response.data.result;
      //console.log( brands )
    })
    .catch(function (error)
    {
      console.log(error);
    })
    .then(function () {
      // do nothing
    });

  return {
    type: FETCH_BRANDS,
    payload: brands
  }
}

export function fetchBrands() {

  return function(dispatch) {
    axios.defaults.baseURL = API_URL;
    axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;
    let AUTH_TOKEN = localStorage.getItem( "api_access_token" );

    axios.get(`${API_URL}marcas`,{
      headers: {
        'Authorization': 'Bearer '+ AUTH_TOKEN
      },
    })
    .then(response => {
      console.log( response.data.data );
      dispatch({
        type: FETCH_BRANDS,
        payload: response.data.data
      });
    })
    .catch((error) => {
      console.log(error);
      //window.location.href='/';
    })
  }
}

export function fetchPosts( brand_id, range=null, type='all' ) {

  return function(dispatch) {

    dispatch({
      type: LOADING_POSTS,
      payload: true
    });

    let start = '-';
    let end = '-';
    let AUTH_TOKEN = localStorage.getItem( "api_access_token" );
    axios.defaults.baseURL = API_URL;
    axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;

    console.log( range );

    if( range != null )
    {
      start = Moment( range[0] ).format('YYYY-MM-DD');
      end = Moment( range[1] ).format('YYYY-MM-DD');
    }

    axios.get(`${API_URL}posts/brand/${brand_id}/${start}/${end}/${type}`,{
      headers: {
        'Authorization': 'Bearer '+ AUTH_TOKEN
      },
    })
    .then(response => {

      console.log( "index posts ", response.data.data );

      dispatch({
        type: FETCH_POSTS,
        payload: response.data.data
      });
    })
    .catch((error) => {
      console.log(error);
      ////////////////////////window.location.href='/';
    })
  }
}


export function fetchCats( pagina_id ) {

  return function(dispatch) {
    dispatch({
      type: LOADING_CATS,
      payload: true
    });

    let AUTH_TOKEN = localStorage.getItem( "api_access_token" );
    axios.defaults.baseURL = API_URL;

    axios.get(`${API_URL}categorias`,{
      headers: {
        'Authorization': 'Bearer '+ AUTH_TOKEN
      },
    })
    .then(response => {
      dispatch({
        type: FETCH_CATS,
        payload: response.data.data
      });
    })
    .catch((error) => {
      console.log(error);
    })
  }
}

export function addCat( texto_categoria, pagina_id ) {
  return function(dispatch) {
    //console.log( "Agregandoo la categoria" );
    let AUTH_TOKEN = localStorage.getItem( "api_access_token" );
    axios.defaults.baseURL = API_URL;
    axios.defaults.headers.common['Authorization'] = "Bearer " + AUTH_TOKEN;

    //console.log( "texto -> ", texto_categoria );

    axios.post(`${API_URL}categorias`,{
      texto: texto_categoria,
      pagina_id: pagina_id
    })
    .then(response => {

        dispatch({
          type: ADDING_BRAND,
          payload: true
        });

    })
    .catch((error) => {
      console.log(error);
    })
  }
}

export function editCat( id_categoria, texto_categoria ) {
  return function(dispatch) {
    let AUTH_TOKEN = localStorage.getItem( "api_access_token" );
    axios.defaults.baseURL = API_URL;
    axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;

    axios.get(`${API_URL}update_categorie/${id_categoria}/${texto_categoria}`,{
      headers: {
        'Authorization': 'Basic '+ AUTH_TOKEN
      },
    })
    .then(response => {

    })
    .catch((error) => {
      console.log(error);
    })
  }
}

export function removeCat( id_categoria ) {
  return function(dispatch) {
    //console.log( "Agregandoo la categoria" );
    let AUTH_TOKEN = localStorage.getItem( "api_access_token" );
    axios.defaults.baseURL = API_URL;
    axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;

    //console.log( "texto -> ", texto_categoria );

    axios.get(`${API_URL}delete_categorie/${id_categoria}`,{
      headers: {
        'Authorization': 'Basic '+ AUTH_TOKEN
      },
    })
    .then(response => {

        dispatch({
          type: DELETE_CAT,
          payload: true
        });

    })
    .catch((error) => {
      console.log(error);
    })
  }
}

////////////////////////////////////////////////////////////////////////////////
// Social Intelligence
export function fetchSocialBestCats( page_id, range, type='all' ) {

  return function(dispatch) {
    //console.log( "Social - obteniendo los datos de categorias " );

    dispatch({
      type: LOADING_COM_ST,
      payload: true
    });

    let AUTH_TOKEN = localStorage.getItem( "api_access_token" );
    axios.defaults.baseURL = API_URL;
    axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;

    let start = "";
    let end = "";


    if( range != null )
    {
      start = Moment( range[0] ).format('YYYY-MM-DD');
      end = Moment( range[1] ).format('YYYY-MM-DD');
    }

    axios.get(`${API_URL}social/bestCategories/${page_id}?start=${start}&end=${end}&type=${type}`,{
      headers: {
        'Authorization': 'Bearer '+ AUTH_TOKEN
      },
    })
    .then(response => {
      let categorias = _.orderBy(response.data.data.data, ['es_calculated'], ['desc']);

      dispatch({
        type: FETCH_COM_ST,
        payload: categorias
      });

    })
    .catch((error) => {
      console.log(error);
    })
  }
}

export function fetchSocialBestCatsGraph( page_id, range, type='all' ) {

  return function(dispatch) {
    //console.log( "Social - obteniendo los datos de categorias " );

    dispatch({
      type: LOADING_COM_ST_G,
      payload: true
    });


    let start = "";
    let end = "";


    if( range != null )
    {
      start = Moment( range[0] ).format('YYYY-MM-DD');
      end = Moment( range[1] ).format('YYYY-MM-DD');
    }

    let AUTH_TOKEN = localStorage.getItem( "api_access_token" );
    axios.defaults.baseURL = API_URL;
    axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;

    axios.get(`${API_URL}social/bestCategories/${page_id}?start=${start}&end=${end}&type=${type}`,{
      headers: {
        'Authorization': 'Bearer '+ AUTH_TOKEN
      },
    })
    .then(response => {
      dispatch({
        type: FETCH_COM_ST_G,
        payload: response.data.data.graph
      });
    })
    .catch((error) => {
      console.log(error);
    })
  }
}

export function fetchSocialPostDistribution( page_id, range, type='all' ) {

  return function(dispatch) {
    //console.log( "Social - obteniendo la distribucion de los posts " );

    dispatch({
      type: LOADING_COM_PD,
      payload: true
    });

    let start = "";
    let end = "";


    if( range != null )
    {
      start = Moment( range[0] ).format('YYYY-MM-DD');
      end = Moment( range[1] ).format('YYYY-MM-DD');
    }

    let AUTH_TOKEN = localStorage.getItem( "api_access_token" );
    axios.defaults.baseURL = API_URL;
    axios.defaults.headers.common['Authorization'] = "Bearer " + AUTH_TOKEN;

    axios.get(`${API_URL}social/postDistribution/${page_id}?start=${start}&end=${end}&type=${type}`,{
      headers: {
        'Authorization': 'Bearer '+ AUTH_TOKEN
      },
    })
    .then(response => {
      dispatch({
        type: FETCH_COM_PD,
        payload: response.data.data.data
      });
    })
    .catch((error) => {
      console.log(error);
    })
  }
}

export function fetchSocialTotalActivity( page_id, range, type='all' ) {

  return function(dispatch) {
    //console.log( "Social - obteniendo actividad total " );

    dispatch({
      type: LOADING_COM_TA,
      payload: true
    });

    let AUTH_TOKEN = localStorage.getItem( "api_access_token" );
    axios.defaults.baseURL = API_URL;
    axios.defaults.headers.common['Authorization'] = "Bearer "+AUTH_TOKEN;

    let start = "";
    let end = "";


    if( range != null )
    {
      start = Moment( range[0] ).format('YYYY-MM-DD');
      end = Moment( range[1] ).format('YYYY-MM-DD');
    }

    axios.get(`${API_URL}social/total/${page_id}?start=${start}&end=${end}&type=${type}`,{
      headers: {
        'Authorization': 'Basic '+ AUTH_TOKEN
      },
    })
    .then(response => {

      console.log( "Datos de total activity -->", response );

      dispatch({
        type: FETCH_COM_TA,
        payload: response.data.data
      });
    })
    .catch((error) => {
      console.log(error);
    })
  }
}

export function fetchSocialBestDays( page_id, range, type='all' ) {

  return function(dispatch) {
    //console.log( "Social - obteniendo Best Days " );

    dispatch({
      type: LOADING_COM_BD,
      payload: true
    });

    let AUTH_TOKEN = localStorage.getItem( "api_access_token" );
    axios.defaults.baseURL = API_URL;
    axios.defaults.headers.common['Authorization'] = "Bearer "+AUTH_TOKEN;

    let start = "";
    let end = "";


    if( range != null )
    {
      start = Moment( range[0] ).format('YYYY-MM-DD');
      end = Moment( range[1] ).format('YYYY-MM-DD');
    }

    axios.get(`${API_URL}social/bestDayPosts/${page_id}?start=${start}&end=${end}&type=${type}`,{
      headers: {
        'Authorization': 'Basic '+ AUTH_TOKEN
      },
    })
    .then(response => {
      console.log("detehch social days");
      console.log( response );
      dispatch({
        type: FETCH_COM_BD,
        payload: response.data.data
      });
    })
    .catch((error) => {
      console.log(error);
    })
  }
}


export function fetchBestHashtags( page_id, range, type='all' ) {
  return function(dispatch) {
    //console.log( "Social - obteniendo Best Hashtags " );

    dispatch({
      type: LOADING_COM_BHT,
      payload: true
    });

    let AUTH_TOKEN = localStorage.getItem( "api_access_token" );
    axios.defaults.baseURL = API_URL;
    axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;

    let start = "";
    let end = "";


    if( range != null )
    {
      start = Moment( range[0] ).format('YYYY-MM-DD');
      end = Moment( range[1] ).format('YYYY-MM-DD');
    }

    axios.get(`${API_URL}get_best_hashtags/${page_id}?start=${start}&end=${end}&type=${type}`,{
      headers: {
        'Authorization': 'Basic '+ AUTH_TOKEN
      },
    })
    .then(response => {

      console.log( "Datos de best hashtags -->", response.data.data );

      dispatch({
        type: FETCH_COM_BHT,
        payload: response.data.data
      });
    })
    .catch((error) => {
      console.log(error);
    })
  }
}

export function fetchAudience( page_id, range, type='all' ) {
  return function(dispatch) {
    console.log( "-----------------------------------------------------------" )
    console.log( "Social - obteniendo Audience data --> ", page_id );

    dispatch({
      type: LOADING_COM_AUD,
      payload: true
    });

    let AUTH_TOKEN = localStorage.getItem( "api_access_token" );
    axios.defaults.baseURL = API_URL;
    axios.defaults.headers.common['Authorization'] = "Bearer "+AUTH_TOKEN;

    let start = "";
    let end = "";

    if( range != null )
    {
      start = Moment( range[0] ).format('YYYY-MM-DD');
      end = Moment( range[1] ).format('YYYY-MM-DD');
    }

    axios.get(`${API_URL}social/audience/${page_id}?start=${start}&end=${end}&type=${type}`)
    .then(response => {

      console.log( "Datos de best audience  -->", response.data.data );

      dispatch({
        type: FETCH_COM_AUD,
        payload: response.data.data
      });
    })
    .catch((error) => {
      console.log(error);
    })
  }
}


export function fetchBestPosts( page_id ) {
  return function(dispatch) {
    //console.log( "Social - obteniendo best posts " );

    dispatch({
      type: LOADING_COM_BESTP,
      payload: true
    });

    let AUTH_TOKEN = localStorage.getItem( "api_access_token" );
    axios.defaults.baseURL = API_URL;
    axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;

    axios.get(`${API_URL}get_best_posts/${page_id}`,{
      headers: {
        'Authorization': 'Basic '+ AUTH_TOKEN
      },
    })
    .then(response => {

      //console.log( "Datos de best audience  -->", response.data.data );

      dispatch({
        type: FETCH_COM_BESTP,
        payload: response.data.data
      });
    })
    .catch((error) => {
      console.log(error);
    })
  }
}

////////////////////////////////////////////////////////////////////////////////
// Actualizar la categoría deun post
export function updatePostCategory( categoria_id, post_id, provider ) {

  return function(dispatch) {
    //console.log( "Actualizando la categoría del post" );


    let AUTH_TOKEN = localStorage.getItem( "api_access_token" );
    axios.defaults.baseURL = API_URL;
    axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;

    axios.get(`${API_URL}posts/update/${categoria_id}/${post_id}/${provider}`,{
      headers: {
        'Authorization': 'Bearer '+ AUTH_TOKEN
      },
    })
    .then(response => {

        //console.log( "post category ", response.data );

        dispatch({
          type: UPDATE_BRAND,
          payload: response.data
        });
    })
    .catch((error) => {
      console.log(error);
    })
  }
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// ADS intel
export function fetchAdsCampaign( page_id, range ) {

  return function(dispatch) {
    axios.defaults.baseURL = API_URL;
    axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;
    let AUTH_TOKEN = localStorage.getItem( "api_access_token" );

    dispatch({
      type: LOADING_CAMPAINGS_ADS,
      payload: true
    });

    let start = "";
    let end = "";

    if( range != null )
    {
      start = range[0];
      end = range[1];
    }

    axios.get(`${API_URL}get_ads_data/${page_id}?start=${start}&end=${end}`,{
      headers: {
        'Authorization': 'Basic '+ AUTH_TOKEN
      },
    })
    .then(response => {
      dispatch({
        type: FETCH_CAMPAINGS_ADS,
        payload: response.data.result
      });
    })
    .catch((error) => {
      console.log(error);
      //window.location.href='/';
      dispatch({
        type: ERROR_CAMPAINGS_ADS,
        payload: error
      });
    })
  }
}

export function fetchCampaignADS( page_id, range ) {

  return function(dispatch) {
    axios.defaults.baseURL = API_URL;
    axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;
    let AUTH_TOKEN = localStorage.getItem( "api_access_token" );

    dispatch({
      type: LOADING_CAMPAINGS_ADS,
      payload: true
    });

    let start = "";
    let end = "";

    if( range != null )
    {
      start = range[0];
      end = range[1];
    }

    axios.get(`${API_ADS_URL}list_campaigns_db/${page_id}?start=${start}&end=${end}`,{
      headers: {
        'Authorization': 'Basic '+ AUTH_TOKEN
      },
    })
    .then(response => {
      console.log( "RESPONSE -> ", response );

      dispatch({
        type: FETCH_CAMPAINGS_ADS,
        payload: response.data
      });
    })
    .catch((error) => {
      console.log(error);
      //window.location.href='/';
      dispatch({
        type: ERROR_CAMPAINGS_ADS,
        payload: error
      });
    })
  }
}
