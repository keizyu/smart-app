
// local
// const API_URL = 'https://api.smart-v3.com/api/';
// const API_ADS_URL = 'https://api.smart-v3.com/api/';

// servidor
const API_URL = 'https://php.smart-v2.eu/api/';
const API_ADS_URL = 'https://php.smart-v2.eu/api/';


export {
	API_URL,
	API_ADS_URL
}
