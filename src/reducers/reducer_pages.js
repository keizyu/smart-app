import { FETCH_PAGES_BEGIN } from '../actions/types';
import { FETCH_PAGES_SUCCESS } from '../actions/types';
import { FETCH_PAGES_FAILURE } from '../actions/types';

const initialState = {
    items: [],
    loading: false,
    error: null
}

// { id: '0000001', description: 'página 1', token: 'token de la página 1' },

export default function pagesReducer( state=initialState, action) {
    switch ( action.type ) {
        case FETCH_PAGES_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            };

        case FETCH_PAGES_SUCCESS:
            return {
                ...state,
                loading: false,
                items: action.payload.pages
            };

        case FETCH_PAGES_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload.error,
                items: []
            };

        default:
            return state;
    }
}
