
import { FETCH_COM_PD } from '../actions/types';
import { LOADING_COM_PD } from '../actions/types';

const initialState = {
    loading: true,
    days: []
}

export function fetchSocialPostDistribution( state=initialState, action )
{
    switch( action.type )
    {
        case LOADING_COM_PD:
            return Object.assign({}, state, {loading: action.payload});

        case FETCH_COM_PD:
            return Object.assign({}, state, {loading:false, days: action.payload});

        default:
        return state;
  }
}
