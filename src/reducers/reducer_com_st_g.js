
import { FETCH_COM_ST_G } from '../actions/types';
import { LOADING_COM_ST_G } from '../actions/types';


const initialState = {
    loading: true,
    graph: []
}

export function fetchSocialBestCatsGraph( state=initialState, action )
{
    switch( action.type )
    {
        case LOADING_COM_ST_G:
            return Object.assign({}, state, {loading: action.payload});

        case FETCH_COM_ST_G:
            return Object.assign({}, state, {loading:false, graph: action.payload});

        default:
        return state;
  }
}
