
import { FETCH_COM_BD } from '../actions/types';
import { LOADING_COM_BD } from '../actions/types';

const initialState = {
    loading: true,
    bestdays: []
}

export function fetchSocialBestDays( state=initialState, action )
{
    switch( action.type )
    {
        case LOADING_COM_BD:
            return Object.assign({}, state, {loading: true});

        case FETCH_COM_BD:
            return Object.assign({}, state, {loading:false, bestdays: action.payload});

        default:
        return state;
  }
}
