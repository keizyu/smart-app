
import { UPDATE_BRAND } from '../actions/types';

const initialState = {
    addingBrand: false
}

export default function(state = initialState, action) {
  switch (action.type) {
    case UPDATE_BRAND:
        return Object.assign({}, state, { addingBrand: action.payload});


    default:
      return state;
  }
}
