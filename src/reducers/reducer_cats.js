
import { FETCH_CATS } from '../actions/types';
import { LOADING_CATS } from '../actions/types';


const initialState = {
    loading: true,
    categories: []
}

export function fetchCats( state=initialState, action )
{
    switch( action.type )
    {
        case LOADING_CATS:
            return Object.assign({}, state, {loading: action.payload});

        case FETCH_CATS:
            return Object.assign({}, state, {loading:false, categories: action.payload});

        default:
        return state;
  }
}
