
import { DELETE_CAT } from '../actions/types';

const initialState = {
    removeCat: false
}

export default function(state = initialState, action) {
  switch (action.type) {
    case DELETE_CAT:
      return action.payload;
    default:
      return state;
  }
}
