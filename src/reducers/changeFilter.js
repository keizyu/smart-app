import { FILTER_POSTS } from '../actions/types';

// filter options are: all, fb, tw, ins, in

export default function(state = 'all', action) {
  switch (action.type) {
    case FILTER_POSTS:
      return action.payload;
    default:
      return state;
  }
}
