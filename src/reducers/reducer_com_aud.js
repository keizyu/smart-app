
import { FETCH_COM_AUD } from '../actions/types';
import { LOADING_COM_AUD } from '../actions/types';

const initialState = {
    loading: true,
    audience: []
}

export function fetchAudience( state=initialState, action )
{
    switch( action.type )
    {
        case LOADING_COM_AUD:
            return Object.assign({}, state, {loading: true});

        case FETCH_COM_AUD:
            return Object.assign({}, state, {loading:false, audience: action.payload});

        default:
        return state;
  }
}
