
import { ADDING_BRAND } from '../actions/types';

const initialState = {
    addingBrand: false
}

export default function(state = initialState, action) {
  switch (action.type) {
    case ADDING_BRAND:
      return action.payload;
    default:
      return state;
  }
}
