import { ORDER_POSTS } from '../actions/types';

// order options are:

export default function(state = 'date', action) {
  switch (action.type) {
    case ORDER_POSTS:
      return action.payload;
    default:
      return state;
  }
}
