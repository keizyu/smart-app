
import { LOADING_CAMPAINGS_ADS, FETCH_CAMPAINGS_ADS, ERROR_CAMPAINGS_ADS } from '../actions/types';


const initialState = {
    error: false,
    loading: true,
    campaigns: []
}

export function fetchCampaignADS( state=initialState, action )
{
    switch( action.type )
    {
        case LOADING_CAMPAINGS_ADS:
            return Object.assign({}, state, {loading: action.payload, error: false, campaigns:[]});

        case FETCH_CAMPAINGS_ADS:
            console.log( "posts -->", state.cats );

            return Object.assign({}, state, {loading:false, campaigns: action.payload});

        case ERROR_CAMPAINGS_ADS:
            console.log( "posts -->", state.cats );

            return Object.assign({}, state, {loading:false, campaigns: [], error: true});

        default:
        return state;
  }
}
