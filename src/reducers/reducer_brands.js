
import { FETCH_BRANDS } from '../actions/types';
import { LOADING_BRANDS } from '../actions/types';

const initialState = {
    loading: true,
    brands: []
}

export function fetchBrands( state=initialState, action )
{
    switch( action.type )
    {
        case LOADING_BRANDS:
            return Object.assign({}, state, {loading: action.payload});

        case FETCH_BRANDS:

            console.log( "----------------------------------------" );
            console.log( state.brands );
            console.log( "----------------------------------------" );

            return Object.assign({},
                state,
                {
                    brands: action.payload,
                    loading: false
                }
                );

        default:
            return state;
    }
}
