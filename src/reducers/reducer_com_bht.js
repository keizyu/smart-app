
import { FETCH_COM_BHT } from '../actions/types';
import { LOADING_COM_BHT } from '../actions/types';

const initialState = {
    loading: true,
    hashtags: []
}

export function fetchBestHashtags( state=initialState, action )
{
    switch( action.type )
    {
        case LOADING_COM_BHT:
            return Object.assign({}, state, {loading: true});

        case FETCH_COM_BHT:
            return Object.assign({}, state, {loading:false, hashtags: action.payload});

        default:
        return state;
  }
}
