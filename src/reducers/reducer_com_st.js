
import { FETCH_COM_ST } from '../actions/types';
import { LOADING_COM_ST } from '../actions/types';


const initialState = {
    loading: true,
    cats: []
}

export function fetchSocialBestCats( state=initialState, action )
{
    switch( action.type )
    {
        case LOADING_COM_ST:
            return Object.assign({}, state, {loading: action.payload});

        case FETCH_COM_ST:
            console.log( "posts -->", state.cats );

            return Object.assign({}, state, {loading:false, posts: action.payload});

        default:
        return state;
  }
}
