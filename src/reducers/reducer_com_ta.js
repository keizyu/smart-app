
import { FETCH_COM_TA } from '../actions/types';
import { LOADING_COM_TA } from '../actions/types';

const initialState = {
    loading: true,
    total: []
}

export function fetchSocialTotalActivity( state=initialState, action )
{
    switch( action.type )
    {
        case LOADING_COM_TA:
            return Object.assign({}, state, {loading: true});

        case FETCH_COM_TA:
            return Object.assign({}, state, {loading:false, total: action.payload});

        default:
        return state;
  }
}
