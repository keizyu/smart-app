
import { FETCH_COM_BESTP } from '../actions/types';
import { LOADING_COM_BESTP } from '../actions/types';

const initialState = {
    loading: true,
    bestposts: []
}

export function fetchBestPosts( state=initialState, action )
{
    switch( action.type )
    {
        case LOADING_COM_BESTP:
            return Object.assign({}, state, {loading: true});

        case FETCH_COM_BESTP:
            return Object.assign({}, state, {loading:false, bestposts: action.payload});

        default:
        return state;
  }
}
