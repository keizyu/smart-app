import {  combineReducers } from 'redux';
import authReducer from './auth';
import { fetchBrands } from './reducer_brands';
import { fetchPosts } from './reducer_posts';
import { fetchCats } from './reducer_cats';
import { fetchSocialBestCats } from './reducer_com_st';
import { fetchSocialBestCatsGraph } from './reducer_com_st_g';
import { fetchSocialPostDistribution } from './reducer_com_pd';
import { fetchSocialTotalActivity } from './reducer_com_ta';
import addingBrand from './adding_brand';
import removeCat from './remove_cat';
import updatePostCategory from './update_brand_post';
import { fetchSocialBestDays } from './reducer_com_bd';
import { fetchBestHashtags } from './reducer_com_bht';
import { fetchAudience } from './reducer_com_aud';
import { fetchBestPosts } from './reducer_com_bestp';
import changeFilter from './changeFilter';
import changeOrder from './changeOrder';

// ADS INTEL
import { fetchCampaignADS } from './reducer_campaign_ads';

const rootReducer = combineReducers({
  auth: authReducer,
  brands: fetchBrands,
  posts: fetchPosts,
  categories: fetchCats,
  cats: fetchSocialBestCats,
  graph: fetchSocialBestCatsGraph,
  days: fetchSocialPostDistribution,
  adding: addingBrand,
  total: fetchSocialTotalActivity,
  update_post: updatePostCategory,
  bestdays: fetchSocialBestDays,
  post_filter: changeFilter,
  post_order: changeOrder,
  hashtags: fetchBestHashtags,
  audience: fetchAudience,
  bestposts: fetchBestPosts,
  removeCat: removeCat,
  campaing_ads: fetchCampaignADS,
});

export default rootReducer;
