
import { FETCH_POSTS } from '../actions/types';
import { LOADING_POSTS } from '../actions/types';


const initialState = {
    loading: true,
    posts: [],
    start: '',
    end: ''
}

export function fetchPosts( state=initialState, action )
{
    switch( action.type )
    {
        case LOADING_POSTS:
            return Object.assign({}, state, {
              loading: action.payload,
              start: action.payload.start,
              end: action.payload.end});

        case FETCH_POSTS:
            console.log( "** posts -->", action.payload );

            console.log( "action.payload.start ", action.payload.start );

            return Object.assign({}, state, {
              loading:false,
              posts: action.payload,
              start: action.payload.start,
              end: action.payload.end});

        default:
        return state;
  }
}
